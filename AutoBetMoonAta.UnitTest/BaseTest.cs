﻿using AutoBetMoonAta.Cores;
using AutoBetMoonAta.UnitTest.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoBetMoonAta.UnitTest
{
    public class DataTest
    {
        public DataTest()
        {
            AccountVM = new AccountVM();
            MoneyStepSettingUCVM = new MoneyStepSettingUCVM(AccountVM);
            ServerResultHistory = new ServerResultHistory();
            Circles = new CircleCollection(MoneyStepSettingUCVM, AccountVM.CircleSettingUCVM);


            //MoneyStepSettingUCVM.

        }
        internal AccountVM AccountVM { get; } 
        internal MoneyStepSettingUCVM MoneyStepSettingUCVM { get; }
        internal ServerResultHistory ServerResultHistory { get; }
        internal CircleCollection Circles { get; }
    }
}
