using AutoBetMoonAta.Cores;
using AutoBetMoonAta.Cores.Interfaces;
using AutoBetMoonAta.UnitTest.Class;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AutoBetMoonAta.UnitTest
{
    [TestClass]
    public class CircleTest
    {
        [TestMethod]
        public void TestSkip()
        {
            DataTest dataTest = new DataTest();
            //dataTest.AccountVM.
            CircleVM circleVM = new CircleVM("BBBBBBBBBB");
            Circle circle = new Circle(circleVM);
            dataTest.Circles.Add(circle);

            Assert.IsTrue(circle.CurrentActionIsBuy);
            circle.UpdateWinLostAndReset(dataTest.ServerResultHistory);
            Assert.IsTrue(circle.CurrentActionIsBuy);
            dataTest.ServerResultHistory.Push(true);
            circle.UpdateWinLostAndReset(dataTest.ServerResultHistory);
            Assert.IsFalse(circle.CurrentActionIsBuy);
            dataTest.ServerResultHistory.Push(true);
            circle.UpdateWinLostAndReset(dataTest.ServerResultHistory);
            Assert.IsTrue(circle.CurrentActionIsBuy);
        }
    }
}