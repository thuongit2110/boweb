﻿using AutoBetMoonAta.Cores.Interfaces;

namespace AutoBetMoonAta.UnitTest.Class
{
    internal class CircleSettingUCVM : ICircleSettingUCVM
    {
        public int ResetLose { get; set; } = 99;

        public int ResetWin { get; set; } = 99;

        public bool IsSplitMoneyFlow { get; set; } = false;
    }
}
