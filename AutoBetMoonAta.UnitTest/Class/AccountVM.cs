﻿using AutoBetMoonAta.Cores.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoBetMoonAta.UnitTest.Class
{
    internal class AccountVM : IAccountVM
    {
        public string ServerResultHistoryString { get; set; }

        public bool IsTestMode { get; }

        public IAdvancedConfigUCVM AdvancedConfigUCVM { get; } = new AdvancedConfigUCVM();

        public ICircleSettingUCVM CircleSettingUCVM { get; } = new CircleSettingUCVM();

        public IAutoData AutoData { get; } = new AutoData();
    }
}
