﻿using AutoBetMoonAta.Cores.Interfaces;

namespace AutoBetMoonAta.UnitTest.Class
{
    internal class AutoData : IAutoData
    {
        public bool IsPause { get; set; } = false;
    }
}
