﻿using AutoBetMoonAta.Cores.Enums;
using AutoBetMoonAta.Cores.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoBetMoonAta.UnitTest.Class
{
    internal class MoneyStepSettingUCVM : IMoneyStepSettingUCVM
    {
        public MoneyStepSettingUCVM(IAccountVM AccountVM)
        {
            this.AccountVM = AccountVM;
        }
        public IAccountVM AccountVM { get; }


        public MoneyStepType MoneyStepTypesSelected { get; set; } = MoneyStepType.MultiplierLoseAndWin;

        public double[] StepResults { get; set; } = new double[] { 1.0, 1.5, 2 };

        public MoneyIndexAdvanced MoneyIndexAdvancedsSelected { get; set; } = MoneyIndexAdvanced.UpLostDownWin;

        public int MoneyIndexAdvancedStart { get; set; } = 1;
        public int MoneyIndexAdvancedIncrease { get; set; } = 1;
        public int MoneyIndexAdvancedDecrease { get; set; } = 1;


        public int MoneyResultUpDownSessionCount { get; set; } = 1;
        public double MoneyResultUpDownMulti { get; set; } = 1.0;
    }
}
