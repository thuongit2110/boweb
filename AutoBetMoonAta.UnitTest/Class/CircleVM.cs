﻿using AutoBetMoonAta.Cores.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoBetMoonAta.UnitTest.Class
{
    internal class CircleVM : ICircleVM
    {
        public CircleVM(string data)
        {
            if (string.IsNullOrWhiteSpace(data)) throw new ArgumentNullException(nameof(data));
            var splits = data.Split('-');
            if (splits.Length >= 2)
            {
                Condition = splits[0];
                Action = splits[1];
            }
            else
            {
                Action = data;
            }
        }
        public string Action { get; }

        public uint ActionActive { get; set; }
        public int ActionResultCount { get; set; }

        public string Condition { get; } = string.Empty;

        public int LoseCount { get; set; }
        public int MoneyIndex { get; set; }
        public int WinCount { get; set; }

        public bool IsActive { get; }
    }
}
