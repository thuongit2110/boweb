﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MomoWrapApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoBetMoonAta.UnitTest
{
    [TestClass]
    public class MomoWrapApiTest
    {
        readonly MomoWrapperApi api = new MomoWrapperApi();


        [TestMethod]
        public void LoginNewTest()
        {
            api.LoginNew(new LoginNew() { Phone = "0383587291", Password = "151093" }).Wait();
        }

        [TestMethod]
        public void LoginOTPTest()
        {
            api.LoginOTP(new LoginOtp() { Phone = "0383587291", Password = "151093", Otp = "5619" }).Wait();
        }

        [TestMethod()]
        public void GetBalanceTest()
        {
            api.GetBalance(new MomoPhone() { Phone = "0383587291" }).Wait();
        }

        [TestMethod()]
        public void SendMoneyTest()
        {
            api.SendMoney(new SendMoney() { Phone = "0383587291", Receiver = "0988475437", Amount = 100, Message = "test" }).Wait();
        }

        [TestMethod()]
        public void CheckAccountTest()
        {
            api.CheckAccount(new CheckAccount() { Phone = "0383587291", Receiver = "0988475437" }).Wait();
        }

        [TestMethod()]
        public void GetTransactionsTest()
        {
            DateTime begin = new DateTime(2022, 03, 15);
            DateTime end = new DateTime(2022, 03, 29);
            api.GetTransactions(new GetTransactions()
            {
                Phone = "0585913694",
                Begin = ((DateTimeOffset)begin).ToUnixTimeMilliseconds(),
                End = ((DateTimeOffset)end).ToUnixTimeMilliseconds()
            }).Wait();
        }

        [TestMethod()]
        public void WithdrawTest()
        {
            api.Withdraw(new Withdraw() { Phone = "0383587291", Amount = 1000 }).Wait();
        }

        [TestMethod()]
        public void GetBankListTest()
        {
            var r = api.GetBankList().Result;
        }

        [TestMethod()]
        public void CheckBankTest()
        {
            api.CheckBank(new CheckBank() { Phone = "0383587291", BankCode = "", AccountNo = "" }).Wait();
        }

        [TestMethod()]
        public void SendToBankTest()
        {
            api.SendToBank(new SendToBank() { Phone = "0383587291", Amount = 1000, BankCode = "", AccountNo = "" }).Wait();
        }
    }
}
