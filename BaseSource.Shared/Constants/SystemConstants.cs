﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.Shared.Constants
{
    public class SystemConstants
    {
        public const string MainConnectionString = "BaseSourceDbConnection";

        public class AppSettings
        {
            public const string DefaultLanguageId = "DefaultLanguageId";
            public const string Token = "Token";
            public const string BackendApiClient = "BackendApiClient";


            public const string NotesAuthorizeToken = "NotesAuthorizeToken";
        }
        public class Roles
        {
            public const string Admin = "Admin";
            public const string F1 = "F1";
            public const string F2 = "F2";
        }

    }
}
