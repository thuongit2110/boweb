﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.Shared.Enums
{
    public enum EBillStatus : byte
    {
        Initial = 1,
        Successed,
        Canceled
    }
    public enum EPaymentThirdPartyId : byte
    {
        MOMO,
        VnPay,
        Paypal
    }
    public enum EPaymentStatus : byte
    {
        Unpaid = 1,
        Pending,
        Paid
    }
    public enum ModeCheckPassNote : byte
    {
        Url,
        Id
    }

    public enum ModeEncrypt : byte
    {
        Encrypt,
        Decrypt
    }
    public enum ModeGetNote : byte
    {
        NoteUrl,
        ShareUrl
    }

    public enum EAdsLocationType : byte
    {
        [Display(Name = "Sidebar")]
        Sidebar = 1,
        [Display(Name = "Topbar")]
        Topbar
    }

    public enum EAdsType : byte
    {
        [Display(Name = "Google Ads")]
        GoogleAds = 1,
        [Display(Name = "Image")]
        Image
    }
}
