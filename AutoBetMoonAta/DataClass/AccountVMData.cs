﻿using MoonAtaApi;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoBetMoonAta.DataClass
{
    public class AccountVMData
    {
        public MoonAtaToken Token { get; set; }
        public string NickName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string TwoFA { get; set; }
        public bool IsAutoLogin { get; set; }
        public double TestBalanced { get; set; } = 500;
        public string EmulatorData { get; set; }
        public bool IsEmunatorLoop { get; set; } = true;
        
        public MoonAtaBetAccountType MoonAtaBetAccountType { get; set; } = MoonAtaBetAccountType.DEMO;
        public CircleSettingData CircleSetting { get; set; } = new CircleSettingData();
        public MoneyStepSettingData MoneyStepSetting { get; set; } = new MoneyStepSettingData();
        public AccountAdvancedSetting AdvancedSetting { get; set; } = new AccountAdvancedSetting();
    }
}
