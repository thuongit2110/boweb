﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoBetMoonAta.DataClass
{
    internal class SettingData
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsAutoLogin { get; set; }
        public int EmulatorDelay { get; set; } = 100;
        public bool DownloadAdv { get; set; } = false;
    }
}
