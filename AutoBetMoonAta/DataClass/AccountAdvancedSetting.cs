﻿namespace AutoBetMoonAta.DataClass
{
    public class AccountAdvancedSetting
    {
        public int BetTimeMax { get; set; } = 30;
        public int BetTimeMin { get; set; } = 20;
        public bool IsAllowBet { get; set; } = true;


        //Circle
        public int CirclePermanentStopOnWinConsecutive { get; set; } = 999;
        public int CirclePermanentStopOnLoseConsecutive { get; set;} = 999;
        public double CirclePermanentStopAtWinMoney { get; set; } = 1000000000;
        public double CirclePermanentStopAtLoseMoney { get; set; } = 1000000000;
        public int CirclePauseOnWinCount { get; set; } = 1;
        public int CirclePauseOnWinConsecutive { get; set; } = 999;//logic < PermanentStop
        public int CirclePauseOnLoseCount { get; set; } = 1;
        public int CirclePauseOnLoseConsecutive { get; set; } = 999;//logic < PermanentStop
        public int CircleInvertedOnWinCount { get; set; } = 1;
        public int CircleInvertedOnWinConsecutive { get; set; } = 999;//logic < PermanentStop
        public int CircleInvertedOnLoseCount { get; set; } = 1;
        public int CircleInvertedOnLoseConsecutive { get; set; } = 999;//logic < PermanentStop

        //global

        public double StopAutoAtWinMoney { get; set; } = 1000000000;
        public double StopAutoAtLoseMoney { get; set; } = 1000000000;
        public int InvertedOnWinCount { get; set; } = 1;
        public int InvertedOnWinConsecutive { get; set; } = 999;
        public int InvertedOnLoseCount { get; set; } = 1;
        public int InvertedOnLoseConsecutive { get; set; } = 999;
        public bool InvertedAll { get; set; } = false;
        public int PauseOnWinCount { get; set; } = 1;
        public int PauseOnWinConsecutive { get; set; } = 999;
        public int PauseOnLoseCount { get; set; } = 1;
        public int PauseOnLoseConsecutive { get; set; } = 999;

    }
}
