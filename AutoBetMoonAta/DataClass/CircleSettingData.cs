﻿using System.Collections.Generic;

namespace AutoBetMoonAta.DataClass
{
    public class CircleSettingData
    {
        public int ResetWin { get; set; } = 99;
        public int ResetLose { get; set; } = 99;
        public bool IsSplitMoneyFlow { get; set; } = false;
        public List<string> CircleDatas { get; set; } = new List<string>();
    }
}
