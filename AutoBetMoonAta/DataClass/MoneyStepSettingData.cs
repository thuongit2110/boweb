﻿using AutoBetMoonAta.Cores.Enums;
using AutoBetMoonAta.Cores.Utils;

namespace AutoBetMoonAta.DataClass
{
    public enum MoneyStepMarkType
    {
        /// <summary>
        /// Có thể tăng khi lãi max
        /// </summary>
        [Name("Có thể tăng khi lãi max")]
        UpToMax,

        /// <summary>
        /// Mốc ban đầu có thể tăng giảm sau mỗi phiên
        /// </summary>
        [Name("Mốc ban đầu có thể tăng giảm sau mỗi phiên")]
        CanUpDownPerSession,

        /// <summary>
        /// Mốc ban đầu luôn cố định, dòng tiền tùy ý chỉnh sửa
        /// </summary>
        [Name("Mốc ban đầu luôn cố định, dòng tiền tùy ý chỉnh sửa")]
        Permanent
    }


    public class MoneyStepSettingData
    {
        public MoneyStepType MoneyStepType { get; set; } = MoneyStepType.MultiplierLose;

        public MoneyStepMarkType MoneyStepMarkType { get; set; } = MoneyStepMarkType.UpToMax;

        public MoneyIndexAdvanced MoneyIndexAdvanced { get; set; } = MoneyIndexAdvanced.UpLostDownWin;

        public int Count { get; set; } = 3;
        public double BaseFunds { get; set; } = 0;
        public double Multi { get; set; } = 1.1;

        public double UpToMaxDivide { get; set; } = 1;
        public double CanUpDownPerSessionDivide { get; set; } = 1;



        public int MoneyIndexAdvancedStart { get; set; } = 1;
        public int MoneyIndexAdvancedIncrease { get; set; } = 1;
        public int MoneyIndexAdvancedDecrease { get; set; } = 1;


        public double MoneyResultUpDownMulti { get; set; } = 1.0;
        public int MoneyResultUpDownSessionCount { get; set; } = 2;
    }
}
