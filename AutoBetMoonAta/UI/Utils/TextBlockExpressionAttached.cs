﻿using AutoBetMoonAta.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using System.Xml;

namespace AutoBetMoonAta.UI
{
    //https://stackoverflow.com/a/18076638/5034139
    public static class TextBlockExpressionAttached
    {
        public static readonly DependencyProperty FormattedTextProperty = DependencyProperty.RegisterAttached(
            "FormattedText",
            typeof(string),
            typeof(TextBlockExpressionAttached),
            new FrameworkPropertyMetadata(string.Empty,FrameworkPropertyMetadataOptions.AffectsRender, FormattedTextPropertyChanged));

        public static void SetFormattedText(DependencyObject textBlock, string value)
        {
            textBlock.SetValue(FormattedTextProperty, value);
        }

        public static string GetFormattedText(DependencyObject textBlock)
        {
            return (string)textBlock.GetValue(FormattedTextProperty);
        }

        private static void FormattedTextPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var textBlock = d as TextBlock;
            if (textBlock == null)
            {
                return;
            }

            CircleVM circleVM = textBlock.DataContext as CircleVM;
            if (circleVM != null)
            {
                string Action = circleVM.Action;
                int ActionResultCount = circleVM.ActionResultCount;
                uint ActionResult = circleVM.ActionActive;
                int ActionLength = circleVM.Action.Length;

                Brush brushUp = Application.Current.Resources["App.ColorUp"] as Brush;
                Brush brushDown = Application.Current.Resources["App.ColorDown"] as Brush;


                textBlock.Inlines.Clear();
                Queue<char> chars = new Queue<char>();
                for (int i = 0; i < ActionLength; i++)
                {
                    //bool isUp = Action[i] == 'B'; //((Action >> i) & 1) == 1;
                    if (ActionResultCount > 0 && ActionResultCount % ActionLength == 0)
                    {
                        bool isWin = ((ActionResult >> i) & 1) == 1;
                        Run _run = new Run(Action[i].ToString());
                        _run.Foreground = isWin ? brushUp : brushDown;
                        textBlock.Inlines.Add(_run);
                        //sb.Append($"<Run Foreground=\"{(isWin ? "Blue" : "Red")}\" >{(isUp ? "B" : "S")}</Run>");
                    }
                    else if (ActionResultCount == i)
                    {
                        Run _run = new Run(Action[i].ToString());
                        _run.Foreground = Brushes.Blue;
                        textBlock.Inlines.Add(_run);
                    }
                    else
                    {
                        if (i < ActionResultCount % ActionLength)
                        {
                            bool isWin = ((ActionResult >> i) & 1) == 1;
                            Run _run = new Run(Action[i].ToString());
                            _run.Foreground = isWin ? brushUp : brushDown;
                            textBlock.Inlines.Add(_run);
                            //sb.Append($"<Run Foreground=\"{(isWin ? "Blue" : "Red")}\" >{(isUp ? "B" : "S")}</Run>");
                        }
                        else
                        {
                            //chars.Push(isUp ? 'B' : 'S');
                            chars.Enqueue(Action[i]);
                        }
                    }
                }
                if (chars.Count > 0)
                {
                    Run run = new Run(new string(chars.ToArray()));
                    textBlock.Inlines.Add(run);
                }
            }

            //var formattedText = (string)e.NewValue ?? string.Empty;
            //formattedText = string.Format("<Span xml:space=\"preserve\" xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\">{0}</Span>", formattedText);

            //textBlock.Inlines.Clear();
            //using (var xmlReader = XmlReader.Create(new StringReader(formattedText)))
            //{
            //    var result = (Span)XamlReader.Load(xmlReader);
            //    textBlock.Inlines.Add(result);
            //}
        }
    }
}
