﻿using AutoBetMoonAta.UI.ViewModels;
using MahApps.Metro.Controls;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutoBetMoonAta.UI
{
    /// <summary>
    /// Interaction logic for EmulatorConfigWindow.xaml
    /// </summary>
    public partial class EmulatorConfigWindow : MetroWindow
    {
        static readonly Regex regex = new Regex("^[BS]+$");
        readonly EmulatorConfigWVM emulatorConfigWVM;
        public bool IsStart { get; private set; } = false;
        internal EmulatorConfigWindow(AccountVM accountVM)
        {
            InitializeComponent();
            this.emulatorConfigWVM = this.DataContext as EmulatorConfigWVM;
            emulatorConfigWVM.AccountVM = accountVM;
        }

        private void btn_loadFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog()
            {
                Filter = "txt|*.txt|all file|*.*",
                Multiselect = false,
                InitialDirectory = Directory.GetCurrentDirectory(),
            };
            if (ofd.ShowDialog() == true)
            {
                var texts = File.ReadAllLines(ofd.FileName).Select(x => x.Trim());
                emulatorConfigWVM.EmulatorData = string.Join("", texts);
            }
        }

        private void btn_start_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(emulatorConfigWVM.EmulatorData))
            {
                MessageBox.Show("Có dữ liệu không hợp lệ", "Lỗi");
                return;
            }
            Match match = regex.Match(emulatorConfigWVM.EmulatorData);
            if (match.Success)
            {
                IsStart = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Có dữ liệu không hợp lệ", "Lỗi");
            }
        }

        private void btn_stop_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_download_Click(object sender, RoutedEventArgs e)
        {
            DownloadCandlesHistory downloadCandlesHistory = new DownloadCandlesHistory(false);
            downloadCandlesHistory.Owner = this;
            downloadCandlesHistory.ShowDialog();
            if (!string.IsNullOrWhiteSpace(downloadCandlesHistory.HistoryData))
            {
                emulatorConfigWVM.EmulatorData = downloadCandlesHistory.HistoryData;
            }
            else emulatorConfigWVM.EmulatorData = string.Empty;
        }
    }
}
