﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AutoBetMoonAta.DataClass;
using AutoBetMoonAta.Queue;
using AutoBetMoonAta.UI.ViewModels;
using MahApps.Metro.Controls;
using MoonAtaApi;
using TqkLibrary.Net.Captcha;
using TqkLibrary.Queues.TaskQueues;

namespace AutoBetMoonAta.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        readonly MainWVM mainWVM;
        public MainWindow()
        {
            InitializeComponent();
            this.mainWVM = this.DataContext as MainWVM;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (mainWVM.Accounts.Count == 0) mainWVM.Accounts.Add(new AccountVM(new AccountVMData(), mainWVM.WriteLog));
            lv_accounts.SelectedIndex = 0;
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(MessageBox.Show("Bạn có muốn tắt tool?","Xác nhận thoát", MessageBoxButton.YesNo,MessageBoxImage.Question) == MessageBoxResult.No)
            {
                e.Cancel = true;
            }
        }

        public async void lv_accounts_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;
            MenuVM menuVM = menuItem.DataContext as MenuVM;
            var selectedItems = lv_accounts.SelectedItems.OfType<AccountVM>().ToList();

            try
            {
                switch (menuVM.Action)
                {
                    case MenuAction.Config:
                        {
                            var item = selectedItems.FirstOrDefault();
                            if (item != null)
                            {
#if !DEBUG
                                if (!item.IsLogined) await LoginMoonAta(item);
#endif
                                if (
#if !DEBUG
                                    item.IsLogined && 
#endif
                                    !item.IsOpenAccountConfigWindow)
                                {
                                    AccountConfigWindow accountConfigWindow = new AccountConfigWindow(item);
                                    accountConfigWindow.UpdateMinThreadPool += this.AccountConfigWindow_UpdateMinThreadPool;
                                    accountConfigWindow.LogEvent += (log) => mainWVM.Logs.Add(log);
                                    accountConfigWindow.Owner = this;
                                    accountConfigWindow.Show();
                                }
                            }
                            break;
                        }

                    case MenuAction.AddAccount:
                        {
                            mainWVM.Accounts.Add(new AccountVM(new AccountVMData(), mainWVM.WriteLog));
                            break;
                        }

                    case MenuAction.DeteteAccount:
                        {
                            if (MessageBox.Show("Xóa account đã chọn?", "Xác nhận", MessageBoxButton.OKCancel, MessageBoxImage.Warning) == MessageBoxResult.OK)
                            {
                                selectedItems.ForEach(x =>
                                {
                                    Singleton.WorkQueues.Cancel(y => y.AccountVM == x);
                                    mainWVM.Accounts.Remove(x);
                                });
                            }
                            break;
                        }


                    case MenuAction.AccountSetting:
                        {
                            var item = selectedItems.FirstOrDefault();
                            if (item != null)
                            {
                                EditAccountWindow addAccountWindow = new EditAccountWindow(item);
                                addAccountWindow.Owner = this;
                                addAccountWindow.ShowDialog();
                                item.Refresh();
                            }
                            break;
                        }

                    case MenuAction.Login:
                        {
                            var item = selectedItems.FirstOrDefault();
                            if (item != null) await LoginMoonAta(item);
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, ex.GetType().FullName);
            }
        }

        private void AccountConfigWindow_UpdateMinThreadPool()
        {
            int min = Environment.ProcessorCount + mainWVM.Accounts.Sum(x => x.CircleSettingUCVM.ThreadsNeed);
            ThreadPool.SetMinThreads(min, min);
        }

        private async void btn_Logout_Click(object sender, RoutedEventArgs e)
        {
            Singleton.Setting.Setting.IsAutoLogin = false;
            Singleton.Setting.Save();
            await Task.Delay(500);
            this.Close();
        }

        private void btn_addAccount_Click(object sender, RoutedEventArgs e)
        {
            mainWVM.Accounts.Add(new AccountVM(new AccountVMData(), mainWVM.WriteLog));
        }

        private async void btn_loginMoonAta_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            AccountVM accountVM = button.DataContext as AccountVM;
            await LoginMoonAta(accountVM);
        }




        async Task LoginMoonAta(AccountVM accountVM)
        {
            try
            {
                bool isLogined = false;
                ApiResponse<ProfileResponse> profile = null;
                try
                {
                    if (!string.IsNullOrWhiteSpace(accountVM.Data.Token?.Token))
                    {
                        accountVM.AutoData.MoonAtaClient.UpdateToken(accountVM.Data.Token);
                        profile = await accountVM.AutoData.MoonAtaClient.Profile();
                        isLogined = true;
                    }
                }
                catch
                {
                    isLogined = false;
                }

                if (!isLogined)
                {
                    if (accountVM.Data.IsAutoLogin)
                    {
                        accountVM.AutoStatus = "Đang giải captcha và đăng nhập";
                        await accountVM.AutoData.MoonAtaClient.Login(
                            accountVM.Data.Email,
                            accountVM.Data.Password,
                            () => ApiHepler.ResolveCaptcha(),
                            () => Singleton.TwoFactorAuth.GetCode(accountVM.Data.TwoFA));
                    }
                    else
                    {
                        WebLoginWindow webLoginWindow = new WebLoginWindow();
                        webLoginWindow.ShowDialog();
                        if (!string.IsNullOrWhiteSpace(webLoginWindow.Token?.Token))
                        {
                            accountVM.AutoData.MoonAtaClient.UpdateToken(webLoginWindow.Token);
                        }
                        else return;
                    }
                    profile = await accountVM.AutoData.MoonAtaClient.Profile();
                }
//#if !DEBUG
                accountVM.AutoStatus = "Kiểm tra thông tin tài khoản";
                if (profile.Ok)
                {
                    await ApiHepler.CheckAccount(profile.Data.nn);
                }
                else throw new Exception("Đọc thông tin account thất bại");
//#endif
                accountVM.Data.NickName = profile.Data.nn;
                accountVM.Refresh();

                var balance = await accountVM.AutoData.MoonAtaClient.SpotBalance();
                accountVM.SpotBalance = balance.Data;

                accountVM.AutoStatus = "Lấy lịch sử nến và khởi tạo kết nối websocket";
                var prices = await accountVM.AutoData.MoonAtaClient.Prices_Adv();
                prices.Where(x => !x.IsBetSession).ToList().ForEach(x => accountVM.AutoData.ServerResultHistory.Push(x.IsGreen));

                accountVM.AutoStatus = "Khởi tạo kết nối websocket";
                accountVM.AutoData.WebSocketHepler.TaskWebsocket(await accountVM.AutoData.MoonAtaClient.InitWebSocket());
                accountVM.AutoStatus = "Đăng nhập hoàn tất";
                accountVM.IsLogined = true;
            }
            catch (ApiException apiex)
            {
                accountVM.WriteLog($"LoginMoonAta [{apiex.GetType().FullName}] {apiex.Message} {apiex.StackTrace}");
                accountVM.AutoStatus = $"Lỗi: {apiex.Message}";
                MessageBox.Show("Tài khoản không nằm trong danh sách hệ thống của Auto789.net vui lòng đăng ký tài khoản để sử dụng Auto.", "Lỗi", MessageBoxButton.OK, MessageBoxImage.Error);
                using var p = Process.Start(Singleton.WebRegister);
            }
            catch (Exception ex)
            {
                if (ex is AggregateException ae) ex = ae.InnerException;
                accountVM.WriteLog($"LoginMoonAta [{ex.GetType().FullName}] {ex.Message} {ex.StackTrace}");
                accountVM.AutoStatus = $"Lỗi: {ex.Message}";
                accountVM.IsLogined = false;
                MessageBox.Show(ex.Message, ex.GetType().FullName, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btn_register_Click(object sender, RoutedEventArgs e)
        {
            using var p = Process.Start(Singleton.WebRegister);
        }
    }
}
