﻿using AutoBetMoonAta.Queue;
using AutoBetMoonAta.UI.ViewModels;
using MahApps.Metro.Controls;
using MoonAtaApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutoBetMoonAta.UI
{
    /// <summary>
    /// Interaction logic for AccountConfigWindow.xaml
    /// </summary>
    public partial class AccountConfigWindow : MetroWindow
    {
        public event Action<string> LogEvent;
        readonly AccountConfigVM accountConfigVM;
        public event Action UpdateMinThreadPool;
        internal AccountConfigWindow(AccountVM accountVM)
        {
            if (accountVM == null) throw new ArgumentNullException(nameof(accountVM));
            InitializeComponent();
            this.accountConfigVM = this.DataContext as AccountConfigVM;
            this.accountConfigVM.AccountVM = accountVM;
            accountVM.IsOpenAccountConfigWindow = true;
        }
        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {

        }
        private void Window_Closed(object sender, EventArgs e)
        {
            accountConfigVM.AccountVM.IsOpenAccountConfigWindow = false;
        }

        private void btn_start_stop_Auto_Click(object sender, RoutedEventArgs e)
        {
            if (accountConfigVM.AccountVM.IsWorking)
            {
                Singleton.WorkQueues.Cancel(x => x.AccountVM.Equals(accountConfigVM.AccountVM));
            }
            else
            {
                if (accountConfigVM.AccountVM.IsTestMode)
                {
                    Singleton.WorkQueues.Add(new TestQueue(accountConfigVM.AccountVM));
                }
                else
                {
                    Singleton.WorkQueues.Add(new WorkQueue(accountConfigVM.AccountVM));
                }
            }
        }

        private void btn_emulator_Click(object sender, RoutedEventArgs e)
        {
            if (accountConfigVM.AccountVM.IsWorking)
            {

            }
            else
            {
                EmulatorConfigWindow emulatorConfigWindow = new EmulatorConfigWindow(accountConfigVM.AccountVM);
                emulatorConfigWindow.Owner = this;
                emulatorConfigWindow.ShowDialog();
                if (emulatorConfigWindow.IsStart)
                {
                    accountConfigVM.AccountVM.IsTestMode = true;
                    accountConfigVM.AccountVM.ServerResultHistoryString = accountConfigVM.AccountVM.AutoData.TestResultHistory.ToString();
                }
            }
        }

        private void btn_reset_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Reset toàn bộ?", "Xác nhận", MessageBoxButton.OKCancel, MessageBoxImage.Warning) == MessageBoxResult.OK)
            {
                accountConfigVM.AccountVM.Reset();
            }
        }

        private void btn_advanced_Click(object sender, RoutedEventArgs e)
        {
            AdvancedConfigWindow advancedConfigWindow = new AdvancedConfigWindow(accountConfigVM.AccountVM);
            advancedConfigWindow.Owner = this;
            advancedConfigWindow.ShowDialog();
        }

        private void btn_editAccount_Click(object sender, RoutedEventArgs e)
        {
            EditAccountWindow editAccountWindow = new EditAccountWindow(this.accountConfigVM.AccountVM);
            editAccountWindow.Owner = this;
            editAccountWindow.ShowDialog();
        }

        private void CircleSettingUc_UpdateMinThreadPool()
        {
            UpdateMinThreadPool?.Invoke();
        }
    }
}
