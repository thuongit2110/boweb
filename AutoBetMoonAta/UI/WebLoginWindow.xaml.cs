﻿using AutoBetMoonAta.Cefs;
using MahApps.Metro.Controls;
using MoonAtaApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutoBetMoonAta.UI
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class WebLoginWindow : MetroWindow
    {
        public MoonAtaToken Token { get; private set; }
        readonly CustomRequestHandler customRequestHandler = new CustomRequestHandler();
        public WebLoginWindow()
        {
            InitializeComponent();
            chrome.Initialized += this.Chrome_Initialized;
            chrome.Loaded += this.Chrome_Loaded;
            chrome.RequestHandler = customRequestHandler;
            customRequestHandler.OnTokenReceived += CustomRequestHandler_OnTokenReceived;
        }

        private void CustomRequestHandler_OnTokenReceived(MoonAtaToken obj)
        {
            Token = obj;
            if(!string.IsNullOrEmpty(obj?.Token))
            {
                if (Dispatcher.CheckAccess()) Close();
                else Dispatcher.Invoke(Close);
            }
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            await chrome.LoadUrlAsync(Singleton.WebLogin);
        }

        private void Chrome_Initialized(object sender, EventArgs e)
        {
            Clear();
        }

        private void Chrome_Loaded(object sender, RoutedEventArgs e)
        {
            //Clear();
        }

        void Clear()
        {
            chrome.GetBrowser().MainFrame.ExecuteJavaScriptAsync("localStorage.clear();sessionStorage.clear();", Singleton.WebLogin);
            CefSharp.Cef.GetGlobalCookieManager().DeleteCookies();
        }

    }
}