﻿using AutoBetMoonAta.UI.ViewModels;
using BaseSource.ViewModels.Common;
using BaseSource.ViewModels.User;
using MahApps.Metro.Controls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutoBetMoonAta.UI
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : MetroWindow
    {
        readonly LoginWVM loginWVM;
        public bool IsLogined { get; private set; } = false;
        public LoginWindow()
        {
            InitializeComponent();
            this.loginWVM = this.DataContext as LoginWVM;

        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            pb.Password = Singleton.Setting.Setting.Password;
            tb_userName.Focus();
            Keyboard.Focus(tb_userName);
            if (Singleton.Setting.Setting.IsAutoLogin) Login();
        }

        private void btn_login_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        private void btn_register_Click(object sender, RoutedEventArgs e)
        {
            using Process process = Process.Start(Singleton.WebUri);
        }

        private void pb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && e.KeyStates == KeyStates.Down)
            {
                Login();
            }
        }

        private void tb_userName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && e.KeyStates == KeyStates.Down)
            {
                Login();
            }
        }


        async void Login()
        {
            if (string.IsNullOrWhiteSpace(tb_userName.Text) || string.IsNullOrWhiteSpace(pb.Password))
            {
#if DEBUG
                IsLogined = true;
                this.Close();
#else
                MessageBox.Show("Tên đăng nhập hoặc mật khẩu không được để trống", "Lỗi");
#endif
                return;
            }
            try
            {
                Mouse.OverrideCursor = Cursors.Wait;
                var token = await ApiHepler.Login(tb_userName.Text, pb.Password);
                if (token.IsSuccessed)
                {
                    Singleton.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.ResultObj);
                    Singleton.Setting.Setting.Password = pb.Password;
                    Singleton.Setting.Save();
                    IsLogined = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show(string.Join(Environment.NewLine, token.ValidationErrors.Select(x => x.Error)), "Lỗi");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                Mouse.OverrideCursor = Cursors.Arrow;
            }
        }

    }
}
