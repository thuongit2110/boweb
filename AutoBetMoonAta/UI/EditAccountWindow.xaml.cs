﻿using AutoBetMoonAta.UI.ViewModels;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TwoFactorAuthNet;
namespace AutoBetMoonAta.UI
{
    /// <summary>
    /// Interaction logic for AddAccountWindow.xaml
    /// </summary>
    public partial class EditAccountWindow : MetroWindow
    {
        static readonly TwoFactorAuth twoFactorAuth = new TwoFactorAuth();
        readonly EditAccountWVM addAccountWVM;
        public string Email
        {
            get { return addAccountWVM.Email; }
            private set { addAccountWVM.Email = value; }
        }
        public string Password
        {
            get { return pb.Password; }
            private set { pb.Password = value; }
        }
        public string TwoFA
        {
            get { return addAccountWVM.TwoFA; }
            private set { addAccountWVM.TwoFA = value; }
        }
        public bool IsAutoLogin
        {
            get { return addAccountWVM.IsAutoLogin; }
            private set { addAccountWVM.IsAutoLogin = value; }
        }


        internal EditAccountWindow(AccountVM accountVM)
        {
            this.InitializeComponent();
            this.addAccountWVM = this.DataContext as EditAccountWVM;
            this.addAccountWVM.AccountVM = accountVM;
            this.Password = accountVM.Data.Password;
            this.Email = accountVM.Data.Email;
            this.TwoFA = accountVM.Data.TwoFA;
            this.IsAutoLogin = accountVM.Data.IsAutoLogin;
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            if(this.IsAutoLogin && string.IsNullOrWhiteSpace(this.Email) || string.IsNullOrWhiteSpace(this.Password))
            {
                MessageBox.Show("Tên đăng nhập hoặc mật khẩu trống", "Lỗi");
                return;
            }

            if(!string.IsNullOrWhiteSpace(this.TwoFA))
            {
                try
                {
                    string facode = twoFactorAuth.GetCode(this.TwoFA);
                }
                catch
                {
                    MessageBox.Show("2FA code không hợp lệ", "Lỗi");
                    return;
                }
            }
            this.addAccountWVM.AccountVM.Data.Email = this.Email;
            this.addAccountWVM.AccountVM.Data.Password = this.Password;
            this.addAccountWVM.AccountVM.Data.TwoFA = this.TwoFA;
            this.addAccountWVM.AccountVM.Data.IsAutoLogin = this.IsAutoLogin;
            this.addAccountWVM.AccountVM.Save();
            this.Close();
        }
    }
}
//24046035