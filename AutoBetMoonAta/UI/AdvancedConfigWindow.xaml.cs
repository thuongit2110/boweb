﻿using AutoBetMoonAta.UI.ViewModels;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutoBetMoonAta.UI
{
    /// <summary>
    /// Interaction logic for AdvancedConfigWindow.xaml
    /// </summary>
    public partial class AdvancedConfigWindow : MetroWindow
    {
        readonly AdvancedConfigWVM advancedConfigWVM;
        internal AdvancedConfigWindow(AccountVM accountVM)
        {
            InitializeComponent();
            this.advancedConfigWVM = this.DataContext as AdvancedConfigWVM;
            this.advancedConfigWVM.AccountVM = accountVM;
        }
    }
}
