﻿using AutoBetMoonAta.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AutoBetMoonAta.UI
{
    /// <summary>
    /// Interaction logic for MoneyStepSettingUC.xaml
    /// </summary>
    public partial class MoneyStepSettingUC : UserControl
    {
        public static readonly DependencyProperty MoneyStepSettingProperty = DependencyProperty.Register(
                nameof(MoneyStepSetting),
                typeof(MoneyStepSettingUCVM),
                typeof(MoneyStepSettingUC),
                new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        internal MoneyStepSettingUCVM MoneyStepSetting
        {
            get { return (MoneyStepSettingUCVM)GetValue(MoneyStepSettingProperty); }
            set { SetValue(MoneyStepSettingProperty, value); }
        }

        public MoneyStepSettingUC()
        {
            InitializeComponent();
        }
    }
}
