﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TqkLibrary.WpfUi;

namespace AutoBetMoonAta.UI.ViewModels
{
    internal class EmulatorConfigWVM : BaseViewModel
    {
        AccountVM _AccountVM;
        public AccountVM AccountVM
        {
            get { return _AccountVM; }
            set
            {
                _AccountVM = value;
                NotifyPropertyChange();
                NotifyPropertyChange(nameof(EmulatorData));
                NotifyPropertyChange(nameof(IsEmunatorLoop));
                NotifyPropertyChange(nameof(TestBalance));
            }
        }

        public string EmulatorData
        {
            get { return AccountVM?.Data?.EmulatorData; }
            set { AccountVM.Data.EmulatorData = value.ToUpper(); NotifyPropertyChange(); AccountVM.Save(); }
        }

        public int EmulatorDelay
        {
            get { return Singleton.Setting.Setting.EmulatorDelay; }
            set { Singleton.Setting.Setting.EmulatorDelay = value; NotifyPropertyChange(); Singleton.Setting.Save(); }
        }

        public bool? IsEmunatorLoop
        {
            get { return AccountVM?.Data?.IsEmunatorLoop; }
            set { AccountVM.Data.IsEmunatorLoop = value == true; NotifyPropertyChange(); AccountVM.Save(); }
        }

        public double? TestBalance
        {
            get { return AccountVM?.TestBalanced; }
            set
            {
                if (value.HasValue)
                {
                    AccountVM.TestBalanced = value.Value;
                    NotifyPropertyChange();
                }
            }
        }
    }
}
