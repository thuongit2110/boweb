﻿using MoonAtaApi;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TqkLibrary.WpfUi;
using TqkLibrary.WpfUi.ObservableCollection;

namespace AutoBetMoonAta.UI.ViewModels
{
    public enum MoonAtaBetTypeVM : int
    {
        Buy,//Up
        Sell,//Down
        Skip,
    }
    internal class TransactionHistoryVM : BaseViewModel
    {
        internal TransactionHistoryVM(BoReceived boReceived, TransactionItem transactionItem, bool isReverseBet = false)
        {
            this.IsReverseBet = isReverseBet;
            this.Session = boReceived.Session;
            if (transactionItem == null)
            {
                this.BetType = MoonAtaBetTypeVM.Skip;
                this.RealBetType = MoonAtaBetTypeVM.Skip;
                this.BetTime = DateTime.Now;
            }
            else
            {
                this.TransactionId = transactionItem.transactionId;
                this.RealBetType = (MoonAtaBetTypeVM)transactionItem.betType;
                this.BetType = transactionItem.betType switch
                {
                    MoonAtaBetType.UP => isReverseBet ? MoonAtaBetTypeVM.Sell : MoonAtaBetTypeVM.Buy,
                    MoonAtaBetType.DOWN => isReverseBet ? MoonAtaBetTypeVM.Buy : MoonAtaBetTypeVM.Sell,
                    _ => throw new Exception()
                };

                this.TransactionAmount = transactionItem.betAmount;
                DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeMilliseconds(transactionItem.createdDatetime);
                this.BetTime = dateTimeOffset.LocalDateTime;
            }
        }
        internal TransactionHistoryVM(MoonAtaBetTypeVM moonAtaBetTypeVM, double? transactionAmount, bool isReverseBet = false)
        {
            this.IsReverseBet = isReverseBet;
            this.BetType = moonAtaBetTypeVM switch
            {
                MoonAtaBetTypeVM.Buy => MoonAtaBetTypeVM.Sell,
                MoonAtaBetTypeVM.Sell => MoonAtaBetTypeVM.Buy,
                _ => MoonAtaBetTypeVM.Skip
            };
            this.TransactionAmount = transactionAmount;
            this.RealBetType = moonAtaBetTypeVM;

        }



        public string TransactionId { get; }

        int _Index = 0;
        public int Index
        {
            get { return _Index; }
            private set { _Index = value; NotifyPropertyChange(); }
        }
        public long? Session { get; }
        public MoonAtaBetTypeVM BetType { get; }
        public MoonAtaBetTypeVM RealBetType { get; }
        public double? TransactionAmount { get; }
        public DateTime? BetTime { get; }
        public bool IsReverseBet { get; }

        TransactionResult? _TransactionResult = null;
        public TransactionResult? TransactionResult
        {
            get { return _TransactionResult; }
            set { _TransactionResult = value; NotifyPropertyChange(); }
        }

        public bool ServerResultIsUp { get; set; }


        public override string ToString()
        {
            return ServerResultIsUp ? "B" : "S";
        }


        internal class TransactionHistoryVMObservableCollection : DispatcherObservableCollection<TransactionHistoryVM>
        {
            public Task InsertTopAsync(TransactionHistoryVM transactionHistoryVM)
            {
                return this.Dispatcher.InvokeAsync(() => this.Insert(0, transactionHistoryVM)).Task;
            }
            protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
            {
                int count = this.Count;
                for (int i = 0; i < count; i++) this[i].Index = count - i;
                base.OnCollectionChanged(e);
            }
        }
    }


    internal class TransactionHistoryVMProvider : TransactionHistoryVM.TransactionHistoryVMObservableCollection, IItemsProvider<TransactionHistoryVM>
    {
        public int FetchCount()
        {
            return this.Count;
        }

        public IList<TransactionHistoryVM> FetchRange(int startIndex, int count)
        {
            return this.Skip(startIndex).Take(count).ToList();
        }
    }
}
