﻿using AutoBetMoonAta.Cores.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TqkLibrary.WpfUi;

namespace AutoBetMoonAta.UI.ViewModels
{
    internal class MenuVM
    {
        public MenuVM(MenuAction action)
        {
            this.Action = action;
            this.ActionText = action.GetAttribute<NameAttribute>().Name;
        }

        public MenuAction Action { get; }
        public string ActionText { get; }
    }
}
