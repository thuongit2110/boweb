﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TqkLibrary.WpfUi;

namespace AutoBetMoonAta.UI.ViewModels
{
    class AdvancedConfigWVM : BaseViewModel
    {
        AccountVM _AccountVM;
        public AccountVM AccountVM
        {
            get { return _AccountVM; }
            set { _AccountVM = value; NotifyPropertyChange(); }
        }
    }
}
