﻿using AutoBetMoonAta.DataClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TqkLibrary.WpfUi;
using TqkLibrary.WpfUi.ObservableCollection;

namespace AutoBetMoonAta.UI.ViewModels
{
    internal class MainWVM : BaseViewModel
    {
        public MainWVM()
        {
            Accounts = new SaveObservableCollection<AccountVMData, AccountVM>(Singleton.ExeDir + "\\Accounts.json", x => new AccountVM(x,WriteLog));
        }
        public SaveObservableCollection<AccountVMData, AccountVM> Accounts { get; }

        public List<MenuVM> AccountMenus { get; } = new List<MenuVM>()
        {
            new MenuVM(MenuAction.Config),
            //new MenuVM(MenuAction.AccountSetting),
            null,
            new MenuVM(MenuAction.AddAccount),
            null,
            new MenuVM(MenuAction.DeteteAccount)
        };
        public LimitObservableCollection<string> Logs { get; }
            = new LimitObservableCollection<string>(() => Singleton.LogDir + $"\\{DateTime.Now:yyyy-MM-dd}.log");

        public void WriteLog(string log)
        {
            Logs.Add($"[{DateTime.Now:HH:mm:ss.fff}] {log}");
        }
    }
}
