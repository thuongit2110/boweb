﻿using AutoBetMoonAta.Cores.Interfaces;
using TqkLibrary.WpfUi;

namespace AutoBetMoonAta.UI.ViewModels
{
    internal class AdvancedConfigUCVM : BaseViewModel, IAdvancedConfigUCVM
    {
        public AccountVM AccountVM { get; }
        internal AdvancedConfigUCVM(AccountVM accountVM)
        {
            this.AccountVM = accountVM;
        }

        public int BetTimeMax
        {
            get { return AccountVM.Data.AdvancedSetting.BetTimeMax; }
            set { AccountVM.Data.AdvancedSetting.BetTimeMax = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int BetTimeMin
        {
            get { return AccountVM.Data.AdvancedSetting.BetTimeMin; }
            set { AccountVM.Data.AdvancedSetting.BetTimeMin = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public bool IsAllowBet
        {
            get { return AccountVM.Data.AdvancedSetting.IsAllowBet; }
            set { AccountVM.Data.AdvancedSetting.IsAllowBet = value; NotifyPropertyChange(); AccountVM.Save(); }
        }



        //Circle
        public int CirclePermanentStopOnWinConsecutive
        {
            get { return AccountVM.Data.AdvancedSetting.CirclePermanentStopOnWinConsecutive; }
            set { AccountVM.Data.AdvancedSetting.CirclePermanentStopOnWinConsecutive = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int CirclePermanentStopOnLoseConsecutive
        {
            get { return AccountVM.Data.AdvancedSetting.CirclePermanentStopOnLoseConsecutive; }
            set { AccountVM.Data.AdvancedSetting.CirclePermanentStopOnLoseConsecutive = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public double CirclePermanentStopAtWinMoney
        {
            get { return AccountVM.Data.AdvancedSetting.CirclePermanentStopAtWinMoney; }
            set { AccountVM.Data.AdvancedSetting.CirclePermanentStopAtWinMoney = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public double CirclePermanentStopAtLoseMoney
        {
            get { return AccountVM.Data.AdvancedSetting.CirclePermanentStopAtLoseMoney; }
            set { AccountVM.Data.AdvancedSetting.CirclePermanentStopAtLoseMoney = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int CirclePauseOnWinCount
        {
            get { return AccountVM.Data.AdvancedSetting.CirclePauseOnWinCount; }
            set { AccountVM.Data.AdvancedSetting.CirclePauseOnWinCount = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int CirclePauseOnWinConsecutive
        {
            get { return AccountVM.Data.AdvancedSetting.CirclePauseOnWinConsecutive; }
            set { AccountVM.Data.AdvancedSetting.CirclePauseOnWinConsecutive = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int CirclePauseOnLoseCount
        {
            get { return AccountVM.Data.AdvancedSetting.CirclePauseOnLoseCount; }
            set { AccountVM.Data.AdvancedSetting.CirclePauseOnLoseCount = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int CirclePauseOnLoseConsecutive
        {
            get { return AccountVM.Data.AdvancedSetting.CirclePauseOnLoseConsecutive; }
            set { AccountVM.Data.AdvancedSetting.CirclePauseOnLoseConsecutive = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int CircleInvertedOnWinCount
        {
            get { return AccountVM.Data.AdvancedSetting.CircleInvertedOnWinCount; }
            set { AccountVM.Data.AdvancedSetting.CircleInvertedOnWinCount = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int CircleInvertedOnWinConsecutive
        {
            get { return AccountVM.Data.AdvancedSetting.CircleInvertedOnWinConsecutive; }
            set { AccountVM.Data.AdvancedSetting.CircleInvertedOnWinConsecutive = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int CircleInvertedOnLoseCount
        {
            get { return AccountVM.Data.AdvancedSetting.CircleInvertedOnLoseCount; }
            set { AccountVM.Data.AdvancedSetting.CircleInvertedOnLoseCount = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int CircleInvertedOnLoseConsecutive
        {
            get { return AccountVM.Data.AdvancedSetting.CircleInvertedOnLoseConsecutive; }
            set { AccountVM.Data.AdvancedSetting.CircleInvertedOnLoseConsecutive = value; NotifyPropertyChange(); AccountVM.Save(); }
        }


        //global
        public double StopAutoAtWinMoney
        {
            get { return AccountVM.Data.AdvancedSetting.StopAutoAtWinMoney; }
            set { AccountVM.Data.AdvancedSetting.StopAutoAtWinMoney = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public double StopAutoAtLoseMoney
        {
            get { return AccountVM.Data.AdvancedSetting.StopAutoAtLoseMoney; }
            set { AccountVM.Data.AdvancedSetting.StopAutoAtLoseMoney = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int InvertedOnWinCount
        {
            get { return AccountVM.Data.AdvancedSetting.InvertedOnWinCount; }
            set { AccountVM.Data.AdvancedSetting.InvertedOnWinCount = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int InvertedOnWinConsecutive
        {
            get { return AccountVM.Data.AdvancedSetting.InvertedOnWinConsecutive; }
            set { AccountVM.Data.AdvancedSetting.InvertedOnWinConsecutive = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int InvertedOnLoseCount
        {
            get { return AccountVM.Data.AdvancedSetting.InvertedOnLoseCount; }
            set { AccountVM.Data.AdvancedSetting.InvertedOnLoseCount = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int InvertedOnLoseConsecutive
        {
            get { return AccountVM.Data.AdvancedSetting.InvertedOnLoseConsecutive; }
            set { AccountVM.Data.AdvancedSetting.InvertedOnLoseConsecutive = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public bool InvertedAll
        {
            get { return AccountVM.Data.AdvancedSetting.InvertedAll; }
            set { AccountVM.Data.AdvancedSetting.InvertedAll = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int PauseOnWinCount
        {
            get { return AccountVM.Data.AdvancedSetting.PauseOnWinCount; }
            set { AccountVM.Data.AdvancedSetting.PauseOnWinCount = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int PauseOnWinConsecutive
        {
            get { return AccountVM.Data.AdvancedSetting.PauseOnWinConsecutive; }
            set { AccountVM.Data.AdvancedSetting.PauseOnWinConsecutive = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int PauseOnLoseCount
        {
            get { return AccountVM.Data.AdvancedSetting.PauseOnLoseCount; }
            set { AccountVM.Data.AdvancedSetting.PauseOnLoseCount = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
        public int PauseOnLoseConsecutive
        {
            get { return AccountVM.Data.AdvancedSetting.PauseOnLoseConsecutive; }
            set { AccountVM.Data.AdvancedSetting.PauseOnLoseConsecutive = value; NotifyPropertyChange(); AccountVM.Save(); }
        }
    }
}
