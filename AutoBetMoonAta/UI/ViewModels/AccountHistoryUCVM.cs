﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TqkLibrary.WpfUi;
using TqkLibrary.WpfUi.ObservableCollection;
namespace AutoBetMoonAta.UI.ViewModels
{
    internal class AccountHistoryUCVM : BaseViewModel
    {
        public AccountVM AccountVM { get; }
        internal AccountHistoryUCVM(AccountVM accountVM)
        {
            this.AccountVM = accountVM;
            TransactionHistorys = new TransactionHistoryVMProvider();
            TransactionHistoryVirtualizing = new AsyncVirtualizingCollection<TransactionHistoryVM>(TransactionHistorys);
        }

        public AsyncVirtualizingCollection<TransactionHistoryVM> TransactionHistoryVirtualizing { get; }
        public TransactionHistoryVMProvider TransactionHistorys { get; }


        int _WinContinuousCount = 0;
        /// <summary>
        /// Win liên tiếp
        /// </summary>
        public int WinContinuousCount
        {
            get { return _WinContinuousCount; }
            set { _WinContinuousCount = value; NotifyPropertyChange(); }
        }


        double _MaxLostProfit = 0;
        /// <summary>
        /// Mức giảm lãi MAX
        /// </summary>
        public double MaxLostProfit
        {
            get { return _MaxLostProfit; }
            set { _MaxLostProfit = value; NotifyPropertyChange(); }
        }

        double _RateProfitDivStart = 0;
        /// <summary>
        /// Tỉ lệ lãi / Tiền start
        /// </summary>
        public double RateProfitDivStart
        {
            get { return _RateProfitDivStart; }
            set { _RateProfitDivStart = value; NotifyPropertyChange(); }
        }

        double _WinCumulativeCount = 0;
        /// <summary>
        /// Win cộng dồn
        /// </summary>
        public double WinCumulativeCount
        {
            get { return _WinCumulativeCount; }
            set { _WinCumulativeCount = value; NotifyPropertyChange(); }
        }

        double _LoseCumulativeCount = 0;
        /// <summary>
        /// Lose cộng dồn
        /// </summary>
        public double LoseCumulativeCount
        {
            get { return _LoseCumulativeCount; }
            set { _LoseCumulativeCount = value; NotifyPropertyChange(); }
        }

        double _FundsStart = 0;
        /// <summary>
        /// Tiền Start
        /// </summary>

        public double FundsStart
        {
            get { return _FundsStart; }
            set { _FundsStart = value; NotifyPropertyChange(); }
        }

        double _ProfitMin = 0;
        /// <summary>
        /// Lãi min
        /// </summary>
        public double ProfitMin
        {
            get { return _ProfitMin; }
            set { _ProfitMin = value; NotifyPropertyChange(); }
        }

        int _LoseMaxCount = 0;
        public int LoseMaxCount
        {
            get { return _LoseMaxCount; }
            set { _LoseMaxCount = value; NotifyPropertyChange(); }
        }

        double _LoseMax = 0;
        /// <summary>
        /// LoseMax
        /// </summary>
        public double LoseMax
        {
            get { return _LoseMax; }
            set { _LoseMax = value; NotifyPropertyChange(); }
        }

        int _LoseContinuousMaxCount = 0;
        public int LoseContinuousMaxCount
        {
            get { return _LoseContinuousMaxCount; }
            set { _LoseContinuousMaxCount = value; NotifyPropertyChange(); }
        }

        int _WinContinuousMaxCount = 0;
        public int WinContinuousMaxCount
        {
            get { return _WinContinuousMaxCount; }
            set { _WinContinuousMaxCount = value; NotifyPropertyChange(); }
        }

        double _LoseContinuousMax = 0;
        /// <summary>
        /// Lose thông max
        /// </summary>
        public double LoseContinuousMax
        {
            get { return _LoseContinuousMax; }
            set { _LoseContinuousMax = value; NotifyPropertyChange(); }
        }

        double _BetMax = 0;
        /// <summary>
        /// Bet Max
        /// </summary>
        public double BetMax
        {
            get { return _BetMax; }
            set { _BetMax = value; NotifyPropertyChange(); }
        }


        double _ProfitMax = 0;
        /// <summary>
        /// Lãi max
        /// </summary>
        public double ProfitMax
        {
            get { return _ProfitMax; }
            set { _ProfitMax = value; NotifyPropertyChange(); }
        }

        int _WinMaxCount = 0;
        public int WinMaxCount
        {
            get { return _WinMaxCount; }
            set { _WinMaxCount = value; NotifyPropertyChange(); }
        }


        double _WinMax = 0;
        /// <summary>
        /// winMax
        /// </summary>
        public double WinMax
        {
            get { return _WinMax; }
            set { _WinMax = value; NotifyPropertyChange(); }
        }

        double _WinContinuousMax = 0;
        /// <summary>
        /// Win thông max
        /// </summary>
        public double WinContinuousMax
        {
            get { return _WinContinuousMax; }
            set { _WinContinuousMax = value; NotifyPropertyChange(); }
        }








        public void Reset()
        {
            WinContinuousCount = 0;
            MaxLostProfit = 0;

            RateProfitDivStart = 0;
            WinCumulativeCount = 0;
            LoseCumulativeCount = 0;

            FundsStart = 0;
            ProfitMin = 0;
            BetMax = 0;
            ProfitMax = 0;

            LoseMax = 0;
            LoseMaxCount = 0;
            LoseContinuousMax = 0;
            LoseContinuousMaxCount = 0;

            WinMax = 0;
            WinMaxCount = 0;
            WinContinuousMax = 0;
            WinContinuousMaxCount = 0;
        }
    }
}
