﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TqkLibrary.WpfUi;

namespace AutoBetMoonAta.UI.ViewModels
{
    internal class DownloadCandlesHistoryWVM : BaseViewModel
    {
        DateTime _From = DateTime.Now;
        public DateTime From
        {
            get { return _From; }
            set { _From = value; NotifyPropertyChange(); }
        }


        DateTime _To = DateTime.Now;
        public DateTime To
        {
            get { return _To; }
            set { _To = value; NotifyPropertyChange(); }
        }

        public bool DownloadAdv
        {
            get { return Singleton.Setting.Setting.DownloadAdv; }
            set { Singleton.Setting.Setting.DownloadAdv = value; NotifyPropertyChange(); Singleton.Setting.Save(); }
        }
    }
}
