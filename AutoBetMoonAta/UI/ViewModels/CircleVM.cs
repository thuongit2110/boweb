﻿using AutoBetMoonAta.Cores.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using TqkLibrary.WpfUi;
using TqkLibrary.WpfUi.ObservableCollection;

namespace AutoBetMoonAta.UI.ViewModels
{
    internal class CircleVM : BaseViewModel, ICircleVM
    {
        public CircleVM(string data)
        {
            if (string.IsNullOrWhiteSpace(data)) throw new ArgumentNullException(nameof(data));
            this.Data = data;
            var splits = data.Split('-');
            if (splits.Length >= 2)
            {
                Condition = splits[0];
                Action = splits[1];
            }
            else
            {
                Action = data;
            }
            //this.ActionActive = Action;
        }
        public string Data { get; }
        public string Condition { get; } = string.Empty;
        public string Action { get; }



        int _Index = 0;
        public int Index
        {
            get { return _Index; }
            private set { _Index = value; NotifyPropertyChange(); }
        }

        public bool IsActive { get; set; } = false;


        int _WinCount = 0;
        public int WinCount
        {
            get { return _WinCount; }
            set
            {
                if (_WinCount != value)
                {
                    _WinCount = value;
                    NotifyPropertyChange();
                }
            }
        }

        int _LoseCount = 0;
        public int LoseCount
        {
            get { return _LoseCount; }
            set
            {
                if (_LoseCount != value)
                {
                    _LoseCount = value;
                    NotifyPropertyChange();
                }
            }

        }





        public uint ActionActive { get; set; } = 0;

        int _ActionResultCount = 0;
        public int ActionResultCount
        {
            get { return _ActionResultCount; }
            set { _ActionResultCount = value; NotifyPropertyChange(); }
        }





        int _MoneyIndex = 0;
        public int MoneyIndex
        {
            get { return _MoneyIndex; }
            set
            {
                if (_MoneyIndex != value)
                {
                    _MoneyIndex = value;
                    NotifyPropertyChange();
                }
            }

        }


        public class CircleList : List<CircleVM>
        {
            public CircleList()
            {

            }
            public new void Add(CircleVM circleVM)
            {
                circleVM.Index = this.Count + 1;
                base.Add(circleVM);
            }
            //protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
            //{
            //    int i = 1;
            //    foreach (var item in this)
            //    {
            //        item.Index = i;
            //        i++;
            //    }
            //    base.OnCollectionChanged(e);
            //}
        }

        public class ActiveCircleDispatcherObservableCollection : DispatcherObservableCollection<CircleVM>
        {

            //protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
            //{
            //    base.OnCollectionChanged(e);

            //    int i = 1;
            //    foreach (var item in this)
            //    {
            //        if(item != null) item.ActiveIndex = i;
            //        i++;
            //    }
            //    if (e.Action == NotifyCollectionChangedAction.Remove)
            //    {
            //        foreach (var item in e.OldItems.Cast<CircleVM>())
            //        {
            //            item.ActiveIndex = 0;
            //        }
            //    }
            //}
            //protected override void ClearItems()
            //{
            //    foreach (var item in this) item.ActiveIndex = 0;
            //    base.ClearItems();
            //}

            //public Task AddAsync(CircleVM circleVM)
            //{
            //    return this.Dispatcher.InvokeAsync(() => this.Add(circleVM)).Task;
            //}

            //public Task RemoveAsync(CircleVM circleVM)
            //{
            //    return this.Dispatcher.InvokeAsync(() => this.Remove(circleVM)).Task;
            //}


            public Task AddRangeAsync(IEnumerable<CircleVM> circleVMs)
            {
                return this.Dispatcher.InvokeAsync(() =>
                {
                    foreach (var item in circleVMs)
                    {
                        item.IsActive = true;
                        this.Add(item);
                    }
                }).Task;
            }

            public Task RemoveRangeAsync(IEnumerable<CircleVM> circleVMs)
            {
                return this.Dispatcher.InvokeAsync(() =>
                {
                    foreach (var item in circleVMs)
                    {
                        this.Remove(item);
                        item.IsActive = false;
                    }
                }).Task;
            }

            //public void AddRangeSynchronizationContext(IEnumerable<CircleVM> circleVMs)
            //{
            //    SynchronizationContext.Send(_AddRangeSynchronizationContext, circleVMs);
            //}
            //void _AddRangeSynchronizationContext(object obj)
            //{
            //    if (obj is IEnumerable<CircleVM> circleVMs)
            //    {
            //        foreach (var item in circleVMs) this.Add(item);
            //    }
            //}


            //public void RemoveRangeSynchronizationContext(IEnumerable<CircleVM> circleVMs)
            //{
            //    SynchronizationContext.Send(_RemoveRangeSynchronizationContext, circleVMs);
            //}
            //void _RemoveRangeSynchronizationContext(object obj)
            //{
            //    if (obj is IEnumerable<CircleVM> circleVMs)
            //    {
            //        foreach (var item in circleVMs) this.Remove(item);
            //    }
            //}
        }
    }
}
