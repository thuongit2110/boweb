﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TqkLibrary.WpfUi;

namespace AutoBetMoonAta.UI.ViewModels
{
    internal class EditAccountWVM : BaseViewModel
    {
        AccountVM _accountVM = null;
        public AccountVM AccountVM
        {
            get { return _accountVM; }
            set { _accountVM = value; NotifyPropertyChange(); }
        }


        string _Email = string.Empty;
        public string Email
        {
            get { return _Email; }
            set { _Email = value; NotifyPropertyChange(); }
        }

        string _TwoFA = string.Empty;
        public string TwoFA
        {
            get { return _TwoFA; }
            set { _TwoFA = value; NotifyPropertyChange(); }
        }

        bool _IsAutoLogin = false;
        public bool IsAutoLogin
        {
            get { return _IsAutoLogin; }
            set { _IsAutoLogin = value; NotifyPropertyChange(); }
        }
    }
}
