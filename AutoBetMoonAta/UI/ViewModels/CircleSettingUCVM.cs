﻿using AutoBetMoonAta.DataClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TqkLibrary.WpfUi;
using System.Collections.ObjectModel;
using TqkLibrary.WpfUi.ObservableCollection;
using System.Threading;
using AutoBetMoonAta.Cores.Interfaces;

namespace AutoBetMoonAta.UI.ViewModels
{
    internal class CircleSettingUCVM : BaseViewModel, ICircleSettingUCVM
    {
        public int ThreadsNeed { get; private set; } = 0;
        public AccountVM AccountVM { get; }
        public CircleSettingUCVM(AccountVM accountVM)
        {
            this.AccountVM = accountVM;
            LoadData();
        }

        public CircleSettingData CircleSetting
        {
            get { return AccountVM.Data.CircleSetting; }
        }
        public int ResetWin
        {
            get { return CircleSetting.ResetWin; }
            set { CircleSetting.ResetWin = value; AccountVM.Save(); NotifyPropertyChange(); }
        }

        public int ResetLose
        {
            get { return CircleSetting.ResetLose; }
            set { CircleSetting.ResetLose = value; AccountVM.Save(); NotifyPropertyChange(); }
        }

        /// <summary>
        /// Tách biệt dòng tiền
        /// </summary>
        public bool IsSplitMoneyFlow
        {
            get { return CircleSetting.IsSplitMoneyFlow; }
            set { CircleSetting.IsSplitMoneyFlow = value; AccountVM.Save(); NotifyPropertyChange(); }
        }

        CircleVM.CircleList _Circles = null;

        public CircleVM.CircleList Circles
        {
            get { return _Circles; }
            private set { _Circles = value; NotifyPropertyChange(); }
        }
        public CircleVM.ActiveCircleDispatcherObservableCollection CircleActives { get; } = new CircleVM.ActiveCircleDispatcherObservableCollection();


        CircleVM _CircleSelected = null;
        public CircleVM CircleSelected
        {
            get { return _CircleSelected; }
            set { _CircleSelected = value; NotifyPropertyChange(); }
        }
        public void LoadData()
        {
            CircleActives.Clear();
            var circles = new CircleVM.CircleList();
            if (CircleSetting.CircleDatas?.Count > 0)
            {
                CircleSetting.CircleDatas.ForEach(x => circles.Add(new CircleVM(x)));
            }
            ThreadsNeed = (int)Math.Ceiling(CircleSetting.CircleDatas.Count * 1.0 / Cores.Singleton.ItemPerThread);
            this.Circles = circles;
        }
    }
}
