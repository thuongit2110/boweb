﻿using AutoBetMoonAta.Cores.Enums;
using AutoBetMoonAta.Cores.Interfaces;
using AutoBetMoonAta.DataClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TqkLibrary.WpfUi;

namespace AutoBetMoonAta.UI.ViewModels
{
    internal class MoneyStepSettingUCVM : BaseViewModel, IMoneyStepSettingUCVM
    {
        public AccountVM AccountVM { get; }
        internal MoneyStepSettingUCVM(AccountVM accountVM)
        {
            this.AccountVM = accountVM;
            StepResultsCalc();
        }

        public IEnumerable<ComboBoxVM<MoneyStepType>> MoneyStepTypes { get; } = new List<ComboBoxVM<MoneyStepType>>()
        {
            new ComboBoxVM<MoneyStepType>(MoneyStepType.MultiplierLose),
            new ComboBoxVM<MoneyStepType>(MoneyStepType.MultiplierWin),
            new ComboBoxVM<MoneyStepType>(MoneyStepType.MoneyResultUpDown),
            new ComboBoxVM<MoneyStepType>(MoneyStepType.MultiplierLoseAndWin)
        };
        public ComboBoxVM<MoneyStepType> MoneyStepTypesSelectedItem
        {
            get { return MoneyStepTypes.First(x => x.Type == AccountVM.Data.MoneyStepSetting.MoneyStepType); }
            set { AccountVM.Data.MoneyStepSetting.MoneyStepType = value.Type; AccountVM.Save(); NotifyPropertyChange(); }
        }

        public IEnumerable<ComboBoxVM<MoneyIndexAdvanced>> MoneyIndexAdvanceds { get; } = new List<ComboBoxVM<MoneyIndexAdvanced>>()
        {
            new ComboBoxVM<MoneyIndexAdvanced>(MoneyIndexAdvanced.UpLostDownWin),
            new ComboBoxVM<MoneyIndexAdvanced>(MoneyIndexAdvanced.DownLostUpWin)
        };
        public ComboBoxVM<MoneyIndexAdvanced> MoneyIndexAdvancedsSelectedItem
        {
            get { return MoneyIndexAdvanceds.First(x => x.Type == AccountVM.Data.MoneyStepSetting.MoneyIndexAdvanced); }
            set { AccountVM.Data.MoneyStepSetting.MoneyIndexAdvanced = value.Type; AccountVM.Save(); NotifyPropertyChange(); }
        }

        public MoneyStepMarkType MarkType
        {
            get { return AccountVM.Data.MoneyStepSetting.MoneyStepMarkType; }
            set { AccountVM.Data.MoneyStepSetting.MoneyStepMarkType = value; AccountVM.Save(); NotifyPropertyChange(); StepResultsCalc(); }
        }

        public int Count
        {
            get { return AccountVM.Data.MoneyStepSetting.Count; }
            set { AccountVM.Data.MoneyStepSetting.Count = value; AccountVM.Save(); NotifyPropertyChange(); StepResultsCalc(); }
        }

        public double BaseFunds
        {
            get { return AccountVM.Data.MoneyStepSetting.BaseFunds; }
            set { AccountVM.Data.MoneyStepSetting.BaseFunds = value; AccountVM.Save(); NotifyPropertyChange(); StepResultsCalc(); }
        }
        public double Multi
        {
            get { return AccountVM.Data.MoneyStepSetting.Multi; }
            set { AccountVM.Data.MoneyStepSetting.Multi = value; AccountVM.Save(); NotifyPropertyChange(); StepResultsCalc(); }
        }

        public double UpToMaxDivide
        {
            get { return AccountVM.Data.MoneyStepSetting.UpToMaxDivide; }
            set { AccountVM.Data.MoneyStepSetting.UpToMaxDivide = value; AccountVM.Save(); NotifyPropertyChange(); StepResultsCalc(); }
        }

        public double CanUpDownPerSessionDivide
        {
            get { return AccountVM.Data.MoneyStepSetting.CanUpDownPerSessionDivide; }
            set { AccountVM.Data.MoneyStepSetting.CanUpDownPerSessionDivide = value; AccountVM.Save(); NotifyPropertyChange(); StepResultsCalc(); }
        }


        public int MoneyIndexAdvancedStart
        {
            get { return AccountVM.Data.MoneyStepSetting.MoneyIndexAdvancedStart; }
            set { AccountVM.Data.MoneyStepSetting.MoneyIndexAdvancedStart = value; AccountVM.Save(); NotifyPropertyChange(); }
        }

        public int MoneyIndexAdvancedIncrease
        {
            get { return AccountVM.Data.MoneyStepSetting.MoneyIndexAdvancedIncrease; }
            set { AccountVM.Data.MoneyStepSetting.MoneyIndexAdvancedIncrease = value; AccountVM.Save(); NotifyPropertyChange(); }
        }

        public int MoneyIndexAdvancedDecrease
        {
            get { return AccountVM.Data.MoneyStepSetting.MoneyIndexAdvancedDecrease; }
            set { AccountVM.Data.MoneyStepSetting.MoneyIndexAdvancedDecrease = value; AccountVM.Save(); NotifyPropertyChange(); }
        }

        public double MoneyResultUpDownMulti
        {
            get { return AccountVM.Data.MoneyStepSetting.MoneyResultUpDownMulti; }
            set { AccountVM.Data.MoneyStepSetting.MoneyResultUpDownMulti = value; AccountVM.Save(); NotifyPropertyChange(); }
        }

        public int MoneyResultUpDownSessionCount
        {
            get { return AccountVM.Data.MoneyStepSetting.MoneyResultUpDownSessionCount; }
            set { AccountVM.Data.MoneyStepSetting.MoneyResultUpDownSessionCount = value; AccountVM.Save(); NotifyPropertyChange(); }
        }


        double _MaxBalanced = 0;
        public double MaxBalanced
        {
            get { return _MaxBalanced; }
            private set { _MaxBalanced = value; NotifyPropertyChange(); }
        }


        string _StepResult = string.Empty;//set from background
        public string StepResult//only set from ui
        {
            get { return _StepResult; }
            set
            {
                _StepResult = value;
                NotifyPropertyChange();
                try
                {
                    var customStepResults = value.Split(',').Select(x => double.Parse(x.Trim())).ToArray();
                    IsStepResultSuccess = true;
                    _StepResults = customStepResults;
                    NotifyPropertyChange(nameof(TotalSumStepResults));
                    AccountVM.Data.MoneyStepSetting.Count = customStepResults.Length;
                    AccountVM.Save(); 
                    NotifyPropertyChange(nameof(Count));
                }
                catch
                {
                    IsStepResultSuccess = false;
                }
            }
        }

        double[] _StepResults;
        public double[] StepResults
        {
            get { return _StepResults; }
        }

        public double TotalSumStepResults
        {
            get { return _StepResults.Sum(); }
        }


        bool _IsStepResultSuccess = true;
        public bool IsStepResultSuccess
        {
            get { return _IsStepResultSuccess; }
            private set { _IsStepResultSuccess = value; NotifyPropertyChange(); }
        }


        IAccountVM IMoneyStepSettingUCVM.AccountVM => this.AccountVM;

        public MoneyStepType MoneyStepTypesSelected => MoneyStepTypesSelectedItem.Type;

        public MoneyIndexAdvanced MoneyIndexAdvancedsSelected => MoneyIndexAdvancedsSelectedItem.Type;

        public void BalanceChanged()
        {
            if (MarkType == MoneyStepMarkType.UpToMax && AccountVM.Balance >= MaxBalanced)
            {
                StepResultsCalc();
            }
            else if (MarkType == MoneyStepMarkType.CanUpDownPerSession)
            {
                StepResultsCalc();
            }
        }

        public void StepResultsCalc()
        {

            List<double> steps = new List<double>();
            switch (MarkType)
            {
                case MoneyStepMarkType.UpToMax:
                    MaxBalanced = AccountVM.Balance;
                    AccountVM.Data.MoneyStepSetting.BaseFunds = MaxBalanced / UpToMaxDivide;
                    NotifyPropertyChange(nameof(BaseFunds));
                    break;

                case MoneyStepMarkType.CanUpDownPerSession:
                    AccountVM.Data.MoneyStepSetting.BaseFunds = AccountVM.Balance / CanUpDownPerSessionDivide;
                    NotifyPropertyChange(nameof(BaseFunds));
                    break;
            }

            double last = BaseFunds;
            steps.Add(last);
            for (int i = 0; i < Count - 1; i++)
            {
                steps.Add(last * Multi);
                last = steps.Last();
            }

            _StepResults = steps.ToArray();
            _StepResult = string.Join(", ", _StepResults.Select(x => x.ToString("0.00000")));
            NotifyPropertyChange(nameof(StepResult));
            NotifyPropertyChange(nameof(TotalSumStepResults));
            IsStepResultSuccess = true;
        }

        public void Reset()
        {
            MaxBalanced = 0;
        }
    }
}
