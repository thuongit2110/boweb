﻿using AutoBetMoonAta.DataClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TqkLibrary.WpfUi;

namespace AutoBetMoonAta.UI.ViewModels
{
    internal class SettingWVM : BaseViewModel
    {
        SettingData Setting { get { return Singleton.Setting.Setting; } }
        void Save() => Singleton.Setting.Save();


    }
}
