﻿using AutoBetMoonAta.Cores;
using AutoBetMoonAta.Cores.Interfaces;
using AutoBetMoonAta.DataClass;
using MoonAtaApi;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TqkLibrary.WpfUi;
using TqkLibrary.WpfUi.ObservableCollection;

namespace AutoBetMoonAta.UI.ViewModels
{
    internal class AccountVM : BaseViewModel, IViewModel<AccountVMData>, IAccountVM
    {
        IAdvancedConfigUCVM IAccountVM.AdvancedConfigUCVM => this.AdvancedConfigUCVM;

        ICircleSettingUCVM IAccountVM.CircleSettingUCVM => this.CircleSettingUCVM;

        IAutoData IAccountVM.AutoData => this.AutoData;

        readonly Action<string> _logCallback;
        public AccountVM(AccountVMData data, Action<string> logCallback)
        {
            this._logCallback = logCallback ?? throw new ArgumentNullException(nameof(logCallback));
            this.Data = data;
            CircleSettingUCVM = new CircleSettingUCVM(this);
            MoneyStepSettingUCVM = new MoneyStepSettingUCVM(this);
            AccountHistoryUCVM = new AccountHistoryUCVM(this);
            AdvancedConfigUCVM = new AdvancedConfigUCVM(this);
            AutoData = new AutoData(this);
            Reset();
        }
        public void WriteLog(string log)
        {
            _logCallback?.Invoke($"[{AccountName}] {log}");
        }

        public void Refresh()
        {
            NotifyPropertyChange(nameof(AccountName));
        }

        public void Save()
        {
            Change?.Invoke(this, Data);
        }

        public AccountVMData Data { get; }

        public event ChangeCallBack<AccountVMData> Change;


        public string AccountName { get { return Data.NickName; } }




        public CircleSettingUCVM CircleSettingUCVM { get; }

        public MoneyStepSettingUCVM MoneyStepSettingUCVM { get; }

        public AccountHistoryUCVM AccountHistoryUCVM { get; }

        public AdvancedConfigUCVM AdvancedConfigUCVM { get; }

        public bool IsOpenAccountConfigWindow { get; set; } = false;


        BoReceived _BoData;
        public BoReceived BoData
        {
            get { return _BoData; }
            set { _BoData = value; NotifyPropertyChange(); }
        }

        SpotBalanceResponse _SpotBalance;
        public SpotBalanceResponse SpotBalance
        {
            get { return _SpotBalance; }
            set
            {
                _SpotBalance = value;
                NotifyPropertyChange();
                MoneyStepSettingUCVM.BalanceChanged();
                NotifyPropertyChange(nameof(Balance));
            }
        }

        int _totalWin = 0;
        public int TotalWin
        {
            get { return _totalWin; }
            set { _totalWin = value; NotifyPropertyChange(); NotifyPropertyChange(nameof(TotalWinPercent)); NotifyPropertyChange(nameof(TotalLosePercent)); }
        }

        int _totalLose = 0;
        public int TotalLose
        {
            get { return _totalLose; }
            set { _totalLose = value; NotifyPropertyChange(); NotifyPropertyChange(nameof(TotalWinPercent)); NotifyPropertyChange(nameof(TotalLosePercent)); }
        }


        public double TotalWinPercent
        {
            get
            {
                if (TotalWin == 0) return 0;
                else return TotalWin * 100.0 / (TotalWin + TotalLose);
            }
        }

        public double TotalLosePercent
        {
            get
            {
                if (TotalLose == 0) return 0;
                else return TotalLose * 100.0 / (TotalWin + TotalLose);
            }
        }

        string _AutoStatus = string.Empty;
        public string AutoStatus
        {
            get { return _AutoStatus; }
            set { _AutoStatus = value; NotifyPropertyChange(); WriteLog($"[AutoStatus] {value}"); }
        }

        string _ServerResultHistoryString = string.Empty;
        public string ServerResultHistoryString
        {
            get { return _ServerResultHistoryString; }
            set { _ServerResultHistoryString = value; NotifyPropertyChange(); }
        }

        double _Profit = 0;
        public double Profit
        {
            get { return _Profit; }
            set { _Profit = value; NotifyPropertyChange(); }
        }


        bool _IsWorking = false;
        public bool IsWorking
        {
            get { return _IsWorking; }
            set
            {
                _IsWorking = value;
                NotifyPropertyChange();
            }
        }


        bool _IsTestMode = false;
        public bool IsTestMode
        {
            get { return _IsTestMode; }
            set { _IsTestMode = value; NotifyPropertyChange(); MoneyStepSettingUCVM.Reset(); MoneyStepSettingUCVM.BalanceChanged(); NotifyPropertyChange(nameof(Balance)); }
        }

        public double TestBalanced
        {
            get { return Data.TestBalanced; }
            set
            {
                if (Data.TestBalanced != value)
                {
                    Data.TestBalanced = value;
                    NotifyPropertyChange();
                    Save();
                    MoneyStepSettingUCVM.BalanceChanged();
                    NotifyPropertyChange(nameof(Balance));
                }

            }
        }

        public double Balance
        {
            get
            {
                if (IsTestMode) return TestBalanced;
                else
                {
                    switch (BetAccountTypesSelected.Type)
                    {
                        case MoonAtaBetAccountType.DEMO: return SpotBalance?.DemoBalance ?? 0;
                        case MoonAtaBetAccountType.LIVE: return SpotBalance?.AvailableBalance ?? 0;
                        default: return 0;
                    }
                }
            }
        }

        public List<ComboBoxVM<MoonAtaBetAccountType>> BetAccountTypes { get; } = new List<ComboBoxVM<MoonAtaBetAccountType>>()
        {
            new ComboBoxVM<MoonAtaBetAccountType>(MoonAtaBetAccountType.DEMO,"Demo"),
            new ComboBoxVM<MoonAtaBetAccountType>(MoonAtaBetAccountType.LIVE,"Live")
        };

        public ComboBoxVM<MoonAtaBetAccountType> BetAccountTypesSelected
        {
            get { return BetAccountTypes.First(x => x.Type == Data.MoonAtaBetAccountType); }
            set { Data.MoonAtaBetAccountType = value.Type; NotifyPropertyChange(); MoneyStepSettingUCVM.Reset(); MoneyStepSettingUCVM.StepResultsCalc(); Save(); }
        }


        bool _IsLogined = false;
        public bool IsLogined
        {
            get { return _IsLogined; }
            set { _IsLogined = value; NotifyPropertyChange(); }
        }







        public AutoData AutoData { get; }

        public void Reset()
        {
            ServerResultHistoryString = (IsTestMode ? AutoData.TestResultHistory : AutoData.ServerResultHistory).ToString();
            AutoStatus = ".....";
            IsTestMode = false;
            AutoData.Reset();

            CircleSettingUCVM.CircleActives.Clear();
            TotalLose = 0;
            TotalWin = 0;
            Profit = 0;
            foreach (var item in CircleSettingUCVM.Circles)
            {
                item.WinCount = 0;
                item.LoseCount = 0;
            }
            AccountHistoryUCVM.TransactionHistorys.Clear();
            AccountHistoryUCVM.Reset();
            MoneyStepSettingUCVM.Reset();
        }

        public void RefreshUIToData()
        {
            AutoStatus = "Bắt đầu";
        }
    }
}
