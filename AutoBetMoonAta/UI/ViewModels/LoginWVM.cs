﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TqkLibrary.WpfUi;

namespace AutoBetMoonAta.UI.ViewModels
{
    internal class LoginWVM : BaseViewModel
    {
        public string UserName
        {
            get { return Singleton.Setting.Setting.UserName; }
            set { Singleton.Setting.Setting.UserName = value; NotifyPropertyChange(); Singleton.Setting.Save(); }
        }

        public bool IsAutoLogin
        {
            get { return Singleton.Setting.Setting.IsAutoLogin; }
            set { Singleton.Setting.Setting.IsAutoLogin = value; NotifyPropertyChange(); Singleton.Setting.Save(); }
        }
    }
}
