﻿using AutoBetMoonAta.Cores.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TqkLibrary.WpfUi;

namespace AutoBetMoonAta.UI.ViewModels
{
    internal class ComboBoxVM<TEnum> where TEnum : Enum
    {
        public ComboBoxVM(TEnum type)
        {
            this.Type = type;
            this.Name = type.GetAttribute<NameAttribute>().Name;
        }
        public ComboBoxVM(TEnum type,string name)
        {
            this.Type = type;
            this.Name = name;
        }
        public TEnum Type { get; }

        public string Name { get; }
    }
}
