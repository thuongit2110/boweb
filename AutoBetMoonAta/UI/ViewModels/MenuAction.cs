﻿using AutoBetMoonAta.Cores.Utils;

namespace AutoBetMoonAta.UI.ViewModels
{
    public enum MenuAction
    {
        [Name("Cài đặt tài khoản")]
        AccountSetting,

        [Name("Cài đặt")]
        Config,

        [Name("Đăng nhập")]
        Login,

        [Name("Thêm tài khoản")]
        AddAccount,

        [Name("Xóa tài khoản")]
        DeteteAccount,
    }
}
