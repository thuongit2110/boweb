﻿using AutoBetMoonAta.UI.ViewModels;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AutoBetMoonAta.UI
{
    /// <summary>
    /// Interaction logic for CircleSettingUc.xaml
    /// </summary>
    public partial class CircleSettingUc : UserControl
    {
        public static readonly DependencyProperty CircleSettingProperty = DependencyProperty.Register(
             nameof(CircleSetting),
             typeof(CircleSettingUCVM),
             typeof(CircleSettingUc),
             new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        internal CircleSettingUCVM CircleSetting
        {
            get { return (CircleSettingUCVM)GetValue(CircleSettingProperty); }
            set { SetValue(CircleSettingProperty, value); }
        }

        public event Action UpdateMinThreadPool;

        public CircleSettingUc()
        {
            InitializeComponent();
        }

        private void btn_LoadFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Regex regex = new Regex("^[SB]{1,32}(-[SB]{1,32}|)$");
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "txt|*.txt|all file|*.*";
                ofd.InitialDirectory = Directory.GetCurrentDirectory();
                ofd.Multiselect = false;
                if (ofd.ShowDialog() == true)
                {
                    var datas = File.ReadAllLines(ofd.FileName).Select(x => x.Trim().ToUpper()).ToList();

                    var datas_temp = datas.Where(x => !string.IsNullOrWhiteSpace(x) && regex.Match(x).Success).ToList();

                    if (datas.Count != datas_temp.Count)
                    {
                        if(MessageBox.Show("Có dòng không hợp lệ hoặc dòng trống\r\nBỏ qua dòng lỗi và tiếp tục?","Lỗi",MessageBoxButton.YesNo) == MessageBoxResult.No)
                        {
                            return;
                        }
                    }


                    

                    CircleSetting.CircleSetting.CircleDatas = datas_temp;
                    CircleSetting.AccountVM.Save();
                    CircleSetting.LoadData();
                    UpdateMinThreadPool?.Invoke();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, ex.GetType().FullName);
            }
        }
    }
}
