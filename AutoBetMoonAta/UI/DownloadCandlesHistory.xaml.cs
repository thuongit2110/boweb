﻿using AutoBetMoonAta.UI.ViewModels;
using MahApps.Metro.Controls;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutoBetMoonAta.UI
{
    /// <summary>
    /// Interaction logic for DownloadCandlesHistory.xaml
    /// </summary>
    public partial class DownloadCandlesHistory : MetroWindow
    {
        public string HistoryData { get; private set; }
        readonly DownloadCandlesHistoryWVM downloadCandlesHistoryWVM;
        readonly bool isSaveFile;
        public DownloadCandlesHistory(bool isSaveFile)
        {
            this.isSaveFile = isSaveFile;
            InitializeComponent();
            this.downloadCandlesHistoryWVM = this.DataContext as DownloadCandlesHistoryWVM;
            dtp_from.Culture = Thread.CurrentThread.CurrentCulture;
            dtp_to.Culture = Thread.CurrentThread.CurrentCulture;
        }

        private async void btn_download_Click(object sender, RoutedEventArgs e)
        {
            if (!downloadCandlesHistoryWVM.DownloadAdv)
            {
                downloadCandlesHistoryWVM.From = downloadCandlesHistoryWVM.From.Date;
                downloadCandlesHistoryWVM.To = downloadCandlesHistoryWVM.From.AddDays(1);
            }
            if (downloadCandlesHistoryWVM.From >= downloadCandlesHistoryWVM.To)
            {
                MessageBox.Show("Thời gian bắt đầu phải nhỏ hơn kết thúc", "Lỗi");
                return;
            }
            try
            {
                Mouse.OverrideCursor = Cursors.Wait;

                var list = await ApiHepler.DownloadCandlesHistory(downloadCandlesHistoryWVM.From, downloadCandlesHistoryWVM.To);
                HistoryData = string.Join("", list.Select(x => x.IsGreen ? "B" : "S"));
                if (isSaveFile)
                {
                    SaveFileDialog saveFileDialog = new SaveFileDialog();
                    saveFileDialog.FileName = $"{downloadCandlesHistoryWVM.From:MM-dd HH-mm} - {downloadCandlesHistoryWVM.To:MM-dd HH-mm}.txt";
                    saveFileDialog.InitialDirectory = Directory.GetCurrentDirectory();
                    if (saveFileDialog.ShowDialog() == true)
                    {
                        File.WriteAllText(saveFileDialog.FileName, HistoryData);
                    }
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                Mouse.OverrideCursor = Cursors.Arrow;
            }
        }
    }
}
