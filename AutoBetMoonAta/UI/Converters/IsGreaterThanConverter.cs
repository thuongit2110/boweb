﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace AutoBetMoonAta.UI
{
    [ValueConversion(typeof(double?), typeof(bool?))]
    public class IsGreaterThanConverter : IValueConverter
    {
        /// <summary>
        /// default false
        /// </summary>
        public bool IsIncludeEqual { get; set; } = false;
        public bool IsRevert { get; set; } = false;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Type value_type = value.GetType();
            if (value_type != typeof(double) && value_type != typeof(double?))
            {
                throw new InvalidOperationException("The target must be a double or double?");
            }

            if (value == null)
            {
                return null;
            }
            double d_value = (double)value;
            double d_param = parameter is string parameter_string ? double.Parse(parameter_string) : (double)parameter;

            return (IsIncludeEqual && d_value == d_param) || IsRevert ? d_value < d_param : d_value > d_param;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
