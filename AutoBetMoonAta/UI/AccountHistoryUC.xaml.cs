﻿using AutoBetMoonAta.UI.ViewModels;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AutoBetMoonAta.UI
{
    /// <summary>
    /// Interaction logic for AccountHistoryUC.xaml
    /// </summary>
    public partial class AccountHistoryUC : UserControl
    {
        public static readonly DependencyProperty AccountHistoryProperty = DependencyProperty.Register(
             nameof(AccountHistory),
             typeof(AccountHistoryUCVM),
             typeof(AccountHistoryUC),
             new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        internal AccountHistoryUCVM AccountHistory
        {
            get { return (AccountHistoryUCVM)GetValue(AccountHistoryProperty); }
            set { SetValue(AccountHistoryProperty, value); }
        }


        public AccountHistoryUC()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            if(MessageBox.Show("Xóa danh sách lịch sử", "Xác nhận", MessageBoxButton.OKCancel,MessageBoxImage.Warning) == MessageBoxResult.OK)
            {
                AccountHistory.TransactionHistorys.Clear();
            }
        }

        private void btn_export_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog()
            {
                InitialDirectory = Directory.GetCurrentDirectory(),
                Filter = "txt|*.txt|all file|*.*",
                FileName = $"{DateTime.Now:yyyy-MM-dd HH-mm-ss}.txt",
            };

            if(sfd.ShowDialog() == true)
            {
                try
                {
                    using StreamWriter sw = new StreamWriter(sfd.FileName, false);
                    AccountHistory.TransactionHistorys.ToList().ForEach(x => sw.Write(x));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().FullName);
                }
            }
        }
    }
}
