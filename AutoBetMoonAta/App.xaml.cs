﻿using AutoBetMoonAta.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ControlzEx.Theming;
using MoonAtaApi;
using System.Threading;
using System.Globalization;
using AutoBetMoonAta.Commons;

namespace AutoBetMoonAta
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            try
            {
                CultureInfo culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                culture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
                culture.DateTimeFormat.LongDatePattern = "dd/MM/yyyy";
                Thread.CurrentThread.CurrentCulture = culture;
                MoonAtaClient.ApiEndPoint = new Uri(Singleton.ApiEndPoint);
                MoonAtaClient.WebSocketEndPoint = new Uri(Singleton.WebSocketEndPoint);
#if !DEBUG
                Shortcut.Create();
#endif
                MainWindow window = new MainWindow();
                window.Closed += Window_Closed;
                window.Show();
            }
            catch (Exception ex)
            {
                Shutdown(-1);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Shutdown();
        }
    }
}
