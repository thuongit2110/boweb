﻿using AutoBetMoonAta.DataClass;
using BaseSource.ViewModels.Admin;
using BaseSource.ViewModels.Common;
using BaseSource.ViewModels.User;
using MoonAtaApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
namespace AutoBetMoonAta
{
    internal class ApiException : Exception
    {
        public ApiException() { }
        public ApiException(string message) : base(message) { }
    }
    internal static class ApiHepler
    {
        internal static async Task<ApiResult<string>> Login(string userName, string password, CancellationToken cancellationToken = default)
        {
            using HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, "api/Account/Authenticate");
            using StringContent stringContent = new StringContent(JsonConvert.SerializeObject(new LoginRequestVm()
            {
                UserName = userName,
                Password = password
            }), Encoding.UTF8, "application/json");
            httpRequestMessage.Content = stringContent;
            using HttpResponseMessage httpResponseMessage = await Singleton.HttpClient.SendAsync(httpRequestMessage, HttpCompletionOption.ResponseContentRead, cancellationToken);
            string text = await httpResponseMessage.EnsureSuccessStatusCode().Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ApiResult<string>>(text);
        }

        internal static async Task<string> CheckAccount(string nickName, CancellationToken cancellationToken = default)
        {
            using HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, "api/MoonAta/CheckAccount");
            using StringContent stringContent = new StringContent(JsonConvert.SerializeObject(new CheckAccountVM()
            {
                NickName = nickName
            }), Encoding.UTF8, "application/json");
            httpRequestMessage.Content = stringContent;
            using HttpResponseMessage httpResponseMessage = await Singleton.HttpClient.SendAsync(httpRequestMessage, HttpCompletionOption.ResponseContentRead, cancellationToken);
            string text = await httpResponseMessage.EnsureSuccessStatusCode().Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<ApiResult<string>>(text);
            if (result.IsSuccessed)
            {
                return result.ResultObj;
            }
            else
            {
                string errorMessage = result.Message;
                if(result.ValidationErrors != null)
                {
                    errorMessage += Environment.NewLine;
                    errorMessage += string.Join(Environment.NewLine, result.ValidationErrors.Select(x => x.Error));
                }
                throw new ApiException(errorMessage);
            }
        }

        internal static async Task<string> ResolveCaptcha(CancellationToken cancellationToken = default)
        {
            using HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, "api/MoonAta/SolveCaptcha");
            using HttpResponseMessage httpResponseMessage = await Singleton.HttpClient.SendAsync(httpRequestMessage, HttpCompletionOption.ResponseContentRead, cancellationToken);
            string text = await httpResponseMessage.EnsureSuccessStatusCode().Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<ApiResult<string>>(text);
            if (result.IsSuccessed)
            {
                return result.ResultObj;
            }
            else
            {
                string errorMessage = result.Message;
                if (result.ValidationErrors != null)
                {
                    errorMessage += Environment.NewLine;
                    errorMessage += string.Join(Environment.NewLine, result.ValidationErrors.Select(x => x.Error));
                }
                throw new ApiException(errorMessage);
            }
        }



        internal static async Task<List<MoonAtaCandleHistoryVm>> DownloadCandlesHistory(DateTime from, DateTime to, CancellationToken cancellationToken = default)
        {
            var parameters = HttpUtility.ParseQueryString(string.Empty);
            parameters[nameof(GetAccountMoonAtaRequestVm.StartDate)] = from.ToString(CultureInfo.InvariantCulture);
            parameters[nameof(GetAccountMoonAtaRequestVm.EndDate)] = to.ToString(CultureInfo.InvariantCulture);
            using HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, $"api/MoonAtaCandleHistoryAdmin/GetAlls?{parameters}");
            using HttpResponseMessage httpResponseMessage = await Singleton.HttpClient.SendAsync(httpRequestMessage, HttpCompletionOption.ResponseContentRead, cancellationToken);
            string text = await httpResponseMessage.EnsureSuccessStatusCode().Content.ReadAsStringAsync();
            var obj = JsonConvert.DeserializeObject<ApiResult<object>>(text);
            if (obj.IsSuccessed) return JsonConvert.DeserializeObject<ApiResult<List<MoonAtaCandleHistoryVm>>>(text).ResultObj;
            else
            {
                throw new ApiException(string.Join(Environment.NewLine, obj.ValidationErrors.Select(x => x.Error)));
            }
        }
    }
}
