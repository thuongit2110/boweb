$dirInfo= New-Object -Typename System.IO.DirectoryInfo -ArgumentList ([System.IO.Directory]::GetCurrentDirectory())
$workingDir="$($dirInfo.FullName)\bin\x64\Release\net462"

$rarPath="C:\Program Files\WinRAR\WinRAR.exe";
$outName="Rar\AutoBetMoonAtaInstall.exe";

$params="a -sfx $($outName) -sfxDefault64.SFX -z$($workingDir)\RarXfs.conf @fileList.txt"

Remove-Item -Force -Confirm:$false -Recurse "$($workingDir)\Rar"
mkdir "$($workingDir)\Rar"
Start-Process -FilePath $($rarPath) -WorkingDirectory $workingDir -ArgumentList $($params)