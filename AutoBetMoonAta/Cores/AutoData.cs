﻿using AutoBetMoonAta.Cores;
using AutoBetMoonAta.Cores.Interfaces;
using AutoBetMoonAta.UI.ViewModels;
using MoonAtaApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoBetMoonAta.Cores
{
    internal class AutoData : IAutoData
    {
        public AccountVM AccountVM { get; }
        internal AutoData(AccountVM accountVM)
        {
            this.AccountVM = accountVM;
            ServerResultHistory = new ServerResultHistory();
            ServerResultHistory.OnNewPushResult += this.ServerResultHistory_OnNewPushResult;
            TestResultHistory = new ServerResultHistory();
            MoonAtaClient = new MoonAtaClient();
            MoonAtaClient.OnTokenReceived += this.MoonAtaClient_OnTokenReceived;
            WebSocketHepler = new WebSocketHepler(MoonAtaClient.CancellationToken);
            WebSocketHepler.OnException += WebSocketHepler_OnException;
            WebSocketHepler.OnBoReceived += this.WebSocketHepler_OnBoReceived;
            WebSocketHepler.WebSocketOnDisconnect += this.WebSocketHepler_WebSocketOnDisconnect;
        }

        private void WebSocketHepler_OnException(Exception ex)
        {
            AccountVM.WriteLog($"WebSocketHepler_OnException [{ex.GetType().FullName}] {ex.Message} {ex.StackTrace}");
        }

        private void ServerResultHistory_OnNewPushResult(string obj)
        {
            AccountVM.WriteLog($"ServerResultHistory_OnNewPushResult {obj}");
            if (!AccountVM.IsTestMode) AccountVM.ServerResultHistoryString = obj;
        }

        private void TestResultHistory_OnNewPushResult(string obj)
        {
            if (AccountVM.IsTestMode) AccountVM.ServerResultHistoryString = obj;
        }

        private async void WebSocketHepler_WebSocketOnDisconnect()
        {
            AccountVM.WriteLog($"WebSocketHepler_WebSocketOnDisconnect");
            try
            {
                WebSocketHepler.TaskWebsocket(await MoonAtaClient.InitWebSocket());
            }
            catch (Exception ex)
            {
                AccountVM.WriteLog($"WebSocketHepler_WebSocketOnDisconnect [{ex.GetType().FullName}] {ex.Message} {ex.StackTrace}");
                AccountVM.AutoStatus = "Không kết nối lại được";
                AccountVM.IsLogined = false;
            }
        }

        private void WebSocketHepler_OnBoReceived(BoReceived obj)
        {
            if (AccountVM.BoData != null && obj.IsBetSession != AccountVM.BoData.IsBetSession)
            {
                AccountVM.WriteLog($"WebSocketHepler_OnBoReceived IsBetSession Change {obj.IsBetSession}");
                if (obj.IsBetSession)
                {
                    bool isUp = obj.OpenPrice >= AccountVM.BoData.OpenPrice;
                    AccountVM.BoData = obj;
                    ServerResultHistory.Push(isUp);
                }
                else AccountVM.BoData = obj;
            }
            else AccountVM.BoData = obj;

            if (DateTime.Now > MoonAtaClient.LastTimeRequest.AddMinutes(1))
            {
                UpdateBlance();
            }
        }

        private async void UpdateBlance()
        {
            try
            {
                var balance = await MoonAtaClient.SpotBalance();
                AccountVM.SpotBalance = balance.Data;
            }
            catch
            {

            }
        }

        private void MoonAtaClient_OnTokenReceived(MoonAtaToken obj)
        {
            this.AccountVM.Data.Token = obj;
            this.AccountVM.Save();
        }

        public ServerResultHistory ServerResultHistory { get; private set; }
        public ServerResultHistory TestResultHistory { get; private set; }

        public CircleCollection CircleCollection { get; private set; }
        public Dictionary<Circle, CircleVM> CircleDict { get; } = new Dictionary<Circle, CircleVM>();
        public MoonAtaClient MoonAtaClient { get; }
        public WebSocketHepler WebSocketHepler { get; }

        public void Reset()
        {
            if (TestResultHistory != null) TestResultHistory.Dispose();
            TestResultHistory = new ServerResultHistory();
            TestResultHistory.OnNewPushResult += this.TestResultHistory_OnNewPushResult;
            CircleCollection = new CircleCollection(AccountVM.MoneyStepSettingUCVM, AccountVM.CircleSettingUCVM);
            CircleDict.Clear();
            WinCount = 0;
            LoseCount = 0;
            WinCountNonSkip = 0;
            LoseCountNonSkip = 0;
            InvertCount = 0;
            PauseCount = 0;
            DataTestIndex = 0;
        }


        public bool IsContinue
        {
            get
            {
                if (AccountVM.Profit >= AccountVM.AdvancedConfigUCVM.StopAutoAtWinMoney) return false;
                if (Math.Abs(AccountVM.Profit) >= AccountVM.AdvancedConfigUCVM.StopAutoAtLoseMoney) return false;
                return true;
            }
        }

        private int WinCount = 0;
        private int LoseCount = 0;
        private int WinCountNonSkip = 0;
        private int LoseCountNonSkip = 0;
        private int InvertCount = 0;
        private int PauseCount = 0;
        public bool IsInvert { get { return AccountVM.AdvancedConfigUCVM.InvertedAll || InvertCount > 0; } }
        public bool IsPause { get { return PauseCount > 0; } }

        public int DataTestIndex { get; set; } = 0;
        public void UpdateAdvanceSetting(bool? isWin, double? betValue = null)
        {
            InvertCount--;
            PauseCount--;
            //win cộng dồn là tính lúc đang đánh này đang win bao tiền cái dây đó
            //Lose max là dây thua có tổng tiền thua lớn nhất
            //win thông max là số phiên win liên tiếp dài nhất và tổng tiền ăn dc dây đó
            if (isWin == true)
            {
                //Lose max là dây thua có tổng tiền thua lớn nhất
                if (AccountVM.AccountHistoryUCVM.LoseCumulativeCount > AccountVM.AccountHistoryUCVM.LoseMax)
                {
                    AccountVM.AccountHistoryUCVM.LoseMax = AccountVM.AccountHistoryUCVM.LoseCumulativeCount;
                    AccountVM.AccountHistoryUCVM.LoseMaxCount = LoseCountNonSkip;
                }
                //lose thông max
                if (LoseCountNonSkip > AccountVM.AccountHistoryUCVM.LoseContinuousMaxCount)
                {
                    AccountVM.AccountHistoryUCVM.LoseContinuousMaxCount = LoseCountNonSkip;
                    AccountVM.AccountHistoryUCVM.LoseContinuousMax = AccountVM.AccountHistoryUCVM.LoseCumulativeCount;
                }
                AccountVM.AccountHistoryUCVM.WinCumulativeCount += betValue.Value * 0.95;//win cộng dồn
                WinCount++;
                WinCountNonSkip++;
                LoseCount = 0;
                LoseCountNonSkip = 0;
                AccountVM.AccountHistoryUCVM.LoseCumulativeCount = 0;//reset lose cộng dồn
            }
            else if (isWin == false)
            {
                //Win max là dây thắng có tổng tiền thắng lớn nhất
                if (AccountVM.AccountHistoryUCVM.WinCumulativeCount > AccountVM.AccountHistoryUCVM.WinMax)
                {
                    AccountVM.AccountHistoryUCVM.WinMax = AccountVM.AccountHistoryUCVM.WinCumulativeCount;
                    AccountVM.AccountHistoryUCVM.WinMaxCount = WinCountNonSkip;
                }
                //win thông max
                if (WinCountNonSkip > AccountVM.AccountHistoryUCVM.WinContinuousMaxCount)
                {
                    AccountVM.AccountHistoryUCVM.WinContinuousMaxCount = WinCountNonSkip;
                    AccountVM.AccountHistoryUCVM.WinContinuousMax = AccountVM.AccountHistoryUCVM.WinCumulativeCount;
                }
                AccountVM.AccountHistoryUCVM.LoseCumulativeCount += betValue.Value; //lose cộng dồn
                LoseCount++;
                LoseCountNonSkip++;
                WinCount = 0;
                WinCountNonSkip = 0;
                AccountVM.AccountHistoryUCVM.WinCumulativeCount = 0;//reset win cộng dồn
            }
            else
            {
                WinCount = 0;
                LoseCount = 0;
            }

            if (WinCount >= AccountVM.AdvancedConfigUCVM.InvertedOnWinConsecutive)
            {
                InvertCount = AccountVM.AdvancedConfigUCVM.InvertedOnWinCount;
            }
            if (LoseCount >= AccountVM.AdvancedConfigUCVM.InvertedOnLoseConsecutive)
            {
                InvertCount = AccountVM.AdvancedConfigUCVM.InvertedOnLoseCount;
            }
            if (WinCount >= AccountVM.AdvancedConfigUCVM.PauseOnWinConsecutive)
            {
                PauseCount = AccountVM.AdvancedConfigUCVM.PauseOnWinCount;
            }
            if (LoseCount >= AccountVM.AdvancedConfigUCVM.PauseOnLoseConsecutive)
            {
                PauseCount = AccountVM.AdvancedConfigUCVM.PauseOnLoseCount;
            }


            AccountVM.AccountHistoryUCVM.WinContinuousCount = WinCount;
            if (isWin != null && betValue != null)
            {
                if (isWin.Value)
                {
                    AccountVM.TotalWin++;
                    AccountVM.Profit += betValue.Value * 0.95;
                    AccountVM.TestBalanced += betValue.Value * 0.95;
                }
                else
                {
                    AccountVM.TotalLose++;
                    AccountVM.Profit -= betValue.Value;
                    AccountVM.TestBalanced -= betValue.Value;
                }

                if (betValue.Value > AccountVM.AccountHistoryUCVM.BetMax) AccountVM.AccountHistoryUCVM.BetMax = betValue.Value;
                AccountVM.AccountHistoryUCVM.MaxLostProfit = Math.Min(AccountVM.AccountHistoryUCVM.MaxLostProfit, AccountVM.Profit);
                AccountVM.AccountHistoryUCVM.RateProfitDivStart = AccountVM.Profit * 100.0 / AccountVM.AccountHistoryUCVM.FundsStart;

                //lãi min
                AccountVM.AccountHistoryUCVM.ProfitMin = Math.Min(AccountVM.Profit, AccountVM.AccountHistoryUCVM.ProfitMin);
                //lãi max
                AccountVM.AccountHistoryUCVM.ProfitMax = Math.Max(AccountVM.Profit, AccountVM.AccountHistoryUCVM.ProfitMax);
            }
        }
    }
}