﻿using AutoBetMoonAta.DataClass;
using AutoBetMoonAta.Queue;
using AutoBetMoonAta.UI;
using MoonAtaApi;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using TqkLibrary.Queues.TaskQueues;
using TqkLibrary.WpfUi;
using TwoFactorAuthNet;

namespace AutoBetMoonAta
{
    internal static class Singleton
    {
        static Singleton()
        {
            Directory.CreateDirectory(LogDir);
            WorkQueues.MaxRun = 999;

            HttpClientHandler httpClientHandler = new HttpClientHandler();
            httpClientHandler.UseProxy = false;
            httpClientHandler.AllowAutoRedirect = true;
            httpClientHandler.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            httpClientHandler.ServerCertificateCustomValidationCallback = (HttpRequestMessage, X509Certificate2, X509Chain, SslPolicyErrors) => true;
            HttpClient = new HttpClient(httpClientHandler, true);
            HttpClient.BaseAddress = new Uri(ApiUri);
            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            
        }
        internal static string ExeDir { get; } = Directory.GetCurrentDirectory();
        internal static string LogDir { get; } = ExeDir + "\\Logs";
        internal static string AppDataDir { get; } = ExeDir + "\\AppData";
        internal static TaskQueue<BaseQueue> WorkQueues { get; } = new TaskQueue<BaseQueue>();
        internal static HttpClient HttpClient { get; } = new HttpClient();

        internal static string WebRegister { get; } = "https://moonata2.net/register?r=66370CE";
        internal static string ApiEndPoint { get; } = "https://moonata2.net";
        internal static string WebSocketEndPoint { get; } = "https://ws.moonata2.net";
        internal static string WebLogin { get; } = "https://moonata2.net/login";
        internal static string WebUri { get; } = "https://www.auto789.net/";
        internal static string ApiUri { get; }
#if DEBUG
        = "https://api.auto789.net/";//"https://localhost:44308/";
#else
        = "https://api.auto789.net/";
#endif
        internal static TwoFactorAuth TwoFactorAuth { get; } = new TwoFactorAuth();


        internal static SaveSettingData<SettingData> Setting { get; } = new SaveSettingData<SettingData>(ExeDir + "\\Setting.json", 100);














        public static char? GetCharAtIndex(this string str, int index)
        {
            if (string.IsNullOrWhiteSpace(str)) return null;
            if (index < 0 || index >= str.Length) return null;
            return str[index];
        }

        public static string MatchStartEnd(this string ending, string starting)
        {
            string result = string.Empty;
            for (int i = 1; i <= ending.Length; i++)
            {
                var end = ending.Substring(ending.Length - i);
                if (end.StartsWith(starting)) result = end;
            }
            return result;
        }

        public static Task<MoonAtaToken> GetTokenUIAsync(this Dispatcher dispatcher, Window parent = null)
        {
            TaskCompletionSource<MoonAtaToken> taskCompletionSource = new TaskCompletionSource<MoonAtaToken>(TaskCreationOptions.RunContinuationsAsynchronously);
            dispatcher.Invoke(() =>
            {
                WebLoginWindow webLoginWindow = new WebLoginWindow();
                if (parent != null) webLoginWindow.Owner = parent;
                webLoginWindow.Closed += (object sender, EventArgs e) => taskCompletionSource.TrySetResult(webLoginWindow.Token);
                webLoginWindow.Show();
            });
            return taskCompletionSource.Task;
        }

    }
}
