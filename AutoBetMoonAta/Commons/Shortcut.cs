﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IWshRuntimeLibrary;
namespace AutoBetMoonAta.Commons
{
    internal static class Shortcut
    {
        internal static void Create()
        {
            using Process process = Process.GetCurrentProcess();
            FileInfo fileInfo = new FileInfo(process.MainModule.FileName);
            FileInfo shortcut = new FileInfo(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), $"{fileInfo.Name}.lnk"));

            if (!shortcut.Exists)
            {
                try
                {
                    var shell = new WshShell();
                    IWshShortcut wshShortcut = shell.CreateShortcut(shortcut.FullName) as IWshShortcut;
                    if (wshShortcut != null)
                    {
                        wshShortcut.Description = shortcut.Name;
                        wshShortcut.TargetPath = fileInfo.FullName;
                        wshShortcut.WorkingDirectory = fileInfo.DirectoryName;
                        wshShortcut.Save();
                    }
                }
                catch { }
            }
        }
    }
}
