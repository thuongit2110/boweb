﻿using CefSharp;
using CefSharp.Handler;
using MoonAtaApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoBetMoonAta.Cefs
{
    internal class CustomRequestHandler: RequestHandler
    {
        public event Action<MoonAtaToken> OnTokenReceived;
        protected override IResourceRequestHandler GetResourceRequestHandler(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, bool isNavigation, bool isDownload, string requestInitiator, ref bool disableDefaultHandling)
        {
            switch(request.ResourceType)
            {
                case ResourceType.Xhr:
                    if( request.Url.EndsWith("/api/auth/auth/token") ||
                        request.Url.EndsWith("/api/auth/auth/token-2fa"))
                    {
                        return new CustomResourceRequestHandler((action) => OnTokenReceived?.Invoke(action));
                    }
                    break;
                default:
                    break;
            }
            return base.GetResourceRequestHandler(chromiumWebBrowser, browser, frame, request, isNavigation, isDownload, requestInitiator, ref disableDefaultHandling);
        }
    }
}