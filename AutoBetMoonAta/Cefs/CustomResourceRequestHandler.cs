﻿using CefSharp;
using CefSharp.Handler;
using CefSharp.ResponseFilter;
using MoonAtaApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoBetMoonAta.Cefs
{
    internal class CustomResourceRequestHandler : ResourceRequestHandler
    {
        private readonly MemoryStream memoryStream = new MemoryStream();
        Action<MoonAtaToken> action;
        public CustomResourceRequestHandler(Action<MoonAtaToken> action)
        {
            this.action = action ?? throw new ArgumentNullException(nameof(action));
        }


        protected override IResponseFilter GetResourceResponseFilter(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, IResponse response)
        {
            return new StreamResponseFilter(memoryStream);
        }

        protected override void OnResourceLoadComplete(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, IResponse response, UrlRequestStatus status, long receivedContentLength)
        {
            ApiResponse<MoonAtaToken> apiResponse = JsonConvert.DeserializeObject<ApiResponse<MoonAtaToken>>(Encoding.UTF8.GetString(memoryStream.ToArray()));
            action?.Invoke(apiResponse.Data);

            base.OnResourceLoadComplete(chromiumWebBrowser, browser, frame, request, response, status, receivedContentLength);
        }

        protected override void Dispose()
        {
            memoryStream.Dispose();
            base.Dispose();
        }
    }
}