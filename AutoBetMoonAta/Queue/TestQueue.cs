﻿using AutoBetMoonAta.Cores;
using AutoBetMoonAta.UI.ViewModels;
using MoonAtaApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TqkLibrary.Queues.TaskQueues;
using System.Collections.Concurrent;
using System.Diagnostics;
using AutoBetMoonAta.Cores.Enums;

namespace AutoBetMoonAta.Queue
{
    internal class TestQueue : BaseQueue
    {
        public TestQueue(AccountVM accountVM) : base(accountVM)
        {
            accountVM.MoneyStepSettingUCVM.StepResultsCalc();
        }

        public override async Task DoWork()
        {
            try
            {
                AccountVM.AccountHistoryUCVM.FundsStart = AccountVM.TestBalanced;

                LoadCircleCollection();

                Stopwatch stopWatch = new Stopwatch();
                while (AccountVM.AutoData.IsContinue &&
                    !cancellationTokenSource.IsCancellationRequested &&
                    (AccountVM.Data.IsEmunatorLoop || AccountVM.AutoData.DataTestIndex < AccountVM.Data.EmulatorData.Length))
                {
                    stopWatch.Start();
                    if (last_transactionHistory != null)
                    {
                        bool Result_isUp = AccountVM.Data.EmulatorData[AccountVM.AutoData.DataTestIndex++ % AccountVM.Data.EmulatorData.Length] == 'B';
                        ResultHistory.Push(Result_isUp);
                        await UpdateResultAndUI(Result_isUp);
                    }

                    last_transactionHistory = await Bet();

                    await AccountVM.AccountHistoryUCVM.TransactionHistorys.InsertTopAsync(last_transactionHistory);

                    stopWatch.Stop();
                    int mili = (int)stopWatch.ElapsedMilliseconds;
                    Trace.WriteLine("StopWatch:" + mili);
                    if (Singleton.Setting.Setting.EmulatorDelay > mili) await Task.Delay(Singleton.Setting.Setting.EmulatorDelay - mili, cancellationTokenSource.Token);
                    stopWatch.Reset();
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected override async Task<TransactionHistoryVM> Bet()
        {
            var bet = await CircleCollection.GetBet();

            MoonAtaBetTypeVM moonAtaBetTypeVM = bet?.Item1 switch
            {
                true => AccountVM.AutoData.IsInvert ? MoonAtaBetTypeVM.Sell : MoonAtaBetTypeVM.Buy,
                false => AccountVM.AutoData.IsInvert ? MoonAtaBetTypeVM.Buy : MoonAtaBetTypeVM.Sell,
                _ => MoonAtaBetTypeVM.Skip
            };
            if (AccountVM.AutoData.IsPause) moonAtaBetTypeVM = MoonAtaBetTypeVM.Skip;
            if (moonAtaBetTypeVM == MoonAtaBetTypeVM.Skip)
            {
                AccountVM.AutoStatus = $"Bỏ qua cược";
            }
            else
            {
                AccountVM.AutoStatus = $"{(AccountVM.AutoData.IsInvert ? "[Đánh ngược]" : "")} Cược {bet.Item2} vào {(moonAtaBetTypeVM == MoonAtaBetTypeVM.Buy ? "B" : "S")} ";
            }
            return new TransactionHistoryVM(moonAtaBetTypeVM, bet?.Item2, AccountVM.AutoData.IsInvert);
        }
    }
}
/*
    ServerResultHistory.Push first
    UpdateIsWaitCondition
loop:
    BET
    ServerResultHistory.Push
    UpdateWinLost (mốc tiền,circle win/lost)
    UpdateToUI
    Reset
    UpdateIsWaitCondition
goto loop;
....
 */