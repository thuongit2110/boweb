﻿using AutoBetMoonAta.DataClass;
using MoonAtaApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TqkLibrary.Queues.TaskQueues;
using TqkLibrary.Net.Captcha;
using AutoBetMoonAta.UI.ViewModels;
using AutoBetMoonAta.UI;
using System.Net.WebSockets;
using System.Collections.Concurrent;
using AutoBetMoonAta.Cores;
using TwoFactorAuthNet;
using System.Windows.Threading;
using AutoBetMoonAta.Cores.Enums;

namespace AutoBetMoonAta.Queue
{
    internal class WorkQueue : BaseQueue
    {
        readonly Random random = new Random(DateTime.Now.Millisecond);
        readonly SemaphoreSlim _signal_seconds = new SemaphoreSlim(0);
        int betTime = 30;
        public WorkQueue(AccountVM accountVM) : base(accountVM)
        {

        }

        public override void Dispose()
        {
            _signal_seconds.Dispose();
            base.Dispose();
        }

        private void WebSocketHepler_OnBoReceived(BoReceived obj)
        {
            if (obj.IsBetSession && obj.Order <= betTime) _signal_seconds.Release();
        }

        /*
            ServerResultHistory.Push first
            UpdateIsWaitCondition
        loop:
            event: trigger
            BET

            UpdateWinLost (mốc tiền,circle win/lost)
            UpdateToUI
            Reset
            UpdateIsWaitCondition
        goto loop;

        event -> ServerResultHistory.Push
        ....
         */


        public override async Task DoWork()
        {
            try
            {
                AccountVM.AccountHistoryUCVM.FundsStart = AccountVM.Balance;
                AccountVM.AutoData.WebSocketHepler.OnBoReceived += this.WebSocketHepler_OnBoReceived;

                LoadCircleCollection();

                ResultHistory.ClearSignal();
                long currentBetSession = 0;
                while (AccountVM.AutoData.IsContinue)
                {
                    if (!AccountVM.IsLogined)
                    {
                        AccountVM.AutoStatus = "Auto stop (bị đăng xuất)";
                        return;
                    }
                    try
                    {
                        if (await ResultHistory.WaitAsync(2000, CancellationToken) && AccountVM.BoData.Session > currentBetSession)
                        {
                            currentBetSession = AccountVM.BoData.Session;
                            betTime = random.Next(AccountVM.AdvancedConfigUCVM.BetTimeMin, AccountVM.AdvancedConfigUCVM.BetTimeMax);

                            //update balanced
                            try
                            {
                                var balance = await AccountVM.AutoData.MoonAtaClient.SpotBalance();
                                AccountVM.SpotBalance = balance.Data;
                            }
                            catch { }

                            bool Result_isUp = (ResultHistory.History & 1) == 1;

                            await UpdateResultAndUI(Result_isUp);

                            last_transactionHistory = await Bet();

                            await AccountVM.AccountHistoryUCVM.TransactionHistorys.InsertTopAsync(last_transactionHistory);
                        }
                        else if (AccountVM.BoData?.IsBetSession != true)
                        {
                            AccountVM.AutoStatus = $"Chờ phiên cược (hiện tại {AccountVM.BoData.Session})";
                        }

                        if (DateTime.Now - AccountVM.AutoData.MoonAtaClient.LastTimeRequest > TimeSpan.FromSeconds(60))//for refresh token
                        {
                            var balance = await AccountVM.AutoData.MoonAtaClient.SpotBalance();
                            AccountVM.SpotBalance = balance.Data;
                        }
                    }
                    catch (OperationCanceledException)
                    {
                        if (CancellationToken.IsCancellationRequested)
                        {
                            AccountVM.AutoStatus = "Hủy";
                            AccountVM.WriteLog("WorkQueue.DoWork Canceled");
                            return;
                        }
                        else throw;
                    }
                    catch (MoonAtaAuthException ex)
                    {
                        AccountVM.WriteLog($"WorkQueue.DoWork [{ex.GetType().FullName}] {ex.Message} {ex.StackTrace}");
                        AccountVM.AutoStatus = "Bị mất phiên đăng nhập";
                        AccountVM.IsLogined = false;
                        return;
                    }
                    catch (Exception ex)
                    {
                        AccountVM.WriteLog($"WorkQueue.DoWork [{ex.GetType().FullName}] {ex.Message} {ex.StackTrace}");
                        if (ex is AggregateException ae) ex = ae.InnerException;
                        AccountVM.AutoStatus = $"{ex.GetType().FullName}: {ex.Message}";
                    }
                }
                AccountVM.AutoStatus = "Dừng auto";
            }
            finally
            {
                AccountVM.AutoData.WebSocketHepler.OnBoReceived -= this.WebSocketHepler_OnBoReceived;
            }
        }


        protected override async Task<TransactionHistoryVM> Bet()
        {
            //bet new
            MoonAtaBet bet = null;
            var b = await CircleCollection.GetBet();
            if (!AccountVM.AutoData.IsPause && b != null)
            {
                bet = new MoonAtaBet
                {
                    Type = (AccountVM.AutoData.IsInvert ? !b.Item1 : b.Item1) ? MoonAtaBetType.UP : MoonAtaBetType.DOWN,
                    Amount = b.Item2
                };
            }

            ApiResponse<TransactionResponse> transaction_open = null;
            if (bet != null)
            {
                if (AccountVM.AdvancedConfigUCVM.IsAllowBet)
                {
                    await _signal_seconds.WaitAsync(TimeSpan.FromSeconds(Math.Max(AccountVM.BoData.Order - betTime, 1)), CancellationToken);
                    betTime = 0;

                    bet.AccountType = AccountVM.BetAccountTypesSelected.Type;

                    AccountVM.AutoStatus = $"Phiên {AccountVM.BoData.Session}: {(AccountVM.AutoData.IsInvert ? "[Đánh ngược]" : "")} Cược {(bet.Type == MoonAtaBetType.UP ? "B" : "S")} với số tiền {bet.Amount}";

                    var betResult = await AccountVM.AutoData.MoonAtaClient.Bet(bet);
                    if (betResult.Ok) AccountVM.AutoStatus = $"Phiên {AccountVM.BoData.Session}: {(AccountVM.AutoData.IsInvert ? "[Đánh ngược]" : "")} Cược thành công {(bet.Type == MoonAtaBetType.UP ? "B" : "S")} với số tiền {bet.Amount}";
                    else AccountVM.AutoStatus = $"Phiên {AccountVM.BoData.Session}: {(AccountVM.AutoData.IsInvert ? "[Đánh ngược]" : "")} Cược thất bại";

                    await Task.Delay(2000, CancellationToken);

                    try
                    {
                        transaction_open = await AccountVM.AutoData.MoonAtaClient.Transaction(new Transaction()
                        {
                            AccountType = AccountVM.BetAccountTypesSelected.Type,
                            Mode = TransactionMode.Open,
                            Page = 1,
                            Size = 1
                        });
                    }
                    catch { }
                }
                else
                {
                    AccountVM.AutoStatus = $"[Không vào tiền] Phiên {AccountVM.BoData.Session}: Cược {(bet.Type == MoonAtaBetType.UP ? "B" : "S")} với số tiền {bet.Amount}";
                }
            }
            else
            {
                AccountVM.AutoStatus = $"Phiên {AccountVM.BoData.Session}: Bỏ qua (bỏ qua hoặc số cược bé hơn 1)";
            }

            return new TransactionHistoryVM(AccountVM.BoData, transaction_open?.Data?.c?.FirstOrDefault(), AccountVM.AutoData.IsInvert);
        }
    }
}
