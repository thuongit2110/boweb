﻿using AutoBetMoonAta.Cores;
using AutoBetMoonAta.Cores.Enums;
using AutoBetMoonAta.Cores.Utils;
using AutoBetMoonAta.UI.ViewModels;
using MoonAtaApi;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TqkLibrary.Queues.TaskQueues;

namespace AutoBetMoonAta.Queue
{
    internal abstract class BaseQueue : IQueue
    {
        internal AccountVM AccountVM { get; }
        protected readonly CancellationTokenSource cancellationTokenSource;
        protected CancellationToken CancellationToken { get { return cancellationTokenSource.Token; } }
        protected ServerResultHistory ResultHistory
        {
            get { return AccountVM.IsTestMode ? AccountVM.AutoData.TestResultHistory : AccountVM.AutoData.ServerResultHistory; }
        }
        protected CircleCollection CircleCollection { get { return AccountVM.AutoData.CircleCollection; } }

        protected TransactionHistoryVM last_transactionHistory = null;

        public BaseQueue(AccountVM accountVM)
        {
            this.AccountVM = accountVM ?? throw new ArgumentNullException(nameof(accountVM));
            if (AccountVM.IsWorking) throw new InvalidOperationException();
            AccountVM.RefreshUIToData();
            AccountVM.IsWorking = true;
            cancellationTokenSource = new CancellationTokenSource();
            last_transactionHistory = accountVM.AccountHistoryUCVM.TransactionHistorys.FirstOrDefault();
        }
        ~BaseQueue()
        {
            cancellationTokenSource.Dispose();
        }
        public bool IsPrioritize => false;

        public bool ReQueue => false;

        public bool ReQueueAfterRunComplete => false;

        public void Cancel()
        {
            cancellationTokenSource.Cancel();
        }

        public virtual void Dispose()
        {
            AccountVM.IsWorking = false;
            cancellationTokenSource.Dispose();
            GC.SuppressFinalize(this);
        }


        public abstract Task DoWork();


        protected void LoadCircleCollection()
        {
            if (CircleCollection.Count == 0)
            {
                foreach (var itemVM in AccountVM.CircleSettingUCVM.Circles)
                {
                    Circle circle = new Circle(itemVM,
                        AccountVM.MoneyStepSettingUCVM.MoneyStepTypesSelectedItem.Type == MoneyStepType.MoneyResultUpDown ?
                            AccountVM.MoneyStepSettingUCVM.MoneyIndexAdvancedStart - 1 : 0);
                    AccountVM.AutoData.CircleDict[circle] = itemVM;
                    circle.OnUpdateUI += this.Circle_OnUpdateUI;
                    CircleCollection.Add(circle);
                }
            }
        }

        private void Circle_OnUpdateUI(Circle obj)
        {
            CircleVM circleVM = AccountVM.AutoData.CircleDict[obj];

            circleVM.WinCount = obj.WinCount;
            circleVM.LoseCount = obj.LostCount;
            circleVM.MoneyIndex = obj.MoneyIndex % obj.MoneyStepSettingUCVM.StepResults.Length + 1;

            circleVM.ActionResultCount = obj.ActionResultCount;
            circleVM.ActionActive = obj.ActionResult;
        }

        protected async Task UpdateResultAndUI(bool isUp)
        {
            AccountVM.AutoStatus = "Cập nhật kết quả";
            if (last_transactionHistory != null)
            {
                last_transactionHistory.ServerResultIsUp = isUp;
                if (last_transactionHistory.BetType != MoonAtaBetTypeVM.Skip)
                {
                    bool isWin = isUp == (last_transactionHistory.RealBetType == MoonAtaBetTypeVM.Buy);
                    //bool isWin = isUp == (last_transactionHistory.BetType == MoonAtaBetTypeVM.Buy);
                    AccountVM.AutoData.UpdateAdvanceSetting(isWin, last_transactionHistory.TransactionAmount);
                    last_transactionHistory.TransactionResult = isWin ? TransactionResult.WIN : TransactionResult.LOSE;
                }
                else AccountVM.AutoData.UpdateAdvanceSetting(null);

                CircleCollection.UpdateWinLostNonSplitMoneyFlow(ResultHistory);

                await UpdateUICircleList(true);
            }
            else await UpdateUICircleList(false);
        }

        async Task UpdateUICircleList(bool isHaveTransaction)
        {
            ConcurrentBag<Circle> adds = new ConcurrentBag<Circle>();
            ConcurrentBag<Circle> removes = new ConcurrentBag<Circle>();
            await CircleCollection.ParallelWorkAsync((x) =>
            {
                if (!x.IsPermanentStop)
                {
                    if (isHaveTransaction) x.UpdateWinLostAndReset(ResultHistory);

                    x.UpdateIsWaitCondition(ResultHistory);
                    if (!x.IsWaitCondition && !x.CircleVM.IsActive) adds.Add(x);
                    else if (x.IsCondition && x.IsWaitCondition && x.CircleVM.IsActive) removes.Add(x);
                    x.UpdateToUI();
                }
            });

            await AccountVM.CircleSettingUCVM.CircleActives.AddRangeAsync(adds.Select(x => AccountVM.AutoData.CircleDict[x]));
            await AccountVM.CircleSettingUCVM.CircleActives.RemoveRangeAsync(removes.Select(x => AccountVM.AutoData.CircleDict[x]));
        }


        protected abstract Task<TransactionHistoryVM> Bet();
    }
}
/*
flow:

loop wait result
{
    if(haveOldTransaction)
    {
        UpdateResultAndUI(); //update result & reset/change flag,....//
    }
    
    get newBetResult
}
*/