﻿using BaseSource.ApiIntegration.AdminApi;
using BaseSource.ViewModels.Admin;
using BaseSource.ViewModels.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BaseSource.WebApp.Areas.Admin.Controllers
{
    public class DocumentMoonAtaController : BaseAdminController
    {
        private readonly IDocumentMoonAtaApiAdminClient _apiClient;
        public DocumentMoonAtaController(IDocumentMoonAtaApiAdminClient apiClient)
        {
            _apiClient = apiClient;
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            var result = await _apiClient.GetAlls();
            if (!result.IsSuccessed)
            {
                return NotFound();
            }
            var model = new CreateDocumentMoonAtaVm();
            if (result.ResultObj != null)
            {
                model.FileName = result.ResultObj.FileName;
                model.FileUrl = result.ResultObj.FileUrl;
                model.UrlVideo = result.ResultObj.UrlVideo;
            }
            return View(model);
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create(CreateDocumentMoonAtaVm model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new ApiErrorResult<string>(ModelState.GetListErrors()));
            }
            var result = await _apiClient.Create(model);
            if (!result.IsSuccessed)
            {
                return Json(new ApiErrorResult<string>(result.ValidationErrors));
            }

            return Json(new ApiSuccessResult<string>(Url.Action("Index")));
        }
    
    }
}
