﻿using BaseSource.ApiIntegration.AdminApi;
using BaseSource.ViewModels.Admin;
using BaseSource.ViewModels.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BaseSource.WebApp.Areas.Admin.Controllers
{
    public class AccountMoonAtaController : BaseAdminController
    {
        private readonly IAccountMoonAtaApiAdminClient _accountMoonAtaApiAdminClient;
        public AccountMoonAtaController(IAccountMoonAtaApiAdminClient accountMoonAtaApiAdminClient)
        {
            _accountMoonAtaApiAdminClient = accountMoonAtaApiAdminClient;
        }
        public async Task<IActionResult> Index(string username, string email, int? page = 1)
        {
            var request = new GetAccountMoonAtaPagingRequest_Admin()
            {
                Page = page.Value,
                PageSize = 10,
                UserName = username,
                Email = email
            };

            var result = await _accountMoonAtaApiAdminClient.GetPagings(request);
            if (!result.IsSuccessed)
            {
                return NotFound();
            }

            return View(result.ResultObj);
        }
        public ActionResult Create()
        {
            return PartialView("_CreateAcc");
        }
        [HttpPost]
        public async Task<ActionResult> Create(CreateAccountMoonAtaVm model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new ApiErrorResult<string>(ModelState.GetListErrors()));
            }
            var result = await _accountMoonAtaApiAdminClient.Create(model);
            if (!result.IsSuccessed)
            {
                ModelState.AddListErrors(result.ValidationErrors);
                return Json(new ApiErrorResult<string>(ModelState.GetListErrors()));
            }
            return Json(new ApiSuccessResult<string>());
        }
        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            var result = await _accountMoonAtaApiAdminClient.Delete(id);
            if (result.IsSuccessed)
            {
                return Json(new ApiSuccessResult<string>());
            }
            return Json(new ApiErrorResult<string>(result.Message));
        }
    }
}
