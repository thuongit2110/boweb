﻿using BaseSource.ApiIntegration.AdminApi;
using BaseSource.ViewModels.Admin;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.WebApp.Areas.Admin.Controllers
{
    public class MoonAtaCandleHistoryController : BaseAdminController
    {
        private readonly IMoonAtaCandleHistoryApiAdminClient _moonAtaCandleHistoryApiAdminClient;
        public MoonAtaCandleHistoryController(IMoonAtaCandleHistoryApiAdminClient moonAtaCandleHistoryApiAdminClient)
        {
            _moonAtaCandleHistoryApiAdminClient = moonAtaCandleHistoryApiAdminClient;
        }

        public IActionResult Index()
        {
            var model = new GetAccountMoonAtaRequestVm()
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now
            };
            return View(model);
        }
        public async Task<IActionResult> ExportFile(GetAccountMoonAtaRequestVm model)
        {
            string fileName = DateTime.Now.ToString("ddMMyyyy_HHmmss") + "_MoonAtaCandleHistory.txt";
            var lstData = await _moonAtaCandleHistoryApiAdminClient.GetAlls(model);
            string temp = "";
            if (lstData.ResultObj.Count > 0)
            {
                foreach (var item in lstData.ResultObj)
                {
                    temp += item.IsGreen ? "B" : "S";
                }

            }
            byte[] bin = Encoding.ASCII.GetBytes(temp);
            return File(bin, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
    }
}
