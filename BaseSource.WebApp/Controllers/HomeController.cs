﻿using BaseSource.ApiIntegration;
using BaseSource.ApiIntegration.WebApi;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BaseSource.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUserApiClient _userApiClient;
        private readonly IDocumentMoonAtaApiClient _documentMoonAtaApiClient;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _appEnvironment;


        public HomeController(ILogger<HomeController> logger,
            IUserApiClient userApiClient,
            IDocumentMoonAtaApiClient documentMoonAtaApiClient,
            IConfiguration configuration,
            IWebHostEnvironment appEnvironment)
        {
            _logger = logger;
            _userApiClient = userApiClient;
            _documentMoonAtaApiClient = documentMoonAtaApiClient;
            _configuration = configuration;
            _appEnvironment = appEnvironment;
        }


        public async Task<IActionResult> Index()
        {
            var data = await _documentMoonAtaApiClient.GetAlls();
            var popup = _configuration.GetValue<string>("HomeModalPopUp");
            var modal = data.ResultObj;
            if (modal == null) modal = new ViewModels.DocumentMoonAta.DocumentMoonAtaVm();
            modal.HomeModalPopUp = popup;
            return View(modal);
        }
        public IActionResult DownloadTool()
        {
            var url = _configuration.GetValue<string>("UrlTool");
            string fileName = url.Split('/').Last();
            string filePath = _appEnvironment.WebRootPath + url;
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);

        }



        //[AllowAnonymous]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}

    }
}
