﻿using BaseSource.Data.EF;
using BaseSource.ViewModels.Admin;
using BaseSource.ViewModels.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BaseSource.BackendApi.ControllersAdmin
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoonAtaCandleHistoryAdminController : BaseAdminApiController
    {
        private readonly BaseSourceDbContext _db;
        public MoonAtaCandleHistoryAdminController(BaseSourceDbContext context)
        {
            _db = context;
        }

        [AllowAnonymous]
        [HttpGet("GetAlls")]
        public async Task<IActionResult> GetAlls([FromQuery] GetAccountMoonAtaRequestVm request)
        {
            var model = _db.MoonAtaCandleHistories.AsQueryable();

            model = model.Where(x => x.CloseTime >= request.StartDate && x.CloseTime <= request.EndDate);

            var data = await model.Select(x => new MoonAtaCandleHistoryVm()
            {
                ClosePrice = x.ClosePrice,
                OpenPrice = x.OpenPrice,
                CloseTime = x.CloseTime
            }).OrderBy(x => x.CloseTime).ToListAsync();

            return Ok(new ApiSuccessResult<List<MoonAtaCandleHistoryVm>>(data));
        }

    }
}
