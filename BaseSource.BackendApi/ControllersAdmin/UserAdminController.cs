﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BaseSource.Data.EF;
using BaseSource.Data.Entities;
using BaseSource.ViewModels.Common;
using BaseSource.ViewModels.Admin;
using X.PagedList;
using Microsoft.AspNetCore.Identity;
using BaseSource.Shared.Enums;
using static BaseSource.Shared.Constants.SystemConstants;

namespace BaseSource.BackendApi.ControllersAdmin
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserAdminController : BaseAdminApiController
    {
        private readonly BaseSourceDbContext _db;
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<AppRole> _roleManager;
        private readonly SignInManager<AppUser> _signInManager;

        public UserAdminController(BaseSourceDbContext context,
                UserManager<AppUser> userManager,
                RoleManager<AppRole> roleManager, SignInManager<AppUser> signInManager)
        {
            _db = context;
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
        }


        [HttpGet("GetPagings")]
        public async Task<IActionResult> GetPagings([FromQuery] GetUserPagingRequest_Admin request)
        {
            var model = _db.Users.AsQueryable();

            if (!string.IsNullOrEmpty(request.UserName))
            {
                model = model.Where(x => x.UserName.Contains(request.UserName));
            };

            if (!string.IsNullOrEmpty(request.Email))
            {
                model = model.Where(x => x.Email.Contains(request.Email));
            }
            if (!User.IsInRole(Roles.Admin))
            {
                model = model.Where(x => x.UserIdCreate == UserId);
            }

            var data = await model.Select(x => new UserVm()
            {
                Id = x.Id,
                UserName = x.UserName,
                Email = x.Email,
                Roles = (from ur in _db.UserRoles
                         join r in _db.Roles on ur.RoleId equals r.Id
                         where ur.UserId == x.Id
                         select r.Name).ToList(),
                JoinedDate = x.JoinedDate
            }).OrderByDescending(x => x.JoinedDate).ToPagedListAsync(request.Page, request.PageSize);

            var pagedResult = new PagedResult<UserVm>()
            {
                TotalItemCount = data.TotalItemCount,
                PageSize = data.PageSize,
                PageNumber = data.PageNumber,
                Items = data.ToList()
            };

            return Ok(new ApiSuccessResult<PagedResult<UserVm>>(pagedResult));
        }

        [HttpGet("GetUserRoles")]
        public async Task<ActionResult> GetUserRoles(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return Ok(new ApiErrorResult<string>());
            }

            var userRoles = await _userManager.GetRolesAsync(user);
            var allRoles = await _roleManager.Roles.Select(x => x.Name).ToListAsync();

            var roleAssignRequest = new RoleAssignVm();
            roleAssignRequest.Id = id;
            foreach (var role in allRoles.OrderBy(x => x))
            {
                roleAssignRequest.Roles.Add(new SelectItem()
                {
                    Id = role,
                    Name = role,
                    Selected = userRoles.Contains(role)
                });
            }

            return Ok(new ApiSuccessResult<RoleAssignVm>(roleAssignRequest));
        }

        [HttpPost("RoleAssign")]
        public async Task<ActionResult> RoleAssign(RoleAssignVm model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByIdAsync(model.Id);
                if (user == null)
                {
                    return Ok(new ApiErrorResult<string>());
                }

                var userRoles = await _userManager.GetRolesAsync(user);
                await _userManager.RemoveFromRolesAsync(user, userRoles);

                var addedRoles = model.Roles.Where(x => x.Selected).Select(x => x.Name).ToList();
                if (model.Roles != null)
                {
                    await _userManager.AddToRolesAsync(user, addedRoles);
                }
                return Ok(new ApiSuccessResult<string>());
            }
            return Ok(new ApiErrorResult<string>());
        }

        [HttpGet("GetUserByRole")]
        public async Task<IActionResult> GetUserByRole(string role)
        {
            var usersOfRole = await _userManager.GetUsersInRoleAsync(role);
            var lstData = new List<UserVm>();
            foreach (var item in usersOfRole)
            {
                var data = new UserVm();
                data.Id = item.Id;
                data.UserName = item.UserName;
                lstData.Add(data);
            }
            return Ok(new ApiSuccessResult<List<UserVm>>(lstData));
        }
        [HttpPost("CreateUser")]
        public async Task<ActionResult> CreateUser(CreateUserVm model)
        {
            
            if (User.IsInRole(Roles.F2))
            {
                return Ok(new ApiErrorResult<string>("Not Permission"));
            }
            if (!ModelState.IsValid)
            {
                return Ok(new ApiErrorResult<string>(ModelState.GetListErrors()));
            }
            if (await _userManager.FindByNameAsync(model.UserName) != null)
            {
                ModelState.AddModelError("UserName", "UserName already exists");
                return Ok(new ApiErrorResult<string>(ModelState.GetListErrors()));
            }
            var appUser = new AppUser()
            {
                Id = Guid.NewGuid().ToString(),
                UserName = model.UserName,
                UserIdCreate = UserId,
                JoinedDate = DateTime.Now
            };

            var result = await _userManager.CreateAsync(appUser, model.Password);
            if (result.Succeeded)
            {
                List<string> stringList = new List<string>();
                if (User.IsInRole(Roles.Admin))
                {
                    stringList.Add(Roles.F1);
                }
                else if (User.IsInRole(Roles.F1))
                {
                    stringList.Add(Roles.F2);
                }

                await _userManager.AddToRolesAsync(appUser, stringList);

                var user = await _userManager.FindByIdAsync(appUser.Id);
                user.EmailConfirmed = true;
                await _db.SaveChangesAsync();

                return Ok(new ApiSuccessResult<string>());
            }
            AddErrors(result, nameof(model.UserName));
            return Ok(new ApiErrorResult<string>(ModelState.GetListErrors()));
        }
        [HttpPost("ResetPassword")]
        public async Task<IActionResult> ResetPassword([FromForm] string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return Ok(new ApiErrorResult<string>("Not Found"));
            }
            string resetToken = await _userManager.GeneratePasswordResetTokenAsync(user);
            if (string.IsNullOrEmpty(resetToken))
                return Ok(new ApiErrorResult<string>());

            var result = await _userManager.ResetPasswordAsync(user, resetToken, "123456");

            if (result.Succeeded)
            {
                return Ok(new ApiSuccessResult<string>());
            }

            AddErrors(result, nameof(user.UserName));
            return Ok(new ApiErrorResult<string>(ModelState.GetListErrors()));

        }
        [HttpPost("DeleteUser")]
        public async Task<IActionResult> DeleteUser([FromForm] string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return Ok(new ApiErrorResult<string>("Not Found"));
            }
            IdentityResult result = IdentityResult.Success;
            var logins = await _userManager.GetLoginsAsync(user);
            var rolesForUser = await _userManager.GetRolesAsync(user);
            foreach (var login in logins)
            {
                result = await _userManager.RemoveLoginAsync(user, login.LoginProvider, login.ProviderKey);
                if (!result.Succeeded)
                    break;
            }
            if (result.Succeeded)
            {
                foreach (var item in rolesForUser)
                {
                    result = await _userManager.RemoveFromRoleAsync(user, item);
                    if (!result.Succeeded)
                        break;
                }
            }
            if (result.Succeeded)
            {
                result = await _userManager.DeleteAsync(user);
            }
            if (result.Succeeded)
            {
                return Ok(new ApiSuccessResult<string>());
            }

            AddErrors(result, nameof(user.UserName));
            return Ok(new ApiErrorResult<string>(ModelState.GetListErrors()));
        }
        [HttpPost("ChangePassword")]
        public async Task<IActionResult> ChangePassword(ChangePasswordAdminVm model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new ApiErrorResult<string>(ModelState.GetListErrors()));
            }
            var user = await _userManager.FindByIdAsync(UserId.ToString());

            var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                // await _signInManager.SignInAsync(user, isPersistent: false);
                await _db.SaveChangesAsync();
                return Ok(new ApiSuccessResult<string>());
            }

            AddErrors(result, nameof(model.OldPassword));
            return Ok(new ApiErrorResult<string>(ModelState.GetListErrors()));

        }

        #region helper
        private void AddErrors(IdentityResult result, string Property)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(Property, error.Description);
                break;
            }
        }
        #endregion


    }
}
