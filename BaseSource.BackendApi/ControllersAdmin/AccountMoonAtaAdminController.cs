﻿using BaseSource.Data.EF;
using BaseSource.Data.Entities;
using BaseSource.ViewModels.Admin;
using BaseSource.ViewModels.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;
using static BaseSource.Shared.Constants.SystemConstants;

namespace BaseSource.BackendApi.ControllersAdmin
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountMoonAtaAdminController : BaseAdminApiController
    {
        private readonly BaseSourceDbContext _db;
        private readonly UserManager<AppUser> _userManager;

        public AccountMoonAtaAdminController(BaseSourceDbContext context, UserManager<AppUser> userManager)
        {
            _db = context;
            _userManager = userManager;
        }
        [HttpGet("GetPagings")]
        public async Task<IActionResult> GetPagings([FromQuery] GetAccountMoonAtaPagingRequest_Admin request)
        {
            var model = _db.AccountMoonAtas.AsQueryable();
            if (!User.IsInRole(Roles.Admin))
            {
                model = model.Where(x => x.UserIdCreate == UserId);
            }
            if (!string.IsNullOrEmpty(request.UserName))
            {
                var user = await _userManager.FindByNameAsync(request.UserName);
                string userId = user?.Id;
                model = model.Where(x => x.UserId == userId);

            };

            if (!string.IsNullOrEmpty(request.Email))
            {
                model = model.Where(x => x.Email.Contains(request.Email));
            }
            var data = await model.Select(x => new AccountMoonAtaVm()
            {
                Id = x.Id,
                Email = x.Email,
                UserName = (from u in _db.Users
                            where u.Id == x.UserId
                            select u.UserName).FirstOrDefault(),
            }).OrderByDescending(x => x.Id).ToPagedListAsync(request.Page, request.PageSize);

            var pagedResult = new PagedResult<AccountMoonAtaVm>()
            {
                TotalItemCount = data.TotalItemCount,
                PageSize = data.PageSize,
                PageNumber = data.PageNumber,
                Items = data.ToList()
            };

            return Ok(new ApiSuccessResult<PagedResult<AccountMoonAtaVm>>(pagedResult));
        }
        [HttpPost("Create")]
        public async Task<ActionResult> Create(CreateAccountMoonAtaVm model)
        {
            if (!User.IsInRole(Roles.Admin))
            {
                return Ok(new ApiErrorResult<string>("Not Permission"));
            }
            if (!ModelState.IsValid)
            {
                return Ok(new ApiErrorResult<string>(ModelState.GetListErrors()));
            }
            var userAddAc = await _userManager.FindByNameAsync(model.UserName.Trim());
            if (userAddAc == null)
            {
                ModelState.AddModelError("UserName", "UserName not found");
                return Ok(new ApiErrorResult<string>(ModelState.GetListErrors()));
            }

            var checkExit = await _db.AccountMoonAtas.FirstOrDefaultAsync(x => x.Email == model.Email);
            if (checkExit != null)
            {
                ModelState.AddModelError("Email", "Email already exists");
                return Ok(new ApiErrorResult<string>(ModelState.GetListErrors()));
            }

            var ac = new AccountMoonAta();
            ac.Email = model.Email;
            ac.UserId = userAddAc.Id;
            ac.UserIdCreate = UserId;
            ac.DateCreate = DateTime.Now;
            await _db.AccountMoonAtas.AddAsync(ac);
            await _db.SaveChangesAsync();

            return Ok(new ApiSuccessResult<string>());
        }
        [HttpPost("Delete")]
        public async Task<IActionResult> Delete([FromForm] int id)
        {
            var x = await _db.AccountMoonAtas.FindAsync(id);
            if (x == null)
            {
                return Ok(new ApiErrorResult<string>("Not Found"));
            }
            _db.AccountMoonAtas.Remove(x);
            await _db.SaveChangesAsync();
            return Ok(new ApiSuccessResult<string>());
        }

    }
}
