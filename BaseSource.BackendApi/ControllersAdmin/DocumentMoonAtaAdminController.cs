﻿using BaseSource.Data.EF;
using BaseSource.Data.Entities;
using BaseSource.Utilities.Helper;
using BaseSource.ViewModels.Admin;
using BaseSource.ViewModels.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BaseSource.BackendApi.ControllersAdmin
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentMoonAtaAdminController : BaseAdminApiController
    {
        private readonly BaseSourceDbContext _db;
        private readonly IWebHostEnvironment _appEnvironment;

        public DocumentMoonAtaAdminController(BaseSourceDbContext context, IWebHostEnvironment appEnvironment)
        {
            _db = context;
            _appEnvironment = appEnvironment;
        }

        [HttpGet("GetAlls")]
        public async Task<IActionResult> GetAlls()
        {
            var data = await _db.DocumentMoonAtas.Select(x => new DocumentMoonAtaVm()
            {
                Id = x.Id,
                FileName = x.FileName,
                FileUrl = string.IsNullOrEmpty(x.FileUrl) ? null : Url.Action("Index", "Home", null, Request.Scheme) + x.FileUrl,
                UrlVideo = x.UrlVideo
            }).FirstOrDefaultAsync();

            return Ok(new ApiSuccessResult<DocumentMoonAtaVm>(data));
        }
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromForm] CreateDocumentMoonAtaVm model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new ApiErrorResult<string>(ModelState.GetListErrors()));
            }

            var x = await _db.DocumentMoonAtas.FirstOrDefaultAsync();
            if (x == null)
            {
                var doc = new DocumentMoonAta();
                var file = model.IFileData;
                if (file != null && file.Length > 0)
                {
                    var filePath = await FileHelper.Upload(file, FileUploadType.Document, _appEnvironment.WebRootPath);
                    doc.FileUrl = filePath;
                    doc.FileName = file.FileName;
                }
                else
                {
                    ModelState.AddModelError("IFileData", "The FileData field is required.");
                    return Ok(new ApiErrorResult<string>(ModelState.GetListErrors()));
                }
                doc.UrlVideo = model.UrlVideo;
                _db.DocumentMoonAtas.Add(doc);
                await _db.SaveChangesAsync();

            }
            else
            {
                var file = model.IFileData;
                if (file != null && file.Length > 0)
                {
                    var filePath = await FileHelper.Upload(file, FileUploadType.Document, _appEnvironment.WebRootPath);
                    if (!string.IsNullOrEmpty(x.FileUrl) && !string.IsNullOrEmpty(filePath))
                    {
                        FileHelper.RemoveFileFromServer(x.FileUrl, _appEnvironment.WebRootPath);
                    }
                    x.FileUrl = filePath;
                    x.FileName = file.FileName;
                }
                x.UrlVideo = model.UrlVideo;
                await _db.SaveChangesAsync();
            }


            return Ok(new ApiSuccessResult<string>());
        }
    }
}
