﻿using BaseSource.Data.EF;
using BaseSource.Data.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MoonAtaApi;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TqkLibrary.Net.Captcha;
using Microsoft.EntityFrameworkCore;
using FlexLabs.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using BaseSource.BackendApi.Singletons;
using System.Net.Http;

namespace BaseSource.BackendApi.BackgroundServices
{
    public class MoonAtaCrawlerBackgroundService : IHostedService
    {
        readonly ILogger<MoonAtaCrawlerBackgroundService> logger;
        readonly IMoonAtaMainAccount moonAtaMainAccount;
        readonly IServiceScopeFactory serviceScopeFactory;
        public MoonAtaCrawlerBackgroundService(
            ILogger<MoonAtaCrawlerBackgroundService> logger,
            IMoonAtaMainAccount moonAtaMainAccount,
            IServiceScopeFactory serviceScopeFactory)
        {
            this.logger = logger;
            this.moonAtaMainAccount = moonAtaMainAccount;
            this.serviceScopeFactory = serviceScopeFactory;
        }




        CancellationToken cancellationToken;
        public Task StartAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("MoonAtaCrawlerBackgroundService StartAsync");
            moonAtaMainAccount.Init();
            this.cancellationToken = cancellationToken;
            _ = Task.Factory.StartNew(Work, cancellationToken, TaskCreationOptions.LongRunning, TaskScheduler.Current);
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            this.moonAtaMainAccount.MoonAtaClient.Cancel();
            return Task.CompletedTask;
        }

        async Task Work()
        {
            logger.LogInformation("MoonAtaCrawlerBackgroundService Work");
#if DEBUG
            using (var db = serviceScopeFactory.CreateScope().ServiceProvider.GetRequiredService<BaseSourceDbContext>())
            {
                if (db.MoonAtaCandleHistories.Count() > 0) return;
            }
#endif
            DateTime LastTimeRequest = DateTime.Now.AddMinutes(-1);
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    if (DateTime.Now - LastTimeRequest >= TimeSpan.FromSeconds(60) ||
                        DateTime.Now - moonAtaMainAccount.MoonAtaClient.LastTimeRequest >= TimeSpan.FromSeconds(60))
                    {
                        var prices = await moonAtaMainAccount.MoonAtaClient.Prices_Adv(cancellationToken);
                        LastTimeRequest = DateTime.Now;

                        var prices_db = prices.Where(x => !x.IsBetSession).Select(x => new MoonAtaCandleHistory()
                        {
                            Session = x.Session,
                            OpenPrice = x.OpenPrice,
                            ClosePrice = x.ClosePrice,
                            HighPrice = x.HighPrice,
                            LowPrice = x.LowPrice,
                            CloseTime = DateTimeOffset.FromUnixTimeMilliseconds(x.CloseTime).LocalDateTime,
                            OpenTime = DateTimeOffset.FromUnixTimeMilliseconds(x.OpenTime).LocalDateTime,
                        }).ToList();

                        using var db = serviceScopeFactory.CreateScope().ServiceProvider.GetRequiredService<BaseSourceDbContext>();
                        await db.MoonAtaCandleHistories
                            .UpsertRange(prices_db)
                            .On(x => x.Session)
                            .RunAsync(cancellationToken);
                    }
                    else await Task.Delay(1000, cancellationToken);
                }
                catch (HttpRequestException hre)
                {
                    logger.LogError(hre, "main loop MoonAtaCrawlerBackgroundService");
                    await Task.Delay(5000, cancellationToken);
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "main loop MoonAtaCrawlerBackgroundService");
                    await Task.Delay(30000, cancellationToken);
                }
            }
            logger.LogCritical("MoonAtaCrawlerBackgroundService.Stop");
        }
    }
}
