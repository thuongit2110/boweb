﻿using BaseSource.Data.EF;
using BaseSource.ViewModels.Common;
using BaseSource.ViewModels.DocumentMoonAta;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BaseSource.BackendApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentMoonAtaController : BaseApiController
    {
        private readonly BaseSourceDbContext _db;
        private readonly IWebHostEnvironment _appEnvironment;

        public DocumentMoonAtaController(BaseSourceDbContext context, IWebHostEnvironment appEnvironment)
        {
            _db = context;
            _appEnvironment = appEnvironment;
        }

        [HttpGet("GetAlls")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAlls()
        {
            var data = await _db.DocumentMoonAtas.Select(x => new DocumentMoonAtaVm()
            {
                FileUrl = string.IsNullOrEmpty(x.FileUrl) ? null : Url.Action("Index", "Home", null, Request.Scheme) + x.FileUrl,
                UrlVideo = x.UrlVideo
            }).FirstOrDefaultAsync();

            return Ok(new ApiSuccessResult<DocumentMoonAtaVm>(data));
        }
    }
}
