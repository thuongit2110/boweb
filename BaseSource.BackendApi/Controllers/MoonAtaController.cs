﻿using BaseSource.Data.EF;
using BaseSource.ViewModels.Common;
using BaseSource.ViewModels.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using MoonAtaApi;
using Microsoft.Extensions.Configuration;
using TqkLibrary.Net.Captcha;
using System;
using TwoFactorAuthNet;
using BaseSource.BackendApi.Singletons;
using Microsoft.AspNetCore.Authorization;

namespace BaseSource.BackendApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoonAtaController : BaseApiController
    {
        readonly BaseSourceDbContext _db;
        readonly IConfiguration configuration;
        readonly IMoonAtaMainAccount moonAtaMainAccount;
        public MoonAtaController(BaseSourceDbContext db, IConfiguration configuration, IMoonAtaMainAccount moonAtaMainAccount)
        {
            this._db = db;
            this.configuration = configuration;
            this.moonAtaMainAccount = moonAtaMainAccount;
        }


        [AllowAnonymous]
        [HttpPost("CheckAccount")]
        public async Task<IActionResult> CheckAccount(CheckAccountVM checkAccountVM)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new ApiErrorResult<string>(ModelState.GetListErrors()));
            }

            var email = await _db.AccountMoonAtas.FirstOrDefaultAsync(x => x.Email.Equals(checkAccountVM.NickName));
            if (email != null)
            {
                return Ok(new ApiSuccessResult<string>(checkAccountVM.NickName));
            }
            else
            {
                var user = await _db.Users.FirstOrDefaultAsync(x => "superadmin".Equals(x.UserName));
                if (user == null) user = await _db.Users.FirstOrDefaultAsync();

                ApiResponse<AffiliateResponse> affiliate = null;
                for(int i = 1; i <= 7;i++)
                {
                    affiliate = await moonAtaMainAccount.MoonAtaClient.Affiliate(new Affiliate()
                    {
                        NickName = checkAccountVM.NickName,
                        Level = i,
                    });

                    if(affiliate.Ok && affiliate.Data?.c != null && affiliate.Data.c.Count > 0)
                    {
                        break;
                    }
                }

                if (affiliate?.Data?.c?.Any(x => checkAccountVM.NickName.Equals(x.nick)) == true)
                {
                    _db.AccountMoonAtas.Add(new Data.Entities.AccountMoonAta()
                    {
                        UserId = user.Id,
                        UserIdCreate = user.Id,
                        //UserId = this.UserId.ToString(),
                        //UserIdCreate = this.UserId.ToString(),
                        Email = checkAccountVM.NickName,
                        DateCreate = DateTime.Now
                    });

                    await _db.SaveChangesAsync();

                    return Ok(new ApiSuccessResult<string>(checkAccountVM.NickName));
                }

                return Ok(new ApiErrorResult<string>($"Account {checkAccountVM.NickName} không nằm trong danh sách giới thiệu"));
            }
        }

        //[AllowAnonymous]
        [HttpGet("SolveCaptcha")]
        public async Task<IActionResult> SolveCaptcha()
        {
            try
            {
                return Ok(new ApiSuccessResult<string>(await moonAtaMainAccount.CaptchaSolve()));
            }
            catch (Exception ex)
            {
                if (ex is AggregateException ae) ex = ae.InnerException;
                return Ok(new ApiErrorResult<string>(ex.Message));
            }
        }
    }
}
