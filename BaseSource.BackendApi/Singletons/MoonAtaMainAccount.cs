﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MoonAtaApi;
using System;
using System.Threading.Tasks;
using TqkLibrary.Net.Captcha;
using TwoFactorAuthNet;
namespace BaseSource.BackendApi.Singletons
{
    public interface IMoonAtaMainAccount
    {
        MoonAtaClient MoonAtaClient { get; }
        void Init();
        Task<string> CaptchaSolve();
    }
    public class MoonAtaMainAccount : IMoonAtaMainAccount, IDisposable
    {
        readonly ILogger<MoonAtaMainAccount> logger;
        readonly IConfiguration configuration;

        TwoCaptchaApi TwoCaptchaApi { get; set; }
        AntiCaptchaApi AntiCaptchaApi { get; set; }
        public MoonAtaClient MoonAtaClient { get; private set; }
        public TwoFactorAuth TwoFactorAuth { get; }
        public MoonAtaMainAccount(
           ILogger<MoonAtaMainAccount> logger,
           IConfiguration configuration)
        {
            this.logger = logger;
            this.configuration = configuration;
            TwoFactorAuth = new TwoFactorAuth();
        }
        ~MoonAtaMainAccount()
        {
            MoonAtaClient.Dispose();
        }
        public void Dispose()
        {
            MoonAtaClient.Dispose();
            GC.SuppressFinalize(this);
        }

        public void Init()
        {
            if (MoonAtaClient != null) return;
            logger.LogInformation("MoonAtaMainAccount Init");
            string twoCaptchaKey = configuration.GetValue<string>("twoCaptchaKey");
            string antiCaptchaKey = configuration.GetValue<string>("antiCaptchaKey");

            string userName = configuration.GetValue<string>("MoonAtaCrawlerAccount:userName");
            string password = configuration.GetValue<string>("MoonAtaCrawlerAccount:password");
            string twoFA = configuration.GetValue<string>("MoonAtaCrawlerAccount:twoFA");

            this.MoonAtaClient = new MoonAtaClient(
                userName,
                password,
                () => CaptchaSolve(),
                () => this.TwoFactorAuth.GetCode(twoFA));

            if (!string.IsNullOrWhiteSpace(twoCaptchaKey)) TwoCaptchaApi = new TwoCaptchaApi(twoCaptchaKey, MoonAtaClient.CancellationToken);
            if (!string.IsNullOrWhiteSpace(antiCaptchaKey)) AntiCaptchaApi = new AntiCaptchaApi(antiCaptchaKey, MoonAtaClient.CancellationToken);
        }

        public Task<string> CaptchaSolve()
        {
            if (this.AntiCaptchaApi != null) return AntiCaptchaSolve();
            else if (this.TwoCaptchaApi != null) return TwoCaptchaSolve();
            else throw new Exception("Hiện không có key dịch vụ giải captcha");
        }


        async Task<string> AntiCaptchaSolve()
        {
            string captchaSiteKey = configuration.GetValue<string>("captchaSiteKey");
            string siteUrl = configuration.GetValue<string>("siteUrl");


            this.logger.LogInformation("AntiCaptchaApi RecaptchaV2");
            var res = await AntiCaptchaApi.CreateTask(new AntiCaptchaTask()
            {
                Type = AntiCaptchaType.RecaptchaV2TaskProxyless,
                WebsiteUrl = siteUrl,
                WebsiteKey = captchaSiteKey,
                IsInvisible = true,
            });

            AntiCaptchaTaskResultResponse result = null;
            do
            {
                MoonAtaClient.CancellationToken.ThrowIfCancellationRequested();
                result = await AntiCaptchaApi.GetTaskResult(res);
                if (result.IsComplete())
                {
                    break;
                }
                else
                {
                    await Task.Delay(3000, MoonAtaClient.CancellationToken);
                }
            }
            while (true);

            if (result.ErrorId == 0) return result.Solution.gRecaptchaResponse;
            else
            {
                logger.LogCritical($"AntiCaptchaApi: {result.ErrorId} {result.ErrorDescription}");
                throw new Exception(result.ErrorDescription);
            }
        }

        async Task<string> TwoCaptchaSolve()
        {
            string captchaSiteKey = configuration.GetValue<string>("captchaSiteKey");
            string siteUrl = configuration.GetValue<string>("siteUrl");

            this.logger.LogInformation("TwoCaptchaApi RecaptchaV2");
            var res = await TwoCaptchaApi.RecaptchaV2(captchaSiteKey, siteUrl);
            if (res.CheckState() == TwoCaptchaState.Error)
            {
                logger.LogCritical($"TwoCaptchaApi: {res.request}");
                throw new Exception(res.request);
            }
            else
            {
                res = await TwoCaptchaApi.WaitResponseJsonCompleted(res.request);
                if (res.CheckState() == TwoCaptchaState.Success) return res.request;
                throw new Exception(res.request);
            }
        }
    }
}
