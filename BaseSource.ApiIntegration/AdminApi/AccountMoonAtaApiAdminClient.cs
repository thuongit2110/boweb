﻿using BaseSource.Shared.Constants;
using BaseSource.ViewModels.Admin;
using BaseSource.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BaseSource.Utilities.Helper;

namespace BaseSource.ApiIntegration.AdminApi
{
    public class AccountMoonAtaApiAdminClient : IAccountMoonAtaApiAdminClient
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public AccountMoonAtaApiAdminClient(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public async Task<ApiResult<string>> Create(CreateAccountMoonAtaVm model)
        {
            var client = _httpClientFactory.CreateClient(SystemConstants.AppSettings.BackendApiClient);
            return await client.PostAsync<ApiResult<string>>("/api/AccountMoonAtaAdmin/Create", model);
        }

        public async Task<ApiResult<string>> Delete(int id)
        {
            var dic = new Dictionary<string, string>()
            {
                { "id", id.ToString() }
            };

            var client = _httpClientFactory.CreateClient(SystemConstants.AppSettings.BackendApiClient);
            return await client.PostAsyncFormUrl<ApiResult<string>>("/api/AccountMoonAtaAdmin/Delete", dic);
        }

        public async Task<ApiResult<PagedResult<AccountMoonAtaVm>>> GetPagings(GetAccountMoonAtaPagingRequest_Admin model)
        {
            var client = _httpClientFactory.CreateClient(SystemConstants.AppSettings.BackendApiClient);
            return await client.GetAsync<ApiResult<PagedResult<AccountMoonAtaVm>>>("/api/AccountMoonAtaAdmin/GetPagings", model);
        }
    }
}
