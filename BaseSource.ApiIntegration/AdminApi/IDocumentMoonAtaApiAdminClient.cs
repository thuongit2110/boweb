﻿using BaseSource.ViewModels.Admin;
using BaseSource.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.ApiIntegration.AdminApi
{
    public interface IDocumentMoonAtaApiAdminClient
    {
        Task<ApiResult<DocumentMoonAtaVm>> GetAlls();
        Task<ApiResult<string>> Create(CreateDocumentMoonAtaVm model);
    }
}
