﻿using BaseSource.Shared.Constants;
using BaseSource.ViewModels.Admin;
using BaseSource.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BaseSource.Utilities.Helper;

namespace BaseSource.ApiIntegration.AdminApi
{
    public class UserAdminApiClient : IUserAdminApiClient
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public UserAdminApiClient(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<ApiResult<PagedResult<UserVm>>> GetPagings(GetUserPagingRequest_Admin model)
        {
            var client = _httpClientFactory.CreateClient(SystemConstants.AppSettings.BackendApiClient);
            return await client.GetAsync<ApiResult<PagedResult<UserVm>>>("/api/UserAdmin/GetPagings", model);
        }


        public async Task<ApiResult<UserVm>> GetById(string id)
        {
            var obj = new
            {
                id = id
            };
            var client = _httpClientFactory.CreateClient(SystemConstants.AppSettings.BackendApiClient);
            return await client.GetAsync<ApiResult<UserVm>>("/api/UserAdmin/GetById", obj);
        }

        public async Task<ApiResult<RoleAssignVm>> GetUserRoles(string id)
        {
            var obj = new
            {
                id = id
            };
            var client = _httpClientFactory.CreateClient(SystemConstants.AppSettings.BackendApiClient);
            return await client.GetAsync<ApiResult<RoleAssignVm>>("/api/UserAdmin/GetUserRoles", obj);
        }

        public async Task<ApiResult<string>> RoleAssign(RoleAssignVm model)
        {
            var client = _httpClientFactory.CreateClient(SystemConstants.AppSettings.BackendApiClient);
            return await client.PostAsync<ApiResult<string>>("/api/UserAdmin/RoleAssign", model);
        }

        public async Task<ApiResult<string>> CreateUser(CreateUserVm model)
        {
            var client = _httpClientFactory.CreateClient(SystemConstants.AppSettings.BackendApiClient);
            return await client.PostAsync<ApiResult<string>>("/api/UserAdmin/CreateUser", model);
        }
        public async Task<ApiResult<string>> ResetPassword(string userId)
        {
            var dic = new Dictionary<string, string>()
            {
                { "userId", userId }
            };

            var client = _httpClientFactory.CreateClient(SystemConstants.AppSettings.BackendApiClient);
            return await client.PostAsyncFormUrl<ApiResult<string>>("/api/UserAdmin/ResetPassword", dic);
        }

        public async Task<ApiResult<string>> DeleteUser(string userId)
        {
            var dic = new Dictionary<string, string>()
            {
                { "userId", userId }
            };

            var client = _httpClientFactory.CreateClient(SystemConstants.AppSettings.BackendApiClient);
            return await client.PostAsyncFormUrl<ApiResult<string>>("/api/UserAdmin/DeleteUser", dic);
        }

        public async Task<ApiResult<string>> ChangePassword(ChangePasswordAdminVm model)
        {
            var client = _httpClientFactory.CreateClient(SystemConstants.AppSettings.BackendApiClient);
            return await client.PostAsync<ApiResult<string>>("/api/UserAdmin/ChangePassword", model);
        }
        
    }
}
