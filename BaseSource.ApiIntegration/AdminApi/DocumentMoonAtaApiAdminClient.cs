﻿using BaseSource.Shared.Constants;
using BaseSource.ViewModels.Admin;
using BaseSource.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BaseSource.Utilities.Helper;
using System.Net.Http.Headers;
using System.IO;
using Newtonsoft.Json;

namespace BaseSource.ApiIntegration.AdminApi
{
    public class DocumentMoonAtaApiAdminClient : IDocumentMoonAtaApiAdminClient
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public DocumentMoonAtaApiAdminClient(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<ApiResult<string>> Create(CreateDocumentMoonAtaVm model)
        {

            var client = _httpClientFactory.CreateClient(SystemConstants.AppSettings.BackendApiClient);
            var multiContent = new MultipartFormDataContent();
            if (model.IFileData != null)
            {
                byte[] data;
                using (var br = new BinaryReader(model.IFileData.OpenReadStream()))
                {
                    data = br.ReadBytes((int)model.IFileData.OpenReadStream().Length);
                }
                ByteArrayContent bytes = new ByteArrayContent(data);
                bytes.Headers.ContentType = MediaTypeHeaderValue.Parse(model.IFileData.ContentType);

                multiContent.Add(bytes, "IFileData", model.IFileData.FileName);
            }
            multiContent.Add(new StringContent(model.UrlVideo ?? ""), "UrlVideo");

            using (var response = await client.PostAsync("api/DocumentMoonAtaAdmin/Create", multiContent))
            {
                var responseString = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<ApiResult<string>>(responseString);
                return result;
            }
        }

        public async Task<ApiResult<DocumentMoonAtaVm>> GetAlls()
        {
            var client = _httpClientFactory.CreateClient(SystemConstants.AppSettings.BackendApiClient);
            return await client.GetAsync<ApiResult<DocumentMoonAtaVm>>("/api/DocumentMoonAtaAdmin/GetAlls");
        }
    }
}
