﻿using BaseSource.ViewModels.Admin;
using BaseSource.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.ApiIntegration.AdminApi
{
    public interface IAccountMoonAtaApiAdminClient
    {
        Task<ApiResult<PagedResult<AccountMoonAtaVm>>> GetPagings(GetAccountMoonAtaPagingRequest_Admin model);
        Task<ApiResult<string>> Create(CreateAccountMoonAtaVm model);
        Task<ApiResult<string>> Delete(int id);
    }
}
