﻿using BaseSource.Shared.Constants;
using BaseSource.ViewModels.Admin;
using BaseSource.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BaseSource.Utilities.Helper;


namespace BaseSource.ApiIntegration.AdminApi
{
    public class MoonAtaCandleHistoryApiAdminClient : IMoonAtaCandleHistoryApiAdminClient
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public MoonAtaCandleHistoryApiAdminClient(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public async Task<ApiResult<List<MoonAtaCandleHistoryVm>>> GetAlls(GetAccountMoonAtaRequestVm model)
        {
            var client = _httpClientFactory.CreateClient(SystemConstants.AppSettings.BackendApiClient);
            return await client.GetAsync<ApiResult<List<MoonAtaCandleHistoryVm>>>("/api/MoonAtaCandleHistoryAdmin/GetAlls", model);
        }
    }
}
