﻿using BaseSource.Shared.Constants;
using BaseSource.ViewModels.Common;
using BaseSource.ViewModels.DocumentMoonAta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BaseSource.Utilities.Helper;

namespace BaseSource.ApiIntegration.WebApi
{
    public class DocumentMoonAtaApiClient : IDocumentMoonAtaApiClient
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public DocumentMoonAtaApiClient(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public async Task<ApiResult<DocumentMoonAtaVm>> GetAlls()
        {
            var client = _httpClientFactory.CreateClient(SystemConstants.AppSettings.BackendApiClient);
            return await client.GetAsync<ApiResult<DocumentMoonAtaVm>>("/api/DocumentMoonAta/GetAlls");
        }
    }
}
