﻿using BaseSource.ViewModels.Common;
using BaseSource.ViewModels.DocumentMoonAta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.ApiIntegration.WebApi
{
    public interface IDocumentMoonAtaApiClient
    {
        Task<ApiResult<DocumentMoonAtaVm>> GetAlls();
    }
}
