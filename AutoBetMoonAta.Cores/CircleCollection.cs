﻿using AutoBetMoonAta.Cores.Enums;
using AutoBetMoonAta.Cores.Interfaces;
using AutoBetMoonAta.Cores.Utils;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace AutoBetMoonAta.Cores
{
    //init -> calc bet -> ApplyServerResult -> update UI -> reset
    public class CircleCollection : Circle.BaseCircleCollection
    {
        public CircleCollection(IMoneyStepSettingUCVM moneyStepSettingUCVM, ICircleSettingUCVM circleSettingUCVM) : base(moneyStepSettingUCVM, circleSettingUCVM)
        {
            if (moneyStepSettingUCVM.MoneyStepTypesSelected == MoneyStepType.MoneyResultUpDown)
                _MoneyIndex = moneyStepSettingUCVM.MoneyIndexAdvancedStart;
        }
        int _MultiplierLostAndWinCount = 0;
        int _MoneyIndex = 0;
        public int MoneyIndex { get { return _MoneyIndex; } }
        public bool IsSplitMoneyFlow { get { return MoneyStepSettingUCVM.AccountVM.CircleSettingUCVM.IsSplitMoneyFlow; } }



        Tuple<bool, double> lastBet = null;
        /// <summary>
        /// true -> Buy,money
        /// </summary>
        /// <returns></returns>
        public async Task<Tuple<bool, double>> GetBet()
        {
            lastBet = null;
            if (IsSplitMoneyFlow)
            {
                var group = this.Where(x => !x.IsWaitCondition && !x.IsPermanentStop && !x.IsPause).GroupBy(x => x.CurrentActionIsBuyWithInverted);
                double sum_b = 0;
                double sum_s = 0;
                if (this.Count > Singleton.ItemPerThread * 2)
                {
                    var sum_b_tasks = group.FirstOrDefault(x => x.Key == true).ToList().ParallelWorkAsync(x => x.Sum(y => y.CurrentMoney));
                    var sum_s_tasks = group.FirstOrDefault(x => x.Key == false).ToList().ParallelWorkAsync(x => x.Sum(y => y.CurrentMoney));
                    await Task.WhenAll(sum_b_tasks, sum_s_tasks);
                    sum_b = sum_b_tasks.Result.Sum();
                    sum_s = sum_s_tasks.Result.Sum();
                }
                else
                {
                    sum_b = group.FirstOrDefault(x => x.Key == true)?.Sum(x => x.CurrentMoney) ?? 0;
                    sum_s = group.FirstOrDefault(x => x.Key == false)?.Sum(x => x.CurrentMoney) ?? 0;
                }

                if (sum_b > sum_s)
                {
                    lastBet = new Tuple<bool, double>(true, sum_b - sum_s);
                }
                else if (sum_s > sum_b)
                {
                    lastBet = new Tuple<bool, double>(false, sum_s - sum_b);
                }
            }
            else
            {
                var group = this.Where(x => !x.IsWaitCondition && !x.IsPermanentStop && !x.IsPause).GroupBy(x => x.CurrentActionIsBuyWithInverted);
                int count_s = group.FirstOrDefault(x => x.Key == false)?.Count() ?? 0;
                int count_b = group.FirstOrDefault(x => x.Key == true)?.Count() ?? 0;

                if (count_b > count_s)
                {
                    lastBet = new Tuple<bool, double>(true, MoneyStepSettingUCVM.StepResults[MoneyIndex % MoneyStepSettingUCVM.StepResults.Length]);
                }
                else if (count_s > count_b)
                {
                    lastBet = new Tuple<bool, double>(false, MoneyStepSettingUCVM.StepResults[MoneyIndex % MoneyStepSettingUCVM.StepResults.Length]);
                }

                if (lastBet != null)
                {
                    if (MoneyStepSettingUCVM.MoneyStepTypesSelected == MoneyStepType.MultiplierLoseAndWin)
                        lastBet = new Tuple<bool, double>(
                            lastBet.Item1,
                            lastBet.Item2 * Math.Pow(MoneyStepSettingUCVM.MoneyResultUpDownMulti, _MultiplierLostAndWinCount));
                }
            }

            return lastBet;
        }

        public void UpdateWinLostNonSplitMoneyFlow(ServerResultHistory serverHistory)
        {
            if (!MoneyStepSettingUCVM.AccountVM.AutoData.IsPause && !IsSplitMoneyFlow && lastBet != null)//not run on startup
            {
                bool isBuy = (serverHistory.History & 1) == 1;
                bool isWin = lastBet.Item1 == isBuy;

                //update money
                MoneyStepSettingUCVM.UpdateMoneyIndex(isWin, ref _MoneyIndex, ref _MultiplierLostAndWinCount);
            }
        }
    }
}
