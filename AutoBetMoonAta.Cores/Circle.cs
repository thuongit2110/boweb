﻿using AutoBetMoonAta.Cores.Enums;
using AutoBetMoonAta.Cores.Interfaces;
using AutoBetMoonAta.Cores.Utils;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace AutoBetMoonAta.Cores
{
    public class Circle
    {
        uint Condition = 0;//[tail ------ head]
        readonly int ConditionLength;

        uint Action = 0;//[tail ------ head]
        readonly int ActionLength;
        int ActionIndex = 0;

        public uint ActionResult { get; private set; } = 0;//[tail ------ head]
        public int ActionResultCount { get; private set; } = 0;

        public bool IsPermanentStop { get; private set; } = false;

        /// <summary>
        /// skip bet only
        /// </summary>
        public bool IsPause { get { return PauseCount > 0; } }

        private bool IsInverted { get { return InvertedCount > 0; } }

        int _MoneyIndex = 0;
        public int MoneyIndex { get { return _MoneyIndex; } }



        public int WinCount { get; private set; } = 0;
        public int LostCount { get; private set; } = 0;


        public int WinCountForReset { get; private set; } = 0;
        public int LostCountForReset { get; private set; } = 0;

        private int WinCountForPermanentStop { get; set; } = 0;
        private int LoseCountForPermanentStop { get; set; } = 0;
        private double TotalMoneyWin { get; set; } = 0;
        private double TotalMoneyLose { get; set; } = 0;
        private int PauseCount { get; set; } = 0;
        private int InvertedCount { get; set; } = 0;
        /// <summary>
        /// Non Condition alway false
        /// </summary>
        public bool IsWaitCondition { get; private set; } = true;


        int MultiplierLostAndWinCount = 0;


        public bool IsCondition { get { return ConditionLength > 0; } }

        public bool CurrentActionIsBuyWithInverted
        {
            get
            {
                return IsInverted ? !CurrentActionIsBuy : CurrentActionIsBuy;
            }
        }
        public bool CurrentActionIsBuy
        {
            get
            {
                return (Action >> (ActionIndex % ActionLength) & 1) == 1;
            }
        }

        public double CurrentMoney
        {
            get
            {
                if (MoneyStepSettingUCVM.MoneyStepTypesSelected == MoneyStepType.MultiplierLoseAndWin)
                    return MoneyStepSettingUCVM.StepResults[MoneyIndex % MoneyStepSettingUCVM.StepResults.Length] *
                        Math.Pow(MoneyStepSettingUCVM.MoneyResultUpDownMulti, MultiplierLostAndWinCount);
                else
                    return MoneyStepSettingUCVM.StepResults[MoneyIndex % MoneyStepSettingUCVM.StepResults.Length];
            }
        }

        public ICircleVM CircleVM { get; }

        public event Action<Circle> OnUpdateUI;
        //[head ------ tail]
        public Circle(ICircleVM circleVM, int startMoneyIndex = 0)
        {
            this.CircleVM = circleVM ?? throw new ArgumentNullException(nameof(circleVM));

            this._MoneyIndex = startMoneyIndex;

            var conditionArr = circleVM.Condition.Reverse().ToArray();
            ConditionLength = circleVM.Condition.Length;
            if (ConditionLength == 0) IsWaitCondition = false;
            for (int i = 0; i < ConditionLength; i++)
            {
                Condition |= (conditionArr[i] == 'B' ? 1u : 0u) << i;
            }

            var actionArr = circleVM.Action.ToArray();
            ActionLength = circleVM.Action.Length;
            for (int i = 0; i < ActionLength; i++)
            {
                Action |= (actionArr[i] == 'B' ? 1u : 0u) << i;
            }

        }

        public void UpdateIsWaitCondition(ServerResultHistory serverHistory)
        {
            if (IsCondition && IsWaitCondition && serverHistory.HistoryLength >= ConditionLength)//set next Condition if is wait
            {
#if DEBUG
                DebugData();
#endif
                int nonConditionLength = 32 - ConditionLength;
                IsWaitCondition = !(((serverHistory.History << nonConditionLength) >> nonConditionLength) == Condition);
            }
        }

        public void UpdateToUI()
        {
            OnUpdateUI?.Invoke(this);
        }

        public void UpdateWinLostAndReset(ServerResultHistory serverHistory)
        {
            if (serverHistory.HistoryLength == 0) return;

            bool isBuy = (serverHistory.History & 1) == 1;
            if (!IsPause && (!IsWaitCondition || !IsCondition))//only non Condition or Condition allowed
            {
                bool isWin = CurrentActionIsBuy == isBuy;
                //update money
#if DEBUG
                DebugData();
#endif
                if (isWin) TotalMoneyWin += CurrentMoney;
                else TotalMoneyLose += CurrentMoney;

                MoneyStepSettingUCVM.UpdateMoneyIndex(isWin, ref _MoneyIndex, ref MultiplierLostAndWinCount);

                //update result
                if (ActionResultCount >= ActionLength)
                {
                    ActionResultCount = 0;
                    ActionResult = 0;
                }
                ActionResult |= (isWin ? 1u : 0) << ActionResultCount;
                ActionResultCount++;

                //update win/lose count
                if (isWin)
                {
                    WinCountForReset++;
                    WinCount++;
                    LostCountForReset = 0;
                    WinCountForPermanentStop++;
                    LoseCountForPermanentStop = 0;
                }
                else
                {
                    WinCountForReset = 0;
                    LostCountForReset++;
                    LostCount++;
                    LoseCountForPermanentStop++;
                    WinCountForPermanentStop = 0;
                }
                ActionIndex++;
            }



            InvertedCount--;
            if (IsPause)
            {
                PauseCount--;
            }
            else if (!IsWaitCondition || !IsCondition)//only non Condition or Condition allowed
            {
                if (WinCountForPermanentStop >= MoneyStepSettingUCVM.AccountVM.AdvancedConfigUCVM.CirclePermanentStopOnWinConsecutive ||
                        LoseCountForPermanentStop >= MoneyStepSettingUCVM.AccountVM.AdvancedConfigUCVM.CirclePermanentStopOnLoseConsecutive ||
                        TotalMoneyWin >= MoneyStepSettingUCVM.AccountVM.AdvancedConfigUCVM.CirclePermanentStopAtWinMoney ||
                        TotalMoneyLose >= MoneyStepSettingUCVM.AccountVM.AdvancedConfigUCVM.CirclePermanentStopAtLoseMoney)
                {
                    IsPermanentStop = true;
                }

                if (WinCountForPermanentStop >= MoneyStepSettingUCVM.AccountVM.AdvancedConfigUCVM.CirclePauseOnWinConsecutive)
                {
                    PauseCount = MoneyStepSettingUCVM.AccountVM.AdvancedConfigUCVM.CirclePauseOnWinCount;
                }
                else if (LoseCountForPermanentStop >= MoneyStepSettingUCVM.AccountVM.AdvancedConfigUCVM.CirclePauseOnLoseConsecutive)
                {
                    PauseCount = MoneyStepSettingUCVM.AccountVM.AdvancedConfigUCVM.CirclePauseOnLoseCount;
                }

                if (WinCountForPermanentStop >= MoneyStepSettingUCVM.AccountVM.AdvancedConfigUCVM.CircleInvertedOnWinConsecutive)
                {
                    InvertedCount = MoneyStepSettingUCVM.AccountVM.AdvancedConfigUCVM.CircleInvertedOnWinCount;
                }
                else if (LoseCountForPermanentStop >= MoneyStepSettingUCVM.AccountVM.AdvancedConfigUCVM.CircleInvertedOnLoseConsecutive)
                {
                    InvertedCount = MoneyStepSettingUCVM.AccountVM.AdvancedConfigUCVM.CircleInvertedOnLoseCount;
                }

                //reset if reach to win/lose count
                if (WinCountForReset >= CircleSettingUCVM.ResetWin)
                {
                    WinCountForReset = 0;
                    ActionIndex = 0;
                    ActionResult = 0;//clear bit
                    ActionResultCount = 0;
                    if (IsCondition) IsWaitCondition = true;
                }
                else if (LostCountForReset >= CircleSettingUCVM.ResetLose)
                {
                    LostCountForReset = 0;
                    ActionIndex = 0;
                    ActionResult = 0;//clear bit
                    ActionResultCount = 0;
                    if (IsCondition) IsWaitCondition = true;
                }
            }
        }


#if DEBUG
        void DebugData()
        {
            //if (CircleVM.Data.Equals("BSBBBSBSBB-SSBBSSSSS"))
            //{

            //}
        }
#endif



        public IMoneyStepSettingUCVM MoneyStepSettingUCVM { get; private set; }
        public ICircleSettingUCVM CircleSettingUCVM { get; private set; }
        public class BaseCircleCollection : Collection<Circle>
        {
            public readonly IMoneyStepSettingUCVM MoneyStepSettingUCVM;
            public readonly ICircleSettingUCVM CircleSettingUCVM;

            internal BaseCircleCollection(IMoneyStepSettingUCVM moneyStepSettingUCVM, ICircleSettingUCVM circleSettingUCVM)
            {
                this.MoneyStepSettingUCVM = moneyStepSettingUCVM ?? throw new ArgumentNullException(nameof(moneyStepSettingUCVM));
                this.CircleSettingUCVM = circleSettingUCVM ?? throw new ArgumentNullException(nameof(circleSettingUCVM));
            }

            protected override void InsertItem(int index, Circle item)
            {
                item.MoneyStepSettingUCVM = MoneyStepSettingUCVM;
                item.CircleSettingUCVM = CircleSettingUCVM;
                base.InsertItem(index, item);
            }

            protected override void SetItem(int index, Circle item)
            {
                item.MoneyStepSettingUCVM = MoneyStepSettingUCVM;
                item.CircleSettingUCVM = CircleSettingUCVM;
                base.SetItem(index, item);
            }
        }
    }
}