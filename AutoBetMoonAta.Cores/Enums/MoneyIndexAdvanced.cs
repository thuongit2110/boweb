﻿using AutoBetMoonAta.Cores.Utils;

namespace AutoBetMoonAta.Cores.Enums
{
    public enum MoneyIndexAdvanced
    {
        [Name("Mốc tiền tăng khi LOST - giảm khi WIN")]
        UpLostDownWin,
        [Name("Mốc tiền giảm khi LOST - tăng khi WIN")]
        DownLostUpWin,
    }
}
