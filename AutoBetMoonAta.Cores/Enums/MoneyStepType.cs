﻿using AutoBetMoonAta.Cores.Utils;

namespace AutoBetMoonAta.Cores.Enums
{
    public enum MoneyStepType
    {
        [Name("Gấp Lose")]
        MultiplierLose,
        [Name("Gấp Win")]
        MultiplierWin,
        [Name("Gấp Lose và Win")]
        MultiplierLoseAndWin,
        [Name("Tiền vào tăng giảm")]
        MoneyResultUpDown
    }
}
