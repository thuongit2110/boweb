﻿using AutoBetMoonAta.Cores.Interfaces;
using System;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutoBetMoonAta.Cores
{
    public class ServerResultHistory : IDisposable
    {
        readonly SemaphoreSlim _signal = new SemaphoreSlim(0);
        public uint History { get; private set; } = 0;
        internal int HistoryLength { get; private set; } = 0;

        public event Action<string> OnNewPushResult;
        public ServerResultHistory()
        {
        }
        ~ServerResultHistory()
        {
            _signal.Dispose();
        }

        public void ClearSignal()
        {
            Debug.WriteLine("ServerResultHistory.ClearSignal");
            while (_signal.Wait(0)) { }
        }

        public Task<bool> WaitAsync(int timeout, CancellationToken cancellationToken)
        {
            Debug.WriteLine($"ServerResultHistory.WaitAsync {_signal.CurrentCount}");
            return _signal.WaitAsync(timeout,cancellationToken);
        }

        public void Push(bool isUp)
        {
            if (HistoryLength < 32) HistoryLength++;
            History = (History << 1) | (isUp ? 1u : 0);
            Debug.WriteLine($"ServerResultHistory.Release {_signal.CurrentCount}");
            _signal.Release();
            OnNewPushResult?.Invoke(this.ToString());
        }


        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = Math.Min(15, HistoryLength - 1); i >= 0; i--)
            {
                stringBuilder.Append(((History >> i) & 1) == 1 ? "B" : "S");
            }
            return stringBuilder.ToString();
        }

        public void Dispose()
        {
            _signal.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
