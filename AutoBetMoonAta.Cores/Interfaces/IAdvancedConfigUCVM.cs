﻿namespace AutoBetMoonAta.Cores.Interfaces
{
    public interface IAdvancedConfigUCVM
    {
        int CircleInvertedOnLoseConsecutive { get; }
        int CircleInvertedOnLoseCount { get; }
        int CircleInvertedOnWinConsecutive { get; }
        int CircleInvertedOnWinCount { get; }
        int CirclePauseOnLoseConsecutive { get; }
        int CirclePauseOnLoseCount { get; }
        int CirclePauseOnWinConsecutive { get; }
        int CirclePauseOnWinCount { get; }
        double CirclePermanentStopAtLoseMoney { get; }
        double CirclePermanentStopAtWinMoney { get; }
        int CirclePermanentStopOnLoseConsecutive { get; }
        int CirclePermanentStopOnWinConsecutive { get; }
    }
}