﻿namespace AutoBetMoonAta.Cores.Interfaces
{
    public interface ICircleVM
    {
        string Action { get; }
        uint ActionActive { get; }
        int ActionResultCount { get; }
        string Condition { get; }
        int LoseCount { get; }
        int MoneyIndex { get; }
        int WinCount { get; }
        bool IsActive { get; }
    }
}