﻿using MoonAtaApi;

namespace AutoBetMoonAta.Cores.Interfaces
{
    public interface IAutoData
    {
        bool IsPause { get; }
    }
}