﻿using AutoBetMoonAta.Cores.Enums;

namespace AutoBetMoonAta.Cores.Interfaces
{
    public interface IMoneyStepSettingUCVM
    {
        IAccountVM AccountVM { get; }
        int MoneyIndexAdvancedStart { get; }
        double MoneyResultUpDownMulti { get; }
        MoneyStepType MoneyStepTypesSelected { get; }
        MoneyIndexAdvanced MoneyIndexAdvancedsSelected { get; }
        int MoneyIndexAdvancedIncrease { get; }
        int MoneyIndexAdvancedDecrease { get; }
        int MoneyResultUpDownSessionCount { get; }
        double[] StepResults { get; }
    }
}