﻿using AutoBetMoonAta.Cores;
using System.Collections.Generic;

namespace AutoBetMoonAta.Cores.Interfaces
{
    public interface IAccountVM
    {
        string ServerResultHistoryString { get; set; }
        bool IsTestMode { get; }
        IAdvancedConfigUCVM AdvancedConfigUCVM { get; }
        ICircleSettingUCVM CircleSettingUCVM { get; }
        IAutoData AutoData { get; }
    }
}