﻿namespace AutoBetMoonAta.Cores.Interfaces
{
    public interface ICircleSettingUCVM
    {
        int ResetLose { get; }
        int ResetWin { get; }
        bool IsSplitMoneyFlow { get; }
    }
}