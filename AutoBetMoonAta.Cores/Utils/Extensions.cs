﻿using AutoBetMoonAta.Cores.Enums;
using AutoBetMoonAta.Cores.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutoBetMoonAta.Cores.Utils
{
    public static class Extensions
    {
        public static void UpdateMoneyIndex(this IMoneyStepSettingUCVM moneyStepSettingUCVM,
            bool isWin, ref int moneyIndex, ref int MultiplierLostAndWinCount)
        {
            switch (moneyStepSettingUCVM.MoneyStepTypesSelected)
            {
                case MoneyStepType.MultiplierLose:
                    {
                        MultiplierLostAndWinCount = 0;
                        if (isWin) moneyIndex = 0;
                        else moneyIndex++;
                        break;
                    }

                case MoneyStepType.MultiplierWin:
                    {
                        MultiplierLostAndWinCount = 0;
                        if (!isWin) moneyIndex = 0;
                        else moneyIndex++;
                        break;
                    }

                case MoneyStepType.MoneyResultUpDown:
                    {
                        MultiplierLostAndWinCount = 0;
                        switch (moneyStepSettingUCVM.MoneyIndexAdvancedsSelected)
                        {
                            case MoneyIndexAdvanced.UpLostDownWin:
                                {
                                    if (!isWin) moneyIndex += moneyStepSettingUCVM.MoneyIndexAdvancedIncrease;
                                    else moneyIndex -= moneyStepSettingUCVM.MoneyIndexAdvancedDecrease;
                                    break;
                                }

                            case MoneyIndexAdvanced.DownLostUpWin:
                                {
                                    if (isWin) moneyIndex += moneyStepSettingUCVM.MoneyIndexAdvancedIncrease;
                                    else moneyIndex -= moneyStepSettingUCVM.MoneyIndexAdvancedDecrease;
                                    break;
                                }

                            default: throw new NotSupportedException();
                        }
                        break;
                    }

                case MoneyStepType.MultiplierLoseAndWin:
                    {
                        if (isWin)
                        {
                            MultiplierLostAndWinCount++;
                            if (MultiplierLostAndWinCount >= moneyStepSettingUCVM.MoneyResultUpDownSessionCount)
                            {
                                MultiplierLostAndWinCount = 0;
                                moneyIndex = 0;
                            }
                        }
                        else
                        {
                            moneyIndex++;
                            MultiplierLostAndWinCount = 0;
                        }
                        break;
                    }

                default: throw new NotSupportedException();
            }
            if (moneyIndex < 0) moneyIndex = 0;
        }

        public static async Task ParallelWorkAsync<T>(this IList<T> list, Action<T> action)
        {
            if (list == null || list.Count == 0) return;
            if (action == null) throw new ArgumentNullException(nameof(action));

            int pageCount = (int)Math.Ceiling((double)list.Count / Singleton.ItemPerThread);
            if (pageCount == 1)
            {
                foreach (var item in list) action.Invoke(item);
            }
            else
            {
                Task[] tasks = new Task[pageCount];
                for (int i = 0; i < pageCount; i++)
                {
                    var page = list.Skip(Singleton.ItemPerThread * i).Take(Singleton.ItemPerThread);
                    tasks[i] = Task.Factory.StartNew(() => { foreach (var item in page) action.Invoke(item); }, CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Default);
                }
                await Task.WhenAll(tasks);
            }
        }

        public static async Task ParallelWorkAsync<T>(this IList<T> list, Func<T, Task> func)
        {
            if (list == null || list.Count == 0) return;
            if (func == null) throw new ArgumentNullException(nameof(func));

            int pageCount = (int)Math.Ceiling((double)list.Count / Singleton.ItemPerThread);
            if (pageCount == 1)
            {
                foreach (var item in list) await func.Invoke(item);
            }
            else
            {
                Task[] tasks = new Task[pageCount];
                for (int i = 0; i < pageCount; i++)
                {
                    var page = list.Skip(Singleton.ItemPerThread * i).Take(Singleton.ItemPerThread);
                    tasks[i] = Task.Factory.StartNew(() => { foreach (var item in page) func.Invoke(item).Wait(); }, CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Default);
                }
                await Task.WhenAll(tasks);
            }
        }


        public static async Task<IEnumerable<TResult>> ParallelWorkAsync<T, TResult>(this IList<T> list, Func<IEnumerable<T>, TResult> func)
        {
            if (list == null || list.Count == 0) return new List<TResult>();
            if (func == null) throw new ArgumentNullException(nameof(func));

            List<TResult> results = new List<TResult>();

            int pageCount = (int)Math.Ceiling((double)list.Count / Singleton.ItemPerThread);
            if (pageCount == 1)
            {
                results.Add(func.Invoke(list));
            }
            else
            {
                Task<TResult>[] tasks = new Task<TResult>[pageCount];
                for (int i = 0; i < pageCount; i++)
                {
                    var page = list.Skip(Singleton.ItemPerThread * i).Take(Singleton.ItemPerThread);
                    tasks[i] = Task.Factory.StartNew(() => func.Invoke(page), CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Default);
                }
                await Task.WhenAll(tasks);
                results.AddRange(tasks.Select(x => x.Result));
            }
            return results;
        }
    }
}
