﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.ViewModels.DocumentMoonAta
{
    public class DocumentMoonAtaVm
    {
        public string UrlVideo { get; set; }
        public string FileUrl { get; set; }
        public string HomeModalPopUp { get; set; }
    }
}
