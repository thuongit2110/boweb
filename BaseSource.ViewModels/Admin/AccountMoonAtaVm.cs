﻿using BaseSource.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.ViewModels.Admin
{
    public class GetAccountMoonAtaPagingRequest_Admin : PageQuery
    {
        public string UserName { get; set; }
        public string Email { get; set; }
    }
    public class AccountMoonAtaVm
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
    }
    public class CreateAccountMoonAtaVm
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string UserName { get; set; }

    }
}
