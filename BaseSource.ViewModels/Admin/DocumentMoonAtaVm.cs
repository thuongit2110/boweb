﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.ViewModels.Admin
{
    public class DocumentMoonAtaVm
    {
        public int Id { get; set; }
        public string UrlVideo { get; set; }
        public string FileUrl { get; set; }
        public IFormFile IFileData { get; set; }
        public string FileName { get; set; }

    }
    public class CreateDocumentMoonAtaVm
    {
        public string UrlVideo { get; set; }
        public string FileUrl { get; set; }
        public IFormFile IFileData { get; set; }
        public string FileName { get; set; }

    }
}
