﻿using BaseSource.ViewModels.Common;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.ViewModels.Admin
{
    public class GetKeywordPagingRequest_Admin : PageQuery
    {
        public string Keyword { get; set; }
    }
    public class KeywordVm
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string FileData { get; set; }
        public string Password { get; set; }
        public string LinkGetPass { get; set; }
        public string DowloadToken { get; set; }
        public string Body { get; set; }
        public int TotalDowload { get; set; }
        public string Url { get; set; }
        public DateTime DateCreate { get; set; }
        public string FileName { get; set; }
    }
    public class CreateKeywordVm
    {
        [Required]
        public string Key { get; set; }
        public string FileName { get; set; }
        public string FileData { get; set; }
        public IFormFile IFileData { get; set; }
        [Required]
        public string Password { get; set; }
        public string LinkGetPass { get; set; }
        [Required]
        public string Body { get; set; }
    }
    public class EditKeywordVm : CreateKeywordVm
    {
        public int Id { get; set; }
    }
    public class FileDataVm
    {
        public string FilePath { get; set; }
        public string FileName { get; set; }
    }
}
