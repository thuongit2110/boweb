﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.ViewModels.Admin
{
    public class GetAccountMoonAtaRequestVm
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
    public class MoonAtaCandleHistoryVm
    {
        public double OpenPrice { get; set; }
        public double ClosePrice { get; set; }
        public DateTime CloseTime { get; set; }
        public bool IsGreen { get { return ClosePrice >= OpenPrice; } }
    }
}
