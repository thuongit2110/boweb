﻿using System.ComponentModel.DataAnnotations;

namespace BaseSource.ViewModels.User
{
    public class CheckAccountVM
    {
        [Required]
        public string NickName { get; set; }
    }
}
