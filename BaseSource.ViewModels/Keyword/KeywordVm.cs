﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.ViewModels.Keyword
{
    public class KeywordVm
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string FileData { get; set; }
        public string Password { get; set; }
        public string LinkGetPass { get; set; }
        public string DowloadToken { get; set; }
        public string Body { get; set; }
        public int TotalDowload { get; set; }
        public string Url { get; set; }
        public DateTime DateCreate { get; set; }
        public string FileName { get; set; }
    }
    public class FileDataVm
    {
        public string FilePath { get; set; }
        public string FileName { get; set; }
    }
    public class DowloadFileVm
    {
        public int Id { get; set; }
        public string DowloadToken { get; set; }
        [Required]
        public string Pass { get; set; }
        public string Body { get; set; }
    }
}
