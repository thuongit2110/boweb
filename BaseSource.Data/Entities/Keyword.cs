﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.Data.Entities
{
    public class Keyword
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string FileData { get; set; }
        public string FileName { get; set; }
        public string Password { get; set; }
        public string LinkGetPass { get; set; }
        public string DowloadToken { get; set; }
        public string Body { get; set; }
        public int TotalDowload { get; set; }
        public string Url { get; set; }
        public string UserIdCreate { get; set; }
        public DateTime DateCreate { get; set; }

    }
}
