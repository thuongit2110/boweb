﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.Data.Entities
{
    public class MoonAtaCandleHistory
    {
        public int Id { get; set; }
        public int Session { get; set; }
        public double OpenPrice { get; set; }
        public double HighPrice { get; set; }
        public double LowPrice { get; set; }
        public double ClosePrice { get; set; }
        public DateTime CloseTime { get; set; }
        public DateTime OpenTime { get; set; }

        [NotMapped]
        public bool IsGreen { get { return ClosePrice >= OpenPrice; } }
    }
}
