﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.Data.Entities
{
    public class AccountMoonAta
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string UserId { get; set; }
        public string UserIdCreate { get; set; }
        public DateTime DateCreate { get; set; }
        public virtual AppUser AppUser { get; set; }


    }
}
