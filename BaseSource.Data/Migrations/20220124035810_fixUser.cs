﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BaseSource.Data.Migrations
{
    public partial class fixUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: "82465f59-9f2e-4a66-ba9d-61244aba00a3");

            migrationBuilder.DeleteData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: "86cd77f8-4984-4812-ad15-eb38aff92acb");

            migrationBuilder.AddColumn<string>(
                name: "UserIdCreate",
                table: "AppUsers",
                type: "nvarchar(128)",
                maxLength: 128,
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: "c1105ce5-9dbc-49a9-a7d5-c963b6daa62a",
                column: "ConcurrencyStamp",
                value: "128365bf-bfdb-4652-8b9d-5c62e3276e3d");

            migrationBuilder.InsertData(
                table: "AppRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "cd26bc76-7015-41bd-8800-53d046e5ebdf", "afaf43cc-5807-4fdd-8288-b1799409bac0", "F1 role", "F1", "F1" },
                    { "7717cc90-5048-4d8d-aab6-2128e9651f5a", "f89d930b-892a-4455-ba4b-38995307eb9a", "F2 role", "F2", "F2" }
                });

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: "ffded6b0-3769-4976-841b-69459049a62d",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "e704bba9-376a-4e49-8600-cf00b9041d91", "AQAAAAEAACcQAAAAEMwhnqaQL//0nw7ZUnY2MYx663nR2qZFP2cfvwpWURgDEI3R+eH9TZKrY2TWrNcEBw==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: "7717cc90-5048-4d8d-aab6-2128e9651f5a");

            migrationBuilder.DeleteData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: "cd26bc76-7015-41bd-8800-53d046e5ebdf");

            migrationBuilder.DropColumn(
                name: "UserIdCreate",
                table: "AppUsers");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: "c1105ce5-9dbc-49a9-a7d5-c963b6daa62a",
                column: "ConcurrencyStamp",
                value: "49384a80-5d03-4f6f-9dcb-f715b3e593c1");

            migrationBuilder.InsertData(
                table: "AppRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "86cd77f8-4984-4812-ad15-eb38aff92acb", "75cf57d8-52e4-4d4f-9834-5827b850a823", "F1 role", "F1", "F1" },
                    { "82465f59-9f2e-4a66-ba9d-61244aba00a3", "23ae619f-7a4d-41a3-9b65-f7fb55444d90", "F2 role", "F2", "F2" }
                });

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: "ffded6b0-3769-4976-841b-69459049a62d",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "f23fa804-782b-4ce2-8541-64b94c8be351", "AQAAAAEAACcQAAAAEKYGIxZCS4ZNvqiTY0FVYNQEhLIkD1BuXDVFvU2fHcGO/i04ZHC+eZ0Ey2uBLdypRA==" });
        }
    }
}
