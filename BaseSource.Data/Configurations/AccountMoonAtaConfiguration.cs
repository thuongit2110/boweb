﻿using BaseSource.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.Data.Configurations
{
    public class AccountMoonAtaConfiguration : IEntityTypeConfiguration<AccountMoonAta>
    {
        public void Configure(EntityTypeBuilder<AccountMoonAta> builder)
        {
            builder.ToTable("AccountMoonAtas");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
            builder.Property(x => x.Email).IsRequired().HasMaxLength(500);
            builder.Property(x => x.UserIdCreate).IsRequired().HasMaxLength(128);
            builder.Property(x => x.DateCreate).IsRequired().HasDefaultValueSql("GetDate()");
            builder.HasOne(x => x.AppUser).WithMany(x => x.AccountMoonAtas).HasForeignKey(x => x.UserId).OnDelete(DeleteBehavior.Cascade);

        }
    }
}
