﻿using BaseSource.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.Data.Configurations
{
    internal class MoonAtaCandleHistoryConfiguration : IEntityTypeConfiguration<MoonAtaCandleHistory>
    {
        public void Configure(EntityTypeBuilder<MoonAtaCandleHistory> builder)
        {
            builder.ToTable("MoonAtaCandleHistories");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
            builder.HasIndex(x => x.Session).IsUnique();
        }
    }
}
