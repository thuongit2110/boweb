﻿using BaseSource.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.Data.Configurations
{
    public class DocumentMoonAtaConfiguration : IEntityTypeConfiguration<DocumentMoonAta>
    {
        public void Configure(EntityTypeBuilder<DocumentMoonAta> builder)
        {
            builder.ToTable("DocumentMoonAtas");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
            builder.Property(x => x.UrlVideo).HasMaxLength(2000);
            builder.Property(x => x.FileUrl).HasMaxLength(2000);
            builder.Property(x => x.FileName).HasMaxLength(500);
        }
    }
}
