﻿using Newtonsoft.Json;

namespace MoonAtaApi
{
    public class ApiResponse<T>
    {
        [JsonProperty("ok")]
        public bool Ok { get; set; }

        [JsonProperty("d")]
        public T Data { get; set; }

        [JsonProperty("m")]
        public string Message { get; set; }
    }
}
