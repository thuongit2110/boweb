﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonAtaApi
{
    public class Affiliate
    {
        public int Page { get; set; } = 1;
        public int Size { get; set; } = 10;
        public int? Level { get; set; } = 1;//1->7
        public string NickName { get; set; }
        public int Days { get; set; } = -1;

        public override string ToString()
        {
            return $"api/wallet/binaryoption/user/affiliate?page={Page}&size={Size}&lvl={Level}&nickName={NickName}&days={Days}";
        }
    }

    public class AffiliateResponse
    {
        public int t { get; set; }
        public int p { get; set; }
        public int s { get; set; }
        public List<AffiliateUser> c { get; set; }
    }

    public class AffiliateUser
    {
        public string nick { get; set; }
        public string mail { get; set; }
        public string phone { get; set; }
        public string sponsor { get; set; }
        public double agency_coms { get; set; }
        public double trading_coms { get; set; }
        public double coms { get; set; }
        public int rank { get; set; }
        public double tradevol { get; set; }
    }
}
