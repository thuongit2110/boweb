﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonAtaApi
{
    //"uid": 1641060205088,
    //    "e": "tqk2811@yahoo.com.vn",
    //    "fn": "",
    //    "ln": "",
    //    "nn": "tqk2811",
    //    "rc": "C2B447B",
    //    "crc": null,
    //    "2fa": false,
    //    "f1": 0,
    //    "cfgs": {
    //        "jwb": null
    //    },
    //    "func": {
    //"invest": true,
    //        "withdraw": true,
    //        "CASINO": true
    //    },
    //    "2fa_login_req": true,
    //    "e_daily_invest": true,
    //    "kyc": null,
    //    "confirmed_phone": null,
    //    "enable_botutorial": true

    public class ProfileResponse
    {
        public string uid { get; set; }
        public string e { get; set; }
        public string nn { get; set; }
        public string rc { get; set; }
    }
}
