﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonAtaApi
{
    public enum MoonAtaBetAccountType
    {
        DEMO,
        LIVE
    }
    public enum MoonAtaBetType
    {
        UP,
        DOWN,
    }
    public class MoonAtaBet
    {
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("betAccountType")]
        public MoonAtaBetAccountType AccountType { get; set; }



        [JsonProperty("betAmount")]
        public double Amount { get; set; }



        [JsonProperty("betType")]
        [JsonConverter(typeof(StringEnumConverter))]
        public MoonAtaBetType Type { get; set; }
    }

    public class MoonAtaBetResponse
    {
        [JsonProperty("ss")]
        public int Ss { get; set; }



        [JsonProperty("time")]
        public long Time { get; set; }


        [JsonProperty("atm")]
        public decimal Atm { get; set; }



        [JsonProperty("type")]
        public MoonAtaBetType Type { get; set; }


        [JsonProperty("acc_type")]
        public MoonAtaBetAccountType AccountType { get; set; }


        [JsonProperty("tk_amt")]
        public decimal TkAmt { get; set; }
    }
}
