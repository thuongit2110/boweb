﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonAtaApi
{
    public class PriceResponse
    {
        public PriceResponse(double[] prices)
        {
            OpenTime = (long)prices[0];
            OpenPrice = prices[1];
            HighPrice = prices[2];
            LowPrice = prices[3];
            ClosePrice = prices[4];

            CloseTime = (long)prices[6];

            IsBetSession = (int)prices[8] == 1;
            Session = (int)prices[9];
        }


        public long OpenTime { get; }//0
        public double OpenPrice { get; }//1
        public double HighPrice { get; }//2
        public double LowPrice { get; }//3
        public double ClosePrice { get; }//4

        public long CloseTime { get; }//6

        public bool IsBetSession { get; }//8
        public int Session { get; }//9


        public bool IsGreen { get { return ClosePrice >= OpenPrice; } }
    }
}
