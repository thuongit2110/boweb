﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonAtaApi
{
    public class RewardInfoResponse
    {
        public decimal coolingBalance { get; set; }
        public decimal rewardBalance { get; set; }
        public decimal totalClaimed { get; set; }
    }
}
