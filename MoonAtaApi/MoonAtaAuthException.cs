﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonAtaApi
{
    public class MoonAtaAuthException: Exception
    {
        public MoonAtaAuthException() { }
        public MoonAtaAuthException(string message):base(message) { }
    }
}
