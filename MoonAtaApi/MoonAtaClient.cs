﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.WebSockets;
using System.Security.Authentication;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace MoonAtaApi
{
    public class MoonAtaClient : IDisposable
    {
        public static Uri ApiEndPoint { get; set; } = new Uri("https://moonata1.net");
        public static Uri WebSocketEndPoint { get; set; } = new Uri("https://ws.moonata1.net");
        class Auth
        {
            public string captcha { get; set; }
            public string client_id { get; set; } = "pocinex-web";
            public string email { get; set; }
            public string grant_type { get; set; }
            public string password { get; set; }
            public string refresh_token { get; set; }
        }



        static readonly JsonSerializerSettings serializerSettings = new JsonSerializerSettings()
        {
            NullValueHandling = NullValueHandling.Ignore,
        };
        private MoonAtaToken _MoonAtaToken = null;
        public MoonAtaToken MoonAtaToken
        {
            get { return _MoonAtaToken; }
            private set { _MoonAtaToken = value; OnTokenReceived?.Invoke(value); }
        }
        public CancellationToken CancellationToken { get { return cancellationTokenSource.Token; } }

        readonly HttpClientHandler httpClientHandler;
        readonly HttpClient httpClient;

        string _userName;
        string _password;
        Func<Task<string>> _captcha;
        Func<string> _twoFA;

        readonly CancellationTokenSource cancellationTokenSource;
        public event Action<MoonAtaToken> OnTokenReceived;
        public DateTime LastTimeRequest { get; private set; }
        public MoonAtaClient()
        {
            httpClientHandler = new HttpClientHandler();
            httpClientHandler.UseCookies = true;
            httpClientHandler.CookieContainer = new CookieContainer();
            httpClientHandler.CookieContainer.Add(new Cookie("WFCOUNTRY", "vi", "/", ApiEndPoint.Host));
            httpClientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            httpClientHandler.UseDefaultCredentials = true;
            //httpClientHandler.SslProtocols = SslProtocols.Tls11 | SslProtocols.Tls12;

            httpClient = new HttpClient(httpClientHandler, true);
            httpClient.BaseAddress = ApiEndPoint;
            httpClient.DefaultRequestHeaders.Referrer = ApiEndPoint;
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            httpClient.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("deflate"));
            httpClient.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("vi"));
            httpClient.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("en"));
            httpClient.DefaultRequestHeaders.Add("UserAgent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36");
            httpClient.DefaultRequestHeaders.ConnectionClose = true;
            httpClient.DefaultRequestHeaders.Add("cache-control", "no-cache");
            httpClient.DefaultRequestHeaders.Add("dnt", "1");
            httpClient.DefaultRequestHeaders.Add("pragma", "no-cache");
            httpClient.DefaultRequestHeaders.Add("sec-ch-ua", "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"96\", \"Google Chrome\";v=\"96\"");
            httpClient.DefaultRequestHeaders.Add("sec-ch-ua-mobile", "?0");
            httpClient.DefaultRequestHeaders.Add("sec-ch-ua-platform", "Windows");
            httpClient.DefaultRequestHeaders.Add("sec-fetch-dest", "empty");
            httpClient.DefaultRequestHeaders.Add("sec-fetch-mode", "cors");
            httpClient.DefaultRequestHeaders.Add("sec-fetch-site", "same-origin");
            cancellationTokenSource = new CancellationTokenSource();
        }

        public MoonAtaClient(string userName, string passWord, Func<Task<string>> captcha, Func<string> twoFA = null) : this()
        {
            if (string.IsNullOrWhiteSpace(userName)) throw new ArgumentNullException(nameof(userName));
            if (string.IsNullOrWhiteSpace(passWord)) throw new ArgumentNullException(nameof(passWord));
            this._userName = userName;
            this._password = passWord;
            _captcha = captcha ?? throw new ArgumentNullException(nameof(captcha));
            this._twoFA = twoFA;
        }

        ~MoonAtaClient()
        {
            httpClient.Dispose();
            cancellationTokenSource.Dispose();
        }

        public void Dispose()
        {
            httpClient.Dispose();
            cancellationTokenSource.Dispose();
            GC.SuppressFinalize(this);
        }

        public void Cancel()
        {
            cancellationTokenSource.Cancel();
        }

        public void UpdateToken(MoonAtaToken token)
        {
            if (!string.IsNullOrWhiteSpace(token.Token) && !string.IsNullOrWhiteSpace(token.RefreshToken))
            {
                this.MoonAtaToken = token;
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", MoonAtaToken.Token);
            }
        }

        public Task Login(string userName, string passWord, Func<Task<string>> captcha, Func<string> twoFA = null, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(userName)) throw new ArgumentNullException(nameof(userName));
            if (string.IsNullOrWhiteSpace(passWord)) throw new ArgumentNullException(nameof(passWord));
            this._userName = userName;
            this._password = passWord;
            _captcha = captcha ?? throw new ArgumentNullException(nameof(captcha));
            this._twoFA = twoFA;

            return Login(cancellationToken);
        }

        public async Task Login(CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(_userName)) throw new MoonAtaAuthException();

            //check connection first
            {
                using HttpRequestMessage req_check = new HttpRequestMessage(HttpMethod.Get, "login");
                using HttpResponseMessage res_check = await httpClient.SendAsync(req_check, HttpCompletionOption.ResponseContentRead, cancellationToken != default ? cancellationToken : CancellationToken);
                res_check.EnsureSuccessStatusCode();
            }


            Auth auth = new Auth();
            auth.email = _userName;
            auth.password = _password;
            auth.captcha = await _captcha.Invoke();
            auth.grant_type = "password";

            using HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, "api/auth/auth/token");
            using StringContent stringContent = new StringContent(JsonConvert.SerializeObject(auth, serializerSettings), Encoding.UTF8, "application/json");
            req.Content = stringContent;
            using HttpResponseMessage res = await httpClient.SendAsync(req, HttpCompletionOption.ResponseContentRead, cancellationToken != default ? cancellationToken : CancellationToken);
            string text1 = await res.Content.ReadAsStringAsync();
            res.EnsureSuccessStatusCode();
            var obj1 = JsonConvert.DeserializeObject<ApiResponse<object>>(text1);
            if (!obj1.Ok) throw new Exception(obj1.Message);
            ApiResponse<MoonAtaToken> content = JsonConvert.DeserializeObject<ApiResponse<MoonAtaToken>>(text1);

            if (content.Ok && content.Data.Require2Fa == true)
            {
                string twoFaCode = _twoFA?.Invoke();
                if (string.IsNullOrWhiteSpace(twoFaCode)) throw new Exception("2FA required");
                MoonAta2FaRequest twoFaRequest = new MoonAta2FaRequest();
                twoFaRequest.ClientId = "pocinex-web";
                twoFaRequest.Code = twoFaCode;
                twoFaRequest.Token = content.Data.T;

                using HttpRequestMessage req2 = new HttpRequestMessage(HttpMethod.Post, "api/auth/auth/token-2fa");
                using StringContent stringContent2 = new StringContent(JsonConvert.SerializeObject(twoFaRequest, serializerSettings), Encoding.UTF8, "application/json");
                req2.Content = stringContent2;
                using HttpResponseMessage res2 = await httpClient.SendAsync(req2, HttpCompletionOption.ResponseContentRead, cancellationToken != default ? cancellationToken : CancellationToken);
                res2.EnsureSuccessStatusCode();
                string text2 = await res2.Content.ReadAsStringAsync();
                var obj2 = JsonConvert.DeserializeObject<ApiResponse<object>>(text2);
                if (!obj2.Ok) throw new Exception(obj2.Message);
                content = JsonConvert.DeserializeObject<ApiResponse<MoonAtaToken>>(text2);
            }
            MoonAtaToken = content.Data;
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", MoonAtaToken.Token);
        }

        async Task Refresh(CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(MoonAtaToken?.RefreshToken)) throw new MoonAtaAuthException();
            Auth auth = new Auth();
            auth.grant_type = "refresh_token";
            auth.refresh_token = MoonAtaToken.RefreshToken;

            using HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, "api/auth/auth/token");
            using StringContent stringContent = new StringContent(JsonConvert.SerializeObject(auth, serializerSettings), Encoding.UTF8, "application/json");
            req.Content = stringContent;
            using HttpResponseMessage res = await httpClient.SendAsync(req, HttpCompletionOption.ResponseContentRead, cancellationToken != default ? cancellationToken : CancellationToken);
            res.EnsureSuccessStatusCode();
            string text1 = await res.Content.ReadAsStringAsync();
            var obj1 = JsonConvert.DeserializeObject<ApiResponse<object>>(text1);
            if (!obj1.Ok)
            {
                await Login();
            }
            else
            {
                ApiResponse<MoonAtaToken> content = JsonConvert.DeserializeObject<ApiResponse<MoonAtaToken>>(text1);
                MoonAtaToken = content.Data;
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", MoonAtaToken.Token);
            }
        }


        async Task PreLogin(CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(MoonAtaToken?.Token))
            {
                await Login(cancellationToken);
            }
        }

        public Task<ApiResponse<MoonAtaBetResponse>> Bet(MoonAtaBet moonAtaBet, CancellationToken cancellationToken = default)
        {
            return GetApiResponse<MoonAtaBetResponse>(() =>
            {
                HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, $"api/wallet/binaryoption/bet");
                req.Content = new StringContent(
                    JsonConvert.SerializeObject(moonAtaBet, serializerSettings),
                    Encoding.UTF8,
                    "application/json");
                return req;
            }, cancellationToken);
        }


        public Task<ApiResponse<SpotBalanceResponse>> SpotBalance(CancellationToken cancellationToken = default)
        {
            return GetApiResponse<SpotBalanceResponse>(() => new HttpRequestMessage(HttpMethod.Get, $"api/wallet/binaryoption/spot-balance"), cancellationToken);
        }

        /// <summary>
        /// 0,1 open,2 max, 3min 4 close ,5,6,7,8,9
        /// </summary>
        /// <returns></returns>
        public Task<ApiResponse<double[][]>> Prices(CancellationToken cancellationToken = default)
        {
            return GetApiResponse<double[][]>(() => new HttpRequestMessage(HttpMethod.Get, "api/wallet/binaryoption/prices"), cancellationToken);
        }

        public async Task<List<PriceResponse>> Prices_Adv(CancellationToken cancellationToken = default)
        {
            var prices = await Prices(cancellationToken);
            return prices.Data.Select(x => new PriceResponse(x)).ToList();
        }

        public Task<ApiResponse<RewardInfoResponse>> Info(CancellationToken cancellationToken = default)
        {
            return GetApiResponse<RewardInfoResponse>(() => new HttpRequestMessage(HttpMethod.Get, "api/wallet/binaryoption/events/reward/info"), cancellationToken);
        }

        public Task<ApiResponse<TransactionResponse>> Transaction(Transaction transaction, CancellationToken cancellationToken = default)
        {
            return GetApiResponse<TransactionResponse>(() => new HttpRequestMessage(HttpMethod.Get, transaction.ToString()), cancellationToken);
        }

        public Task<ApiResponse<ProfileResponse>> Profile(CancellationToken cancellationToken = default)
        {
            return GetApiResponse<ProfileResponse>(() => new HttpRequestMessage(HttpMethod.Get, "api/auth/me/profile"), cancellationToken);
        }

        public Task<ApiResponse<AffiliateResponse>> Affiliate(Affiliate affiliate, CancellationToken cancellationToken = default)
        {
            return GetApiResponse<AffiliateResponse>(() => new HttpRequestMessage(HttpMethod.Get, affiliate.ToString()), cancellationToken);
        }


        class SocInit
        {
            public string sid { get; set; }
        }

        const string _t = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_";
        string GetT()
        {
            DateTime dateTime = DateTime.Now;
            long unixTime = ((DateTimeOffset)dateTime).ToUnixTimeMilliseconds();

            string result = string.Empty;

            while (unixTime != 0)
            {
                result += _t[(int)(unixTime % 64)].ToString();
                unixTime = (long)Math.Floor((decimal)unixTime / 64);
            }
            return result;
        }
        public async Task<ClientWebSocket> InitWebSocket()
        {
            var profile = await Profile();
            var parameters = HttpUtility.ParseQueryString(string.Empty);
            parameters["uid"] = profile.Data.uid;
            parameters["ssid"] = MoonAtaToken.Token;
            parameters["EIO"] = "3";
            parameters["transport"] = "polling";
            parameters["t"] = GetT();

            {
                Uri uri = new Uri($"https://{WebSocketEndPoint.Host}/socket.io/?" + parameters.ToString());
                using HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, uri);
                using HttpResponseMessage res = await httpClient.SendAsync(req, HttpCompletionOption.ResponseContentRead, CancellationToken);
                string content = await res.Content.ReadAsStringAsync();
                res.EnsureSuccessStatusCode();
                string json = content.Substring(content.IndexOf("{"));
                SocInit socinit = JsonConvert.DeserializeObject<SocInit>(json);
                parameters["sid"] = socinit.sid;
            }

            {
                parameters["t"] = GetT();
                Uri uri2 = new Uri($"https://{WebSocketEndPoint.Host}/socket.io/?" + parameters.ToString());
                using HttpRequestMessage req2 = new HttpRequestMessage(HttpMethod.Get, uri2);
                using HttpResponseMessage res2 = await httpClient.SendAsync(req2, HttpCompletionOption.ResponseContentRead, CancellationToken);
                string content2 = await res2.Content.ReadAsStringAsync();
                res2.EnsureSuccessStatusCode();
            }

            {
                parameters["t"] = GetT();

                Uri uri3 = new Uri($"https://{WebSocketEndPoint.Host}/socket.io/?" + parameters.ToString());
                using HttpRequestMessage req3 = new HttpRequestMessage(HttpMethod.Post, uri3);
                using StringContent stringContent3 = new StringContent("24:42[\"BO_PRICE_SUBSCRIBE\"]");
                req3.Content = stringContent3;
                using HttpResponseMessage res3 = await httpClient.SendAsync(req3, HttpCompletionOption.ResponseContentRead, CancellationToken);
                string content3 = await res3.Content.ReadAsStringAsync();
                res3.EnsureSuccessStatusCode();
            }

            {
                Uri uri4 = new Uri($"https://{WebSocketEndPoint.Host}/socket.io/?" + parameters.ToString());
                using HttpRequestMessage req4 = new HttpRequestMessage(HttpMethod.Get, uri4);
                using HttpResponseMessage res4 = await httpClient.SendAsync(req4, HttpCompletionOption.ResponseContentRead, CancellationToken);
                string content4 = await res4.Content.ReadAsStringAsync();
                res4.EnsureSuccessStatusCode();
            }


            parameters["t"] = GetT();
            parameters["transport"] = "websocket";
            var soc = new ClientWebSocket();

            byte[] buffer = new byte[2048];
            ArraySegment<byte> bufferSeg = new ArraySegment<byte>(buffer);

            {
                await soc.ConnectAsync(new Uri($"wss://{WebSocketEndPoint.Host}/socket.io/?" + parameters.ToString()), CancellationToken);
                await soc.SendAsync(new ArraySegment<byte>(Encoding.ASCII.GetBytes("2probe")), WebSocketMessageType.Text, true, CancellationToken);
                var a = await soc.ReceiveAsync(bufferSeg, CancellationToken);
                await soc.SendAsync(new ArraySegment<byte>(Encoding.ASCII.GetBytes("5")), WebSocketMessageType.Text, true, CancellationToken);
                var b = await soc.ReceiveAsync(bufferSeg, CancellationToken);
            }

            return soc;
        }


        async Task<ApiResponse<T>> GetApiResponse<T>(Func<HttpRequestMessage> f_req, CancellationToken cancellationToken = default)
        {
            await PreLogin(cancellationToken);
            using HttpRequestMessage req = f_req.Invoke();
            using HttpResponseMessage res = await httpClient.SendAsync(
                req,
                HttpCompletionOption.ResponseContentRead,
                cancellationToken != default ? cancellationToken : CancellationToken);

            LastTimeRequest = DateTime.Now;
            string content = await res.Content.ReadAsStringAsync();
            if (res.IsSuccessStatusCode)
            {
                var check = JsonConvert.DeserializeObject<ApiResponse<object>>(content);
                if (check.Ok)
                    return JsonConvert.DeserializeObject<ApiResponse<T>>(content);
                else
                    throw new Exception(content);
            }
            else
            {
                if (res.StatusCode == HttpStatusCode.Unauthorized)
                {
                    await Refresh();

                    using HttpRequestMessage req2 = f_req.Invoke();
                    using HttpResponseMessage res2 = await httpClient.SendAsync(
                        req2,
                        HttpCompletionOption.ResponseContentRead,
                        cancellationToken != default ? cancellationToken : CancellationToken);

                    content = await res2.Content.ReadAsStringAsync();
                    if (res2.IsSuccessStatusCode)
                    {
                        return JsonConvert.DeserializeObject<ApiResponse<T>>(content);
                    }
                }

                throw new Exception(await res.Content.ReadAsStringAsync());
            }
        }
    }
}
