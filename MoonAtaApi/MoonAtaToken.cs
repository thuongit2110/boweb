﻿using Newtonsoft.Json;

namespace MoonAtaApi
{
    public interface IMoonAtaToken
    {

        string Token { get; }
        string RefreshToken { get; }
    }
    public class MoonAtaToken : IMoonAtaToken
    {
        [JsonProperty("require2Fa")]
        public bool? Require2Fa { get; set; }

        [JsonProperty("t")]
        public string T { get; set; }

        [JsonProperty("verify-device")]
        public bool? VerifyDevice { get; set; }

        [JsonProperty("access_token")]
        public string Token { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
    }
}
