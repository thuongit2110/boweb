﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonAtaApi
{
    public class SpotBalanceResponse
    {
        [JsonProperty("availableBalance")]
        public double AvailableBalance { get; set; }


        [JsonProperty("usdtAvailableBalance")]
        public double UsdtAvailableBalance { get; set; }


        [JsonProperty("demoBalance")]
        public double DemoBalance { get; set; }


        [JsonProperty("aliAvailableBalance")]
        public double AliAvailableBalance { get; set; }


        [JsonProperty("aliPrice")]
        public double AliPrice { get; set; }
    }
}
