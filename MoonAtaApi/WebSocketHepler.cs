﻿using Newtonsoft.Json;
using Nito.AsyncEx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace MoonAtaApi
{
    public class BoReceived
    {
        [JsonProperty("lowPrice")] public decimal LowPrice { get; set; }
        [JsonProperty("highPrice")] public decimal HighPrice { get; set; }
        [JsonProperty("openPrice")] public decimal OpenPrice { get; set; }
        [JsonProperty("closePrice")] public decimal ClosePrice { get; set; }
        [JsonProperty("baseVolume")] public decimal BaseVolume { get; set; }
        [JsonProperty("createDateTime")] public long CreateDateTime { get; set; }
        [JsonProperty("ordinal")] public long Ordinal { get; set; }
        [JsonProperty("session")] public long Session { get; set; }
        [JsonProperty("isBetSession")] public bool IsBetSession { get; set; }
        [JsonProperty("orderClose")] public int OrderClose { get; set; }
        [JsonProperty("order")] public int Order { get; set; }

        [JsonIgnore]
        public bool? IsGreen
        {
            get
            {
                if (ClosePrice > OpenPrice) return true;
                else if (ClosePrice < OpenPrice) return false;
                else return null;
            }
        }

        //{"lowPrice":41886.01,"session":1858606,"isBetSession":false,"highPrice":41889.83,"openPrice":41888.76,"closePrice":41888.92,"baseVolume":2.37686,"orderClose":9,"createDateTime":1641650460873,"ordinal":55758160,"order":21}
    }

    public class AliInfo
    {
        //{"priceInUSDT":"0.7807660383411648571247164899","timestamp":1641789183297}
        [JsonProperty("priceInUSDT")]
        public decimal PriceInUSDT { get; set; }

        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }
    }
    public class WebSocketHepler
    {
        static readonly Regex regex = new Regex("{.*?}");
        public bool IsWorking { get; private set; } = false;
        readonly CancellationToken cancellationToken;
        public WebSocketHepler(CancellationToken cancellationToken)
        {
            this.cancellationToken = cancellationToken;
        }
        public event Action WebSocketOnDisconnect;
        public event Action<Exception> OnException;
        public event Action<BoReceived> OnBoReceived;
        public event Action<AliInfo> OnAliInfoReceived;
        public void TaskWebsocket(ClientWebSocket clientWebSocket)
        {
            Task.Factory.StartNew(() =>
            {
                AsyncContext.Run(async() => await _TaskWebsocket(clientWebSocket));
            }, CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        public async Task _TaskWebsocket(ClientWebSocket clientWebSocket)
        {
            using var sock = clientWebSocket;
            byte[] buffer = new byte[4096];
            ArraySegment<byte> bufferSeg = new ArraySegment<byte>(buffer);
            int count = 0;
            IsWorking = true;
            DateTime lastTime = DateTime.Now.AddSeconds(15);
            try
            {
                while (!cancellationToken.IsCancellationRequested && sock.State == WebSocketState.Open)
                {
                    switch (sock.State)
                    {
                        case WebSocketState.Open:
                            {
                                var received = await sock.ReceiveAsync(bufferSeg, cancellationToken);
                                if (received.EndOfMessage)
                                {
                                    string text = Encoding.UTF8.GetString(buffer, 0, received.Count);
                                    System.Diagnostics.Debug.WriteLine(text);
                                    count++;
                                    if (text.Contains("\"BO_PRICE\""))
                                    {
                                        Match match = regex.Match(text);
                                        if (match.Success)
                                        {
                                            OnBoReceived?.Invoke(JsonConvert.DeserializeObject<BoReceived>(match.Value));
                                        }
                                    }
                                    else if (text.Contains("\"ALI_INFO\""))
                                    {
                                        Match match = regex.Match(text);
                                        if (match.Success)
                                        {
                                            OnAliInfoReceived?.Invoke(JsonConvert.DeserializeObject<AliInfo>(match.Value));
                                        }
                                    }
                                }
                                else
                                {

                                }
                            }
                            break;

                        default:
                            System.Diagnostics.Debug.WriteLine($"{sock.State}");
                            return;
                    }
                    if (DateTime.Now >= lastTime)
                    {
                        System.Diagnostics.Debug.WriteLine($"Send 2");
                        await sock.SendAsync(new ArraySegment<byte>(Encoding.ASCII.GetBytes("2")), WebSocketMessageType.Text, true, cancellationToken);
                        lastTime = DateTime.Now.AddSeconds(15);
                        count = 0;
                    }
                }
            }
            catch(Exception ex)
            {
                OnException?.Invoke(ex);
            }
            finally
            {
                System.Diagnostics.Debug.WriteLine($"Closed: State:{sock.State}, CloseStatus:{sock.CloseStatus}");
                IsWorking = false;
            }

            WebSocketOnDisconnect?.Invoke();
        }
    }
}
