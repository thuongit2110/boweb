﻿using Newtonsoft.Json;

namespace MoonAtaApi
{
    public class MoonAta2FaRequest
    {
        [JsonProperty("client_id")]
        public string ClientId { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("td_code")]
        public string TdCode { get; set; }

        [JsonProperty("td_p_code")]
        public string TdPCode { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
