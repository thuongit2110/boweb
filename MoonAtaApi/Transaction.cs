﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonAtaApi
{
    public enum TransactionMode
    {
        Open,
        Close
    }
    public class Transaction
    {
        public TransactionMode Mode { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
        public MoonAtaBetAccountType AccountType { get; set; }
        public override string ToString()
        {
            return $"api/wallet/binaryoption/transaction/{Mode.ToString().ToLower()}?page={Page}&size={Size}&betAccountType={AccountType}";
        }
    }

    public class TransactionResponse
    {
        public int p { get; set; }
        public int s { get; set; }
        public int t { get; set; }
        public List<TransactionItem> c { get; set; }
    }
    public class TransactionItem
    {
        public decimal basePrice { get; set; }
        public double betAmount { get; set; }
        public MoonAtaBetType betType { get; set; }
        public decimal? closePrice { get; set; }
        public long createdDatetime { get; set; }
        public string currencyType { get; set; }
        public TransactionResult result { get; set; }
        public string tradeType { get; set; }
        public string transactionId { get; set; }
        public string userId { get; set; }
        public decimal winAmount { get; set; }
    }

    public enum TransactionResult
    {
        OPEN,
        LOSE,
        WIN
    }
}
