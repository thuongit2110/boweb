﻿using Ganss.XSS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSource.Utilities.Extensions
{
    public static class SafeHtml
    {

        public static string GetSafeHtml(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }
            var sanitizer = new HtmlSanitizer();
            sanitizer.RemovingTag += (s, e) =>
            {
                if (e.Tag.NodeName.Equals("iframe", StringComparison.OrdinalIgnoreCase))
                {
                    e.Cancel = true;
                }
            };
            var sanitized = sanitizer.Sanitize(input);

            return sanitized;
        }
    }
}
